#!/bin/bash

os_name=`uname -s`
database=sjtu_online_judge
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

if [ "$os_name" = "Linux" ]; then
    if [[ $EUID -ne 0 ]]; then
        echo "This script must be run as root"
        exit 1
    fi

    echo "[WARNING] This auto deployment script does not guarantee the security"
    echo "[WARNING] You'll need to review all security settings, like database access policy and passwords"
    echo "This auto deployment script will all services, including local judge on this machine, are you sure you want to continue?"
    select yn in "Yes" "No"; do
        case $yn in
            No ) exit 0;;
            Yes ) break;;
        esac
    done

    echo "Do you want to change the apt source to ftp.sjtu.edu.cn?"
    select yn in "Yes" "No"; do
        case $yn in
            No ) break;;
            Yes )
                sed -i 's/us.archive.ubuntu.com/ftp.sjtu.edu.cn/g' /etc/apt/sources.list
                sed -i 's/security.ubuntu.com/ftp.sjtu.edu.cn/g' /etc/apt/sources.list
                apt-get update
                break;;
               
        esac
    done
    
    while true; do
        echo "Please set a password for all services (e.g. Database, ftp...)"
        echo "Do not enter any special characters, only enter letters/digits"
        echo -n Password:
        read -s password
        echo
        echo -n Retype Password:
        read -s password_retype
        echo
        if [ -z $password ]; then
            echo "Empty password is not allowed!"
            continue
        fi
        if [ $password = $password_retype ]; then
            break;
        else
            echo "2 passwords does not match"
        fi
    done

    
    #echo "Please enter the FTP user's username (All test cases will be stored in this user's home directory) [acmtestcase]"
    #echo -n "Username for FTP: "
    #read ftp_user_name
    if [ -z $ftp_user_name ]; then
        ftp_user_name=acmtestcase
    fi

    #echo "Enter the username that will execute submitted programs [bigdog]"
    #echo -n "Username for Localjudge: "
    #read exec_username
    if [ -z $exec_username ]; then
        exec_username=bigdog
    fi
    
    echo -n "* Running apt-get..."
    yes | apt-get install build-essential python python-dev python-pip postgresql postgresql-server-dev-all redis-server git apache2 libapache2-mod-wsgi dos2unix vsftpd rsync openjdk-7-jdk g++ 
    echo "done"
    
    echo -n "* Running pip install ..."
    pip install django==1.5 celery psycopg2 django-celery celery-with-redis django-redis South 
    echo "done"
    
    echo -n "* Configuring postgresql database..."
    echo -e "ALTER USER postgres WITH PASSWORD '$password'\n\\q" | su postgres -c psql >> deploy.log
    echo -e "CREATE DATABASE $database\n\\q" | su postgres -c psql >> deploy.log
    if [ -d /etc/postgresql/9.1 ]; then
        echo "host all all 0.0.0.0/0 password" >> /etc/postgresql/9.1/main/pg_hba.conf
        sed -i "s/#listen_addresses = 'localhost'/listen_addresses = '*'/g" /etc/postgresql/9.1/main/postgresql.conf
    fi
    if [ -d /etc/postgresql/9.3 ]; then
        echo "host all all 0.0.0.0/0 password" >> /etc/postgresql/9.3/main/pg_hba.conf
        sed -i "s/#listen_addresses = 'localhost'/listen_addresses = '*'/g" /etc/postgresql/9.3/main/postgresql.conf
    fi

    service postgresql restart > /dev/null
    echo "done"
    
    echo -n "* Configuring redis database..."
    sed -i '/^bind/d' /etc/redis/redis.conf
    service redis-server restart > /dev/null
    echo "done"
    
    echo -n "* Configuring FTP server"
        
    echo "creating user $ftp_user_name"
    useradd -m -s /bin/bash $ftp_user_name
    echo -e "$password\n$password\n" | passwd $ftp_user_name 2>> deploy.log
    sed -i "s/#write_enable=YES/write_enable=YES/g" /etc/vsftpd.conf
    service vsftpd restart > /dev/null
    echo done
    
    echo -n "* Configuring rsync..."
    sed -i 's/RSYNC_ENABLE=false/RSYNC_ENABLE=true/g' /etc/default/rsync
    mkdir -p /etc/rsyncd
    # begin write conf files
    touch /etc/rsyncd/rsyncd.motd
    echo "$ftp_user_name:$password" > /etc/rsyncd/rsyncd.secrets
    chmod 600 /etc/rsyncd/rsyncd.secrets # Important, otherwise rsyncd refuses to auth
    echo "motd file = /etc/rsyncd/rsyncd.motd" > /etc/rsyncd.conf
    echo "[$ftp_user_name]" >> /etc/rsyncd.conf
    echo "path = /home/$ftp_user_name" >> /etc/rsyncd.conf
    echo "comment = This is the path to folder on the server" >> /etc/rsyncd.conf
    echo "uid = $ftp_user_name" >> /etc/rsyncd.conf
    echo "gid = $ftp_user_name" >> /etc/rsyncd.conf
    echo "read only = yes" >> /etc/rsyncd.conf
    echo "auth users = $ftp_user_name" >> /etc/rsyncd.conf
    echo "secrets file = /etc/rsyncd/rsyncd.secrets" >> /etc/rsyncd.conf
    # end write conf files
    service rsync restart > /dev/null
    echo "done"
    
    echo -n "* Configuring local judge..."
    useradd -m -s /bin/bash $exec_username
    cp $DIR/apparmor/bigdog.dog /etc/apparmor.d/
    echo -e "$password\n$password\n" | passwd $exec_username 2>> deploy.log
    echo done

    echo -n "* Populating settings.py..."
    cp website/judge/settings.py website/judge/settings.py.`date +%s`
    cp website/judge/settings.py.example website/judge/settings.py
    server_address="127.0.0.1"
    sed -i "s/{{ REDIS_ADDRESS }}/$server_address/g" website/judge/settings.py
    sed -i "s/{{ TEST_CASE_FTP_URL }}/$server_address/g" website/judge/settings.py
    sed -i "s/{{ TEST_CASE_FTP_USERNAME }}/$ftp_user_name/g" website/judge/settings.py
    sed -i "s/{{ TEST_CASE_FTP_PASSWORD }}/$password/g" website/judge/settings.py
    sed -i "s/{{ RSYNC_HOST }}/$server_address/g" website/judge/settings.py
    sed -i "s/{{ RSYNC_USER }}/$ftp_user_name/g" website/judge/settings.py
    sed -i "s/{{ RSYNC_SRC }}/$ftp_user_name/g" website/judge/settings.py
    sed -i "s/{{ RSYNC_PASSWORD }}/$password/g" website/judge/settings.py
    sed -i "s/{{ FTP_HOST }}/$server_address/g" website/judge/settings.py
    sed -i "s/{{ FTP_USERNAME }}/$ftp_user_name/g" website/judge/settings.py
    sed -i "s/{{ FTP_PASSWORD }}/$password/g" website/judge/settings.py
    sed -i "s/{{ DATABASE_NAME }}/$database/g" website/judge/settings.py
    sed -i "s/{{ DATABASE_USER }}/postgres/g" website/judge/settings.py
    sed -i "s/{{ DATABASE_PASSWORD }}/$password/g" website/judge/settings.py
    sed -i "s/{{ DATABASE_HOST }}/$server_address/g" website/judge/settings.py
    sed -i "s|{{ STATIC_ROOT }}|$DIR/static/|g" website/judge/settings.py
    echo done

    echo -n "Initialize Apache..."
    mkdir $DIR/static
    chmod -R +r $DIR
    touch /etc/apache2/sites-available/sjtuonlinejudge
    ln -s /etc/apache2/sites-available/sjtuonlinejudge /etc/apache2/sites-enabled/sjtuonlinejudge.conf
    echo "Alias /static/ $DIR/static/" > /etc/apache2/sites-available/sjtuonlinejudge

    echo "<Directory $DIR/static>" >> /etc/apache2/sites-available/sjtuonlinejudge
    echo "<IfVersion < 2.3 >" >> /etc/apache2/sites-available/sjtuonlinejudge
    echo "Order deny,allow" >> /etc/apache2/sites-available/sjtuonlinejudge
    echo "Allow from all" >> /etc/apache2/sites-available/sjtuonlinejudge
    echo "</IfVersion>" >> /etc/apache2/sites-available/sjtuonlinejudge
    echo "<IfVersion >= 2.3 >" >> /etc/apache2/sites-available/sjtuonlinejudge
    echo "Require all granted" >> /etc/apache2/sites-available/sjtuonlinejudge
    echo "</IfVersion>" >> /etc/apache2/sites-available/sjtuonlinejudge
    echo "</Directory>" >> /etc/apache2/sites-available/sjtuonlinejudge

    echo "WSGIScriptAlias / $DIR/website/judge/wsgi.py" >> /etc/apache2/sites-available/sjtuonlinejudge
    echo "WSGIPythonPath $DIR/website" >> /etc/apache2/sites-available/sjtuonlinejudge


    echo "<Directory $DIR/website/judge>" >> /etc/apache2/sites-available/sjtuonlinejudge
    echo "<Files wsgi.py>" >> /etc/apache2/sites-available/sjtuonlinejudge
    echo "<IfVersion < 2.3 >" >> /etc/apache2/sites-available/sjtuonlinejudge
    echo "Order deny,allow" >> /etc/apache2/sites-available/sjtuonlinejudge
    echo "Allow from all" >> /etc/apache2/sites-available/sjtuonlinejudge
    echo "</IfVersion>" >> /etc/apache2/sites-available/sjtuonlinejudge
    echo "<IfVersion >= 2.3 >" >> /etc/apache2/sites-available/sjtuonlinejudge
    echo "Require all granted" >> /etc/apache2/sites-available/sjtuonlinejudge
    echo "</IfVersion>" >> /etc/apache2/sites-available/sjtuonlinejudge
    echo "</Files>" >> /etc/apache2/sites-available/sjtuonlinejudge
    echo "</Directory>" >> /etc/apache2/sites-available/sjtuonlinejudge
    service apache2 restart
    echo done

    echo -n "Initialize django db..."
    cd website
    python manage.py syncdb
    python manage.py migrate
    echo yes | python manage.py collectstatic
    cd ..
    echo done

else
    echo "Your operating system is not supported"
    echo "The suggested OS is Ubuntu 14.04 LTS"
fi


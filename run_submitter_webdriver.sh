#!/bin/bash

cd website
python manage.py celeryd -Q submitter_webdriver -l INFO -c 1 -E --soft-time-limit=180 -n submitter_webdriver_worker.`hostname`

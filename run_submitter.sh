#!/bin/bash

cd website
python manage.py celeryd -Q submitter -l INFO -c 1 -E --soft-time-limit=300 -n submitter_worker.`hostname`

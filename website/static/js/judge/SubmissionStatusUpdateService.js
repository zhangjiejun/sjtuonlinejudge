/*global define:false*/
define(["jquery"], function($) {
	var singleton = function() {
		var handlers = {};
		var interval = 10000;
		var started = false;
		var url = null;
		
		function poll() {
			var list = [];
			$.each(handlers, function(key, value) {
				if (value) {
					list.push(key);
				}
			});
			if (list.length > 0) {
				$.get(url, { list: list.join() }, function(data) {
					if (!started) {
						return;
					}
					$.each(data, function(pk, status) {
						var pos = pk.lastIndexOf("_");
						pk = pk.substr(pos + 1);
						handlers[pk](status);
					});
					setTimeout(poll, interval);
				});
			} else {
				if (started) {
					setTimeout(poll, interval);
				}
			}
		}
		
		return {
			/**
			 * Track the status of a specific submission
			 */
			registerHandler: function(pk, handler) {
				handlers[pk] = handler;
			},
			
			/**
			 * Untrack a specific submission givin id.
			 * If id is not given, all submissions will be untracked
			 */
			unregisterHandler: function(pk) {
				if (pk) {
					delete handlers[pk];
				} else {
					handlers = {};
				}
			},
			
			/**
			 * Set the polling interval
			 */
			setPollingInterval: function(newInterval) {
				interval = newInterval;
			},
			
			/**
			 * Set the polling url
			 */
			setUrl: function(newUrl) {
				url = newUrl;
			},
			
			/**
			 * Start this service
			 */
			start: function() {
				started = true;
				poll();
			},
			
			/**
			 * Stop this service
			 */
			stop: function() {
				started = false;
			}
		};
	};
	return singleton();
});
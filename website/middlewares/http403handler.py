import judge.utils


class Http403(judge.utils.Http403):
    pass


class Http403Middleware(object):
    def process_exception(self, request, exception):
        if isinstance(exception, judge.utils.Http403):
            return judge.utils.display_message(request, 'Access Denied', exception.message, status=403)

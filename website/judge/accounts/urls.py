from django.conf.urls import patterns, url
from judge.accounts.views import *

urlpatterns = patterns('django.contrib.auth.views',
    url(r'^logout/$', 'logout', {'next_page': '/'}, name="logout"),
)

urlpatterns += patterns('judge.accounts.views',
    url(r'^login/$', 'login', {"template_name": "accounts/login.djhtml"}, name="login"),
    url(r'^profile/$', 'profile'),
    url(r'^register/$', RegisterView.as_view(), name="register"),
    url(r"^complete-profile/", CompleteProfileView.as_view(), name="complete-profile"),
    url(r"^complete-monthly-profile/$", CreateMonthlyProfileView.as_view(), name="complete-monthly-profile"),
    url(r"^complete-individual-profile/$", CreateIndividualProfileView.as_view(), name="complete-individual-profile"),
    url(r"^complete-team-profile/$", CreateTeamProfileView.as_view(), name="complete-team-profile"),
)

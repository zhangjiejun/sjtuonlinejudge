from django.contrib.auth.models import User
from django.contrib.auth.decorators import login_required
from django.core.exceptions import ObjectDoesNotExist
from django.core.urlresolvers import reverse, reverse_lazy
from django.template import RequestContext
from django.http import HttpResponse, HttpResponseRedirect
from django.contrib import messages
from django.shortcuts import get_object_or_404, render, resolve_url

from judge.utils import smart_render_to_response, display_message, smart_render,\
    JudgeFormView
from judge.accounts.forms import *
from judge.accounts.models import *
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm
from django.contrib.auth import authenticate, login as auth_login
from django.views.decorators.cache import never_cache
from django.views.decorators.debug import sensitive_post_parameters
from django.views.decorators.csrf import csrf_protect
from django.utils.http import is_safe_url
from django.conf import settings
from django.contrib.sites.models import get_current_site
from django.template.response import TemplateResponse
from django.views.generic.base import TemplateView


@login_required
def profile(request):
    """View for users to update their profile"""
    if not settings.OJ["ALLOW_CHANGE_PROFILE"]:
        return display_message(request, "Profile is not allowed to change.", "")
    profile = request.user.userprofile
    try:
        profile = profile.monthlyprofile
        if request.method == 'POST':
            form = MonthlyProfileForm(request.POST, instance=profile)
            if form.is_valid():
                form.save()
                messages.success(request, "Profile Updated.")
                return HttpResponseRedirect(reverse("profile"))
        else:
            form = MonthlyProfileForm(instance=profile)
        return render(request, "general_form.djhtml",
                      {"form": form, "form_title": "Update Profile"})
    except ObjectDoesNotExist:
        pass
    try:
        profile = profile.individualprofile
        if request.method == "POST":
            form = IndividualProfileForm(request.POST, instance=profile)
            if form.is_valid():
                form.save()
                messages.success(request, "Successfully updated.")
                return HttpResponseRedirect(reverse("profile"))
        else:
            form = IndividualProfileForm(instance=profile)
        return render(request, "general_form.djhtml",
                      {"form": form, "form_title": "Update Profile"})
    except ObjectDoesNotExist:
        profile = profile.teamprofile
        if request.method == 'POST':
            form = UpdateTeamProfileForm(request.POST)
            if form.is_valid():
                form.save()
                messages.success(request, "Successfully updated.")
                return HttpResponseRedirect(reverse("profile"))
        else:
            form = UpdateTeamProfileForm(instance=profile)
        return render(request, "accounts/general_form.djhtml",
                      {"form": form, "form_title": "Update Profile"})


class RegisterView(JudgeFormView):
    form_title = "Register"
    form_class = UserCreationForm
    REGISTER_CLOSED_MESSAGE = "Registration is closed."

    def dispatch(self, request, *args, **kwargs):
        if not settings.OJ["ALLOW_REGISTER"]:
            return display_message(request, self.REGISTER_CLOSED_MESSAGE, "", status=403)
        return super(RegisterView, self).dispatch(request, *args, **kwargs)
    
    def form_valid(self, form):
        form.save()
        data = form.cleaned_data
        user = authenticate(username=data["username"], password=data["password1"])
        auth_login(self.request, user)
        return HttpResponseRedirect(reverse("complete-profile"))


class CompleteProfileView(TemplateView):
    template_name = "accounts/profile_choice.djhtml"


class CreateTeamProfileView(JudgeFormView):
    form_title = "Complete Your Profile"
    form_help_text = "You are registering as an ACM-ICPC Team."
    form_class = TeamProfileForm
    no_cancel = True

    def form_valid(self, form):
        profile = form.save(commit=False)
        profile.user = self.request.user
        profile.save()
        return HttpResponseRedirect("/")


class CreateIndividualProfileView(JudgeFormView):
    form_title = "Complete Your Profile"
    form_help_text = "You are registering as an ACM-ICPC Individual Competitor."
    form_class = IndividualProfileForm
    no_cancel = True

    def form_valid(self, form):
        profile = form.save(commit=False)
        profile.user = self.request.user
        profile.save()
        return HttpResponseRedirect("/")


class CreateMonthlyProfileView(JudgeFormView):
    form_title = "Complete Your Profile"
    form_help_text = "You are registering as a Monthly Competitor. You will be able to attend SJTU Monthly Contests"
    form_class = MonthlyProfileForm
    no_cancel = True
    
    def form_valid(self, form):
        profile = form.save(commit=False)
        profile.type = "D"
        profile.user = self.request.user
        profile.save()
        return HttpResponseRedirect("/")


@sensitive_post_parameters()
@csrf_protect
@never_cache
def login(request, template_name='registration/login.html',
          redirect_field_name="next",
          authentication_form=AuthenticationForm,
          current_app=None, extra_context=None):
    """
    Displays the login form and handles the login action.
    """
    redirect_to = request.REQUEST.get(redirect_field_name, '')

    if request.method == "POST":
        form = authentication_form(data=request.POST)
        if form.is_valid():

            # Ensure the user-originating redirection url is safe.
            if not is_safe_url(url=redirect_to, host=request.get_host()):
                redirect_to = resolve_url(settings.LOGIN_REDIRECT_URL)

            # Okay, security check complete. Log the user in.
            auth_login(request, form.get_user())

            if request.session.test_cookie_worked():
                request.session.delete_test_cookie()
                
            if not UserProfile.objects.filter(user=form.get_user()).exists():
                return HttpResponseRedirect(reverse("complete-profile"))

            return HttpResponseRedirect(redirect_to)
    else:
        form = authentication_form(request)

    request.session.set_test_cookie()

    current_site = get_current_site(request)

    context = {
        'form': form,
        redirect_field_name: redirect_to,
        'site': current_site,
        'site_name': current_site.name,
    }
    if extra_context is not None:
        context.update(extra_context)
    return TemplateResponse(request, template_name, context,
                            current_app=current_app)


def view_profile(request, pk):
    user = get_object_or_404(User, pk=pk)
    profile = user.userprofile
    return smart_render_to_response(
        request, 'accounts/view_profile.html', {
            'profile': profile,
        },
        RequestContext(request),
    )

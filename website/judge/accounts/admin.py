from django.contrib import admin
from judge.accounts.models import UserProfile, TeamProfile, IndividualProfile, MonthlyProfile, OldJudgeUser

class MonthlyProfileAdmin(admin.ModelAdmin):
    list_filter = ["type"]
    list_display = ("user", "name", "type")

admin.site.register(UserProfile)
admin.site.register(IndividualProfile)
admin.site.register(TeamProfile)
admin.site.register(OldJudgeUser)
admin.site.register(MonthlyProfile, MonthlyProfileAdmin)
from django import forms
from judge.accounts.models import IndividualProfile, MonthlyProfile, TeamProfile
from django.contrib.auth.models import User
from django.core.exceptions import ObjectDoesNotExist
from django.core import validators
from django.conf import settings


class UserField(forms.CharField):
    def clean(self, value):
        if value in validators.EMPTY_VALUES:
            return value
        try:
            return User.objects.get(username=value)
        except ObjectDoesNotExist:
            raise forms.ValidationError('User does not exist')


class IndividualProfileForm(forms.ModelForm):
    class Meta:
        model = IndividualProfile
        fields = ("name", "default_language", "student_id")


class TeamProfileForm(forms.ModelForm):
    class Meta:
        model = TeamProfile
        fields = ("name", "default_language", "leader", "member_one", "member_two")
        widgets = {
            "leader": forms.TextInput(attrs={"placeholder": "Leader's username"}),
            "member_one": forms.TextInput(attrs={"placeholder": "Member one's username"}),
            "member_two": forms.TextInput(attrs={"placeholder": "Member two's username"})
        }


class MonthlyProfileForm(forms.ModelForm):
    class Meta:
        model = MonthlyProfile
        fields = ("name", "email", "default_language", "student_id", "qq", "cell_phone", "gender", "college", "college_year")


class UpdateTeamProfileForm(forms.Form):
    name = forms.CharField(help_text='Required. Your team\'s name others will see')
    default_language = forms.ChoiceField(choices=settings.SUPPORTED_LANGUAGES)
    leader = UserField(help_text="Captain's username")
    member_one = UserField(help_text="Member 1's username")
    member_two = UserField(help_text="Member 2's username")


class UpdateEmailForm(forms.Form):
    email = forms.EmailField()


class RegisterTeamForm(forms.Form):
    username = forms.CharField(help_text='Required. At most 30 characters')
    password = forms.CharField(help_text='Required. Password can be arbitrarily long ' + 
                               'and can contain any character. We do not store your raw ' +
                               'password (we store a hash of it).',
                               widget=forms.PasswordInput())
    password_confirm = forms.CharField(help_text='Required. Enter your password again to confirm',
                                       widget=forms.PasswordInput())
    name = forms.CharField(help_text='Required. This your displayed token in this site')
    leader = UserField(help_text='Required. Captain\'s username')
    member_one = UserField(help_text='Required. Member 1\'s username')
    member_two = UserField(help_text='Required. Member 2\'s username')
    default_language = forms.ChoiceField(choices=settings.SUPPORTED_LANGUAGES)

    def clean_username(self):
        data = self.cleaned_data['username']
        if User.objects.filter(username=data).count():
            raise forms.ValidationError('This username is already used')
        return data

    def clean(self):
        data = super(RegisterTeamForm, self).clean()
        if data.get('password') != data.get('password_confirm'):
            raise forms.ValidationError('2 Passwords does not match')
        return data


class RegisterIndividualForm(forms.Form):
    username = forms.CharField(help_text='Required. At most 30 characters')
    password = forms.CharField(help_text='Required. Password can be arbitrarily long and ' +
                                         'can contain any character. We do not store your ' + 
                                         'raw password (we store a hash of it).',
                                         widget=forms.PasswordInput())
    password_confirm = forms.CharField(help_text='Required. Enter your password again to confirm',
                                       widget=forms.PasswordInput())
    email = forms.EmailField(required=False, help_text='Optional. But it is the ' +
                             'only way to reset your password')
    name = forms.CharField(help_text='Required. This is your displayed token in this site .')
    default_language = forms.ChoiceField(choices=settings.SUPPORTED_LANGUAGES)
    student_id = forms.CharField(help_text='Optional. If you want to attend our monthly ' + 
                                           'contest, you must provide you real student ID', 
                                           required=False)

    def clean_username(self):
        data = self.cleaned_data['username']
        if User.objects.filter(username=data).count():
            raise forms.ValidationError('This username is already used')
        return data

    def clean_email(self):
        data = self.cleaned_data['email']
        if data and User.objects.filter(email=data).count():
            raise forms.ValidationError('This email is already used')
        return data

    def clean(self):
        data = super(RegisterIndividualForm, self).clean()
        if data.get('password') != data.get('password_confirm'):
            raise forms.ValidationError('2 Passwords does not match')
        return data
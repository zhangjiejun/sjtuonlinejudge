from django.test import TestCase
from django.test.client import Client
from django.test.utils import override_settings
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from judge.accounts.models import IndividualProfile, TeamProfile, UserProfile
from django.conf import settings
from unittest import skip
from judge.accounts.views import RegisterView


class RegisterViewTest(TestCase):
    """Test class for RegisterView"""

    def setUp(self):
        self.client = Client()
        self.url = reverse("register")

    def test_register_routine_individual(self):
        """Tests if the register a individual user routine works

        The routine is:
        1. User GET the register URL
        2. User POST a form to the register URL, with "username", "password1", "password2",
           and a django.contrib.auth.models.User object is created
        3. User is redirected prompted to choose profile type
        4. User GET the complete profile URL (individual)
        5. User POST a form to the complete profile URL

        """
        # Step 1
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200,
                         "Status code of GET register URL is {}, expected 200".format(response.status_code))
        # Step 2
        register_data = {
            "username": "regular_user",
            "password1": "password1",
            "password2": "password1",
        }
        oj_settings = settings.OJ.copy()
        oj_settings["ALLOW_REGISTER"] = True
        response = self.client.post(self.url, register_data, follow=True)
        self.assertEqual(response.status_code, 200,
                         "Status of POST register URL is {}, expected 200".format(response.status_code))
        self.assertEqual(User.objects.filter(username="regular_user").count(), 1,
                         "RegisterView returned OK but can't find the registered User object")
        # Step 3
        self.assertTrue("<h2>Complete Profile</h2>" in response.content,
                        "Can't determine if user is prompted to choose a profile to complete, "
                        "feature string '<h2>Complete Profile</h2>' not found in returned HTML.")
        # Step 4
        response = self.client.get(reverse("complete-individual-profile"))
        self.assertEqual(response.status_code, 200,
                         "GET complete-individual-profile view status code {}, expected 200".format(response.status_code))
        # Step 5
        response = self.client.post(reverse("complete-individual-profile"),
                                    {
                                        "name": "Regular User",
                                        "default_language": "C++",
                                        "student_id": "1234567"
                                    },
                                    follow=True)
        self.assertEqual(response.status_code, 200,
                         "GET complete-individual-profile view status code {}, "
                         "expected 200".format(response.status_code))
        try:
            profile = UserProfile.objects.get(user__username="regular_user")
            profile = profile.individualprofile
        except UserProfile.DoesNotExist:
            self.fail("Complete Profile view returned OK, but the UserProfile object is not found.")
        self.assertEqual(profile.name, "Regular User")
        self.assertEqual(profile.default_language, "C++")
        self.assertEqual(profile.student_id, "1234567")

    def test_register_routine_monthly(self):
        """Tests if the register a monthly user routine works

        The routine is:
        1. User GET the register URL
        2. User POST a form to the register URL, with "username", "password1", "password2",
           and a django.contrib.auth.models.User object is created
        3. User is redirected prompted to choose profile type
        4. User GET the complete profile URL (monthly)
        5. User POST a form to the complete profile URL

        """
        # Step 1
        response = self.client.get(self.url)
        self.assertEqual(response.status_code, 200,
                         "Status code of GET register URL is {}, expected 200".format(response.status_code))
        # Step 2
        register_data = {
            "username": "monthly_user",
            "password1": "password1",
            "password2": "password1",
        }
        response = self.client.post(self.url, register_data, follow=True)
        self.assertEqual(response.status_code, 200,
                         "Status of POST register URL is {}, expected 200".format(response.status_code))
        self.assertEqual(User.objects.filter(username="monthly_user").count(), 1,
                         "RegisterView returned OK but can't find the registered User object")
        # Step 3
        self.assertTrue("<h2>Complete Profile</h2>" in response.content,
                        "Can't determine if user is prompted to choose a profile to complete, "
                        "feature string '<h2>Complete Profile</h2>' not found in returned HTML.")
        # Step 4
        response = self.client.get(reverse("complete-individual-profile"))
        self.assertEqual(response.status_code, 200,
                         "GET complete-individual-profile view status code {}, expected 200".format(response.status_code))
        # Step 5
        response = self.client.post(reverse("complete-monthly-profile"),
                                    {
                                        "name": "Monthly User",
                                        "default_language": "C++",
                                        "email": "a@b.com",
                                        "student_id": "1234567",
                                        "qq": "2234567",
                                        "cell_phone": "18812341234",
                                        "gender": "M",
                                        "college": "SJTU",
                                        "college_year": "1"
                                    },
                                    follow=True)
        self.assertEqual(response.status_code, 200,
                         "GET complete-monthly-profile view status code {}, "
                         "expected 200".format(response.status_code))
        try:
            profile = UserProfile.objects.get(user__username="monthly_user")
            profile = profile.monthlyprofile
        except UserProfile.DoesNotExist:
            self.fail("Complete Profile view returned OK, but the UserProfile object is not found.")
        self.assertEqual(profile.name, "Monthly User")
        self.assertEqual(profile.default_language, "C++")
        self.assertEqual(profile.email, "a@b.com")
        self.assertEqual(profile.student_id, "1234567")
        self.assertEqual(profile.qq, "2234567")
        self.assertEqual(profile.cell_phone, "18812341234")
        self.assertEqual(profile.gender, "M")
        self.assertEqual(profile.college, "SJTU")
        self.assertEqual(profile.college_year, "1")

    @skip("need debug")
    def test_register_routine_team(self):
        """Tests if the register a individual user routine works

        The routine is:
        1. User GET the register URL
        2. User POST a form to the register URL, with "username", "password1", "password2",
           and a django.contrib.auth.models.User object is created
        3. User is redirected prompted to choose profile type
        4. User GET the complete profile URL (individual)
        5. User POST a form to the complete profile URL

        """
        # Step 1
        self.assertEqual(response.status_code, 200,
                         "Status code of GET register URL is {}, expected 200".format(response.status_code))
        # Step 2
        register_data = {
            "username": "team_user",
            "password1": "password1",
            "password2": "password1",
        }
        response = self.client.post(self.url, register_data, follow=True)
        self.assertEqual(response.status_code, 200,
                         "Status of POST register URL is {}, expected 200".format(response.status_code))
        self.assertEqual(User.objects.filter(username="team_user").count(), 1,
                         "RegisterView returned OK but can't find the registered User object")
        # Step 3
        self.assertTrue("<h2>Complete Profile</h2>" in response.content,
                        "Can't determine if user is prompted to choose a profile to complete, "
                        "feature string '<h2>Complete Profile</h2>' not found in returned HTML.")
        # Step 4
        response = self.client.get(reverse("complete-team-profile"))
        self.assertEqual(response.status_code, 200,
                         "GET complete-team-profile view status code {}, expected 200".format(response.status_code))
        # Step 5
        leader_user = User.objects.create_user("leader_user", "", "password1")
        member_1 = User.objects.create_user("member_1", "", "password1")
        member_2 = User.objects.create_user("member_2", "", "password1")
        leader_user.save()
        member_1.save()
        member_2.save()
        response = self.client.post(reverse("complete-team-profile"),
                                    {
                                        "name": "Team User",
                                        "default_language": "C++",
                                        "leader": "leader_user",
                                        "member_one": "member_1",
                                        "member_two": "member_2",
                                    },
                                    follow=True)
        self.assertEqual(response.status_code, 200,
                         "GET complete-team-profile view status code {}, "
                         "expected 200".format(response.status_code))
        try:
            profile = UserProfile.objects.get(user__username="team_user")
            profile = profile.teamprofile
        except UserProfile.DoesNotExist:
            self.fail("Complete Profile view returned OK, but the UserProfile object is not found.")
        self.assertEqual(profile.name, "Team User")
        self.assertEqual(profile.default_language, "C++")
        self.assertEqual(profile.leader, leader_user)
        self.assertEqual(profile.member_1, member_1)
        self.assertEqual(profile.member_2, member_2)

    def test_disable_register(self):
        """Tests if RegisterView refuses register when settings disables register"""
        oj_settings = settings.OJ.copy()
        oj_settings["ALLOW_REGISTER"] = False
        with self.settings(OJ=oj_settings):
            # Test GET
            response = self.client.get(reverse("register"))
            self.assertEqual(response.status_code, 403)
            response = self.client.post(reverse("register"), {
                "username": "test_disable_register",
                "password1": "password1",
                "password2": "password1",
            })
            self.assertEqual(response.status_code, 403)
            self.assertEqual(User.objects.all().count(), 0,
                             "A user can manually POST to register even when registration is closed!")
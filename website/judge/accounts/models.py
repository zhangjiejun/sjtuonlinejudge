# vim: set fileencoding=utf8

from django.db import models
from django.contrib.auth.models import User
from django.conf import settings
import hashlib
import base64
import json
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth.models import User, check_password
from django.core.cache import cache

SUPPORTED_LANGUAGES = settings.SUPPORTED_LANGUAGES


class UserProfile(models.Model):
    user = models.OneToOneField(User)
    name = models.CharField(max_length=30, db_index=True)
    default_language = models.CharField(max_length=30, choices=SUPPORTED_LANGUAGES)
    
    def get_subclass(self):
        """Returns a existing subclass of this profile.

        :return: an instance of TeamProfile/IndividualProfile/MonthlyProfile

        """
        try:
            return self.monthlyprofile
        except MonthlyProfile.DoesNotExist:
            pass
        try:
            return self.individualprofile
        except IndividualProfile.DoesNotExist:
            pass
        return self.teamprofile

    @property
    def profile_type(self):
        """Returns the type of this profile ("Individual"/"Team"/"Monthly {{ A/B/C/D }})

        The type is cached for 24 hours.

        """
        cache_key = "profile_type_{}".format(self.user_id)
        type = cache.get(cache_key)
        if not type:
            type = self.get_subclass().profile_type
            cache.set(cache_key, type, 60 * 60 * 24)
        return type

    def __unicode__(self):
        return u"{} (uid={})".format(self.name, self.user_id)


class TeamProfile(UserProfile):
    leader = models.ForeignKey(User, related_name="as_leader", blank=True, null=True)
    member_one = models.ForeignKey(User, related_name="as_member_one", blank=True, null=True)
    member_two = models.ForeignKey(User, related_name="as_member_two", blank=True, null=True)

    @property
    def profile_type(self):
        return "Team"


class IndividualProfile(UserProfile):
    student_id = models.CharField(max_length=30, blank=True)

    @property
    def profile_type(self):
        return "Individual"


class MonthlyProfile(UserProfile):
    email = models.EmailField(max_length=256, blank=True)
    student_id = models.CharField(max_length=30, blank=True)
    cell_phone = models.CharField(max_length=16, blank=True)
    qq = models.CharField(max_length=16, blank=True, verbose_name="QQ")
    gender = models.CharField(max_length=1, choices=(("M", "Male"), ("F", "Female")), db_index=True, blank=True)
    college = models.CharField(max_length=256, db_index=True, blank=True)
    college_year = models.CharField(max_length=1, blank=True,
                                    choices=(("0", "High School or below"),
                                                ("1", "Freshman (1st year)"),
                                                ("2", "Sophomore (2nd year)"),
                                                ("3", "Junior (3rd year)"),
                                                ("4", "Senior (4th year)"),
                                                ("5", "Graduated")))
    type = models.CharField(max_length=1, db_index=True, blank=True,
                            choices=(("A", "A - SJTU Freshman & Sophomore (1st year & 2nd year)"),
                                     ("B", "B - Other SJTU students"),
                                     ("C", "C - Not SJTU Student"),
                                     ("D", "Pending")))
    type_validated = models.BooleanField()

    @property
    def profile_type(self):
        return "Monthly {}".format(self.type)


class OldJudgeUser(models.Model):
    username = models.CharField(max_length=256, db_index=True)
    data = models.TextField()


class OldJudgeUserAuth(object):
    def authenticate(self, username="", password=""):
        if User.objects.filter(username=username).exists():
            user = User.objects.get(username=username)
            login_valid = check_password(password, user.password)
            return user if login_valid else None
        try:
            old_user = OldJudgeUser.objects.get(username=username)
        except ObjectDoesNotExist:
            return None
        info = json.loads(old_user.data)
        if "{SHA}" + base64.encodestring(hashlib.sha1(password).digest()).strip() == info["password"]:
            user = User.objects.create_user(username, "", password)
            profile = MonthlyProfile(email=info["email"] or "a@example.com", student_id="", cell_phone=info["phone_number"] or "",
                                     qq=info["qq"] or "", gender=info["gender"] or "M", college=info["school"] or "",
                                     college_year="", type=info["class_name"] or "D", type_validated=True,
                                     user=user, name=info["realname"] or username, default_language="C++")
            profile.save()
            return user
        return None
    
    def get_user(self, user_id):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None
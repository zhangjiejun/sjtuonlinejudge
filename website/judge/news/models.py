from django.db import models
from django.contrib.auth.models import User
from django.core.urlresolvers import reverse

class News(models.Model):
    author = models.ForeignKey(User)
    post_time = models.DateTimeField(auto_now_add = True, db_index = True)
    title = models.CharField(max_length=80)
    content = models.TextField()
    top = models.BooleanField()
    
    def get_absolute_url(self):
        return reverse("news-detail", args=(self.pk,))
from django.views.generic.list import ListView
from judge.news.models import News
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView
from django.forms.models import ModelForm
from django.forms.widgets import Textarea
from judge.utils import AuthMixin


class NewsForm(ModelForm):
    class Meta:
        model = News
        widgets = {"content": Textarea(attrs={"class": "richtext-textarea"})}


class NewsListView(ListView):
    template_name = "news/news_list.djhtml"
    queryset = News.objects.order_by("-post_time")
    paginate_by = 10


class NewsDetailView(DetailView):
    template_name = "news/news_detail.djhtml"
    model = News


class NewsCreateView(AuthMixin, CreateView):
    template_name = "general_form.djhtml"
    form_class = NewsForm
    permission_required = ["news.create_news"]
    
    def get_initial(self):
        return {"author": self.request.user}


class NewsUpdateView(AuthMixin, UpdateView):
    template_name = "general_form.djhtml"
    form_class = NewsForm
    model = News
    permission_required = ["news.update_news"]
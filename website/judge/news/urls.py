from django.conf.urls import patterns, url
from judge.news.views import NewsListView, NewsDetailView, NewsUpdateView,\
    NewsCreateView

urlpatterns = patterns('judge.submissions.views',
    url(r"^list/$", NewsListView.as_view(), name="news-list"),
    url(r"^post/$", NewsCreateView.as_view(), name="news-create"),
    url(r"^(?P<pk>\d+)/$", NewsDetailView.as_view(), name="news-detail"),
    url(r"^(?P<pk>\d+)/update/$", NewsUpdateView.as_view(), name="news-update"),
)

from judge.utils import smart_render
from datetime import datetime
from django.db.models import Q
from judge.problems.models import Problem
from judge.contests.models import Contest
from judge.accounts.models import UserProfile
from judge.news.models import News
from django.core.urlresolvers import reverse
from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.conf import settings


def homepage(request):
    now = datetime.now()
    base = Contest.access_all(request.user)
    scheduled_contests = base.filter(start_time__gt=now)
    running_contests = base.filter(start_time__lte=now, end_time__gt=now)
    ended_contests = base.filter(end_time__lte=now)
    top_news = News.objects.filter(top=True).order_by("-post_time")[:5]
    return render(request, 'index.djhtml', 
                  {'scheduled_contests': scheduled_contests,
                   'running_contests': running_contests,
                   'ended_contests': ended_contests,
                   "top_news": top_news})
    

def change_theme(request):
    if request.method == 'GET':
        return render(request, 'change_theme.html', {'next_url': request.GET.get('next') or '',
                                                     'choices': settings.INSTALLED_THEMES})
    else:
        theme = request.POST.get('theme')
        choices = zip(*settings.INSTALLED_THEMES)[0]
        if not theme in choices:
            theme = 'basichtml'
        request.session['template_theme'] = theme
        url = request.POST.get('next_url') or reverse('judge.views.homepage')
        return HttpResponseRedirect(url)


def general_search(request):
    if not request.user.is_superuser:
        user_result = UserProfile.objects.filter(user__is_staff=False)
        problem_result = Problem.objects.filter(hidden=False)
        contest_result = Contest.objects.filter(hidden=False)
    else:
        problem_result = Problem.objects.all()
        contest_result = Contest.objects.all()
        user_result = UserProfile.objects.all()
    keywords = request.GET.get('keywords')
    words = keywords.split()[:5] if keywords else []
    for word in words:
        problem_result = problem_result.filter(Q(title__icontains=word) |
                                               Q(source__icontains=word))
        contest_result = contest_result.filter(Q(title__icontains=word) |
                                               Q(source__icontains=word))
        user_result = user_result.filter(Q(name__icontains=word))
    problem_count = problem_result.count()
    problem_result = problem_result[:5] if not request.GET.get('allproblem') else problem_result
    contest_count = contest_result.count()
    contest_result = contest_result[:5] if not request.GET.get('allcontest') else contest_result
    user_count = user_result.count()
    user_result = user_result[:5] if not request.GET.get('alluser') else user_result
    return smart_render(request, 'general_search.html',
                        {'problem_result': problem_result,
                         'problem_result_count': problem_count,
                         'contest_result': contest_result,
                         'contest_result_count': contest_count,
                         'user_result': user_result,
                         'user_result_count': user_count,
                         'keywords': keywords,})


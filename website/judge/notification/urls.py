from django.conf.urls import patterns, url

urlpatterns = patterns(
    "judge.notification.views",
    url(r"^check/$", "check_notification", name="notification-check")
)

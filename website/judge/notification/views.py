import json
from django.http import HttpResponse
import services
from django.core.cache import cache


def check_notification(request):
    if request.user.is_authenticated():
        cache_key = services.generate_cache_key(request.user.username)
        messages = cache.get(cache_key)
        cache.delete(cache_key)
        return HttpResponse(json.dumps(messages), mimetype="application/json")
    return HttpResponse("")

from django.core.cache import cache


def generate_cache_key(username):
    return "notification_for_{}".format(username)


def notify_user(username, message):
    cache_key = generate_cache_key(username)
    messages = cache.get(cache_key)
    if messages is None:
        messages = []
    messages.append(message)
    cache.set(cache_key, messages, 5 * 60 * 60)
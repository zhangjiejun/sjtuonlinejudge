from django.conf.urls import patterns, include, url
from django.contrib import admin

admin.autodiscover()

urlpatterns = patterns('',
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    url(r'^$', 'judge.views.homepage', name="homepage"),
    url(r'^ojdaemon/', include(admin.site.urls)),
    url(r'^problems/', include('judge.problems.urls')),
    url(r'^accounts/', include('judge.accounts.urls')),
    url(r'^submissions/', include('judge.submissions.urls')),
    url(r'^contests/', include('judge.contests.urls')),
    url(r'^news/', include('judge.news.urls')),
    url(r'^notification/', include('judge.notification.urls')),
)
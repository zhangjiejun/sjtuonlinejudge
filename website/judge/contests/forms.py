from django.conf import settings
from django import forms
import judge.contests.models as models


SUPPORTED_LANGUAGES = settings.SUPPORTED_LANGUAGES


class SubmitForm(forms.Form):
    problem_id = forms.CharField()
    language = forms.ChoiceField(choices=SUPPORTED_LANGUAGES)
    code = forms.CharField(widget=forms.Textarea(attrs = {'rows': '14', 'cols': '80'}))
    
    def __init__(self, *args, **kwargs):
        problem_choices = [(i, i) for i in kwargs["problem_ids"]]
        del kwargs["problem_ids"]
        super(SubmitForm, self).__init__(*args, **kwargs)
        self.fields["problem_id"] = forms.ChoiceField(choices=problem_choices)


class ContestForm(forms.ModelForm):
    class Meta:
        model = models.Contest
        exclude = ('shadows', 'problems')

    def clean(self):
        data = super(ContestForm, self).clean()
        start_time = data.get('start_time')
        end_time = data.get('end_time')
        if start_time and end_time and start_time >= end_time:
            msg = 'End time should be later than start time'
            self._errors['start_time'] = self.error_class([msg])
            self._errors['end_time'] = self.error_class([msg])
            del data['start_time']
            del data['end_time']
        min_delay = data.get('min_delay_seconds')
        max_delay = data.get('max_delay_seconds')
        if min_delay and max_delay and min_delay >= max_delay:
            msg = 'Min delay time should be greater than max delay time'
            self._errors['min_delay_seconds'] = self.error_class([msg])
            self._errors['max_delay_seconds'] = self.error_class([msg])
            del data['min_delay_seconds']
            del data['max_delay_seconds']
        return data


class ClarificationForm(forms.ModelForm):
    class Meta:
        model = models.Clarification
        exclude = ("contest", "request_user", "reply", "replied", "public")


class ClarificationUpdateForm(forms.ModelForm):
    class Meta:
        model = models.Clarification
        exclude = ("contest", "request_user")
        widgets = {"reply": forms.Textarea(attrs={"placeholder": "No response."})}


class ImportExternalSubmissionForm(forms.Form):
    HELP_TEXT = ("<p>Should be a JSON serialized string, a list of objects containing following keys:</p>"
                "<pre>+ *time*, int, seconds after contest's beginning when the submission is submitted\n"
                "+ *user*, string, the user's name submitted this submissions\n"
                "+ *problem*, string of 1 char, the problem's letter\n"
                "+ *is_accepted*, bool, indicating whether this submission is accepted\n"
                "+ *code*, optional string, the submission's code\n"
                "</pre>")
    data = forms.CharField(widget=forms.Textarea(attrs = {'rows': '14', 'cols': '80', "class": "code"}))
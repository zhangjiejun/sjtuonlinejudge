from django.conf.urls import patterns, url
from judge.contests.views import *

urlpatterns = patterns('judge.contests.views',
    url(r"^list/$", ContestListView.as_view(), name="contest-list"),
    url(r"^(?P<pk>\d+)/$", ContestDetailView.as_view(), name="contest-detail"),
    url(r"^(?P<pk>\d+)/problem-(?P<letter>[A-Z])/$", "contest_problem_detail", name="contest-problem-detail"),
    url(r"^(?P<pk>\d+)/pid/(?P<pid>\d+)/$", "contest_problem_detail_pid", name="contest-problem-detail-pid"),
    url(r'^(?P<pk>\d+)/problem-(?P<problem>[A-Z])/pdf/$', 'get_pdf', {'disposition': 'inline'}, 'contest-view-pdf'),
    url(r'^(?P<pk>\d+)/problem-(?P<problem>[A-Z])/download-pdf/$', 'get_pdf', {'disposition': 'attachment'}, 'contest-download-pdf'),
    url(r'^(?P<pk>\d+)/problem-(?P<problem>[A-Z])/html/$', 'get_html', {'disposition': 'inline'}, 'contest-view-html'),
    url(r'^(?P<pk>\d+)/problem-(?P<problem>[A-Z])/download-html/$', 'get_html', {'disposition': 'attachment'}, 'contest-download-html'),
    url(r"^(?P<pk>\d+)/submit/$", "submit", name="contest-submit"),
    url(r"^(?P<pk>\d+)/submissions/$", SubmissionListView.as_view(), name="contest-submissions"),
    url(r"^(?P<pk>\d+)/clarification/list/$", ClarificationListView.as_view(), name="contest-clarification-list"),
    url(r"^(?P<pk>\d+)/clarification/create/$", ClarificationCreateView.as_view(), name="contest-clarification-create"),
    url(r"^(?P<pk>\d+)/clarification/(?P<clarification_pk>\d+)/$", ClarificationUpdateView.as_view(), name="contest-clarification-update"),
    url(r"^(?P<pk>\d+)/standings/data/$", "standings_data", name="contest-standings-data"),
    url(r"^shadow-data/$", "shadow_data", name="contest-shadow-data"),
    url(r'^(?P<pk>\d+)/standings/data-submissions/$', "ajax_query_submissions", name='contest-standings-submission'),
    url(r'^(?P<pk>\d+)/standings/$', 'standings', name="contest-standings"),
    url(r'^(?P<pk>\d+)/scorecard-oi/$', 'scorecard_oi', name="contest-scorecard-oi"),
    url(r'^(?P<pk>\d+)/import-shadow/$', ImportShadowView.as_view(), name="contest-import-shadow"),
    url(r'^(?P<pk>\d+)/clear-shadow/$', "clear_shadow", name="contest-clear-shadow"),
    url(r'^(?P<pk>\d+)/edit-problems/$', EditProblemsView.as_view(), name="contest-edit-problems"),
    url(r'^(?P<pk>\d+)/edit-problems-api/$', EditProblemsApiView.as_view(), name="contest-edit-problems-api"),
    url(r'^compare-code/$', 'compare_code', name="compare-code"),
    url(r'^current-time/$', 'current_time', name="current-time"),
    
#    url(r'^(?P<pk>\d+)/view-code/(?P<code_pk>\d+)/$', 'view_code'),
#
#    url(r'^(?P<pk>\d+)/clear-shadow/$', 'clear_shadow'),
#    url(r'^(?P<pk>\d+)/manage-problems/$', 'manage_problems'),
#    url(r'^(?P<pk>\d+)/ajax-update-problems/$', 'ajax_update_problems'),
#
    url(r'^(?P<pk>\d+)/change-visibility/$', 'change_visibility', name="contest-change-visibility"),
#
#
)

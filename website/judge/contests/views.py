from django.shortcuts import get_object_or_404, render
from django.http import HttpResponseRedirect, Http404, HttpResponse, HttpResponseBadRequest
from django.contrib import messages
from django.contrib.auth.decorators import login_required, permission_required
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from django.db.models import Q
from django.conf import settings
from judge.utils import smart_render, display_message,\
    JudgeFormView, JudgeFormMixin, AuthMixin
from judge.problems.models import Problem
from judge.contests.forms import *
from judge.submissions.models import Submission
from middlewares.http403handler import Http403
from judge.contests.models import Contest, ContestProblemConfiguration, Clarification
from datetime import datetime, timedelta
from django.views.generic import View, ListView, DetailView, CreateView, UpdateView, TemplateView
from judge.accounts.models import MonthlyProfile
import judge.notification.services as notification_services
from django.template.loader import render_to_string
from django.core.cache import cache
from django.db import transaction
import services
import json
import base64
import math
import difflib
from django.views.decorators.cache import cache_page
from judge.problems import services as problem_services


def current_time(request):
    ret = json.dumps(datetime.strftime(datetime.now(), "%H:%M:%S"))
    return HttpResponse(
        ret, mimetype="application/json")


class ContestListView(ListView):
    template_name = "contests/contest_list.djhtml"
    paginate_by = 20
    
    def get_queryset(self):
        if self.request.user.is_superuser:
            return Contest.objects.all().order_by("-start_time")
        else:
            return Contest.objects.filter(hidden=False).order_by("-start_time")
        

class ContestContextMixin(object):
    """A mixin adds several common context data.
    
    Usage Example:
        class ContestClarificationListView(ContestContextMixin, ListView):
            ...
    
    contest -- the current contest
    
    """
    def get_context_data(self, **kwargs):
        context = super(ContestContextMixin, self).get_context_data(**kwargs)
        context["contest"] = Contest.objects.checked_get(self.request.user, pk=self.kwargs["pk"])
        return context


class ContestDetailView(DetailView):
    template_name = "contests/contest_detail.djhtml"
    
    def get_queryset(self):
        if self.request.user.is_superuser:
            return Contest.objects.all()
        else:
            return Contest.objects.filter(hidden=False)
    
    def get_context_data(self, **kwargs):
        context = super(ContestDetailView, self).get_context_data(**kwargs);
        context["problem_confs"] = self.get_object().contestproblemconfiguration_set.order_by("problem_letter")
        return context


def contest_problem_detail(request, pk, letter='A'):
    contest = Contest.objects.checked_get(request.user, pk=pk)
    config = get_object_or_404(ContestProblemConfiguration, contest=contest, problem_letter=letter)
    problem = config.problem
    return render(request, 'contests/contest_problem_detail.djhtml',
                  {"contest": contest, "problem": problem, "config": config,
                   "configs": contest.contestproblemconfiguration_set.all()})


def contest_problem_detail_pid(request, pk, pid):
    mapping = services.get_problem_mapping(pk)
    return HttpResponseRedirect(reverse("contest-problem-detail", args=(pk, mapping.get(pid, "A"))))


@login_required
def submit(request, pk, letter=None):
    now = datetime.now()
    contest = Contest.objects.checked_get(request.user, pk=pk)
    if not contest.is_running():
        raise Http403("Contest not running")
    
    # Check if the user can submit
    submissions_last_minute = Submission.objects.filter(user=request.user, submit_time__gte=now - timedelta(minutes=1))
    if submissions_last_minute.count() >= settings.MAX_SUBMISSIONS_PER_MIN:
        raise Http403("Too many submissions during the last minute. " +
                      "You can not submit more than {0} submission(s) " +
                      "within a minute.".format(settings.MAX_SUBMISSIONS_PER_MIN))
    configs = contest.contestproblemconfiguration_set.all().select_related('problem')
    problem_ids = map(chr, range(ord("A"), ord("A") + configs.count()))
    if request.method == "POST":
        # If user is not currently in registrants, add it
        if not contest.registrants.filter(pk=request.user.pk).exists():
            contest.registrants.add(request.user)
        form = SubmitForm(request.POST, problem_ids=problem_ids)
        if form.is_valid():
            data = form.cleaned_data
            problem = ContestProblemConfiguration.objects.get(contest=contest, problem_letter=data["problem_id"]).problem
            submission = Submission.objects.create(contest=contest, problem=problem, user=request.user, code=data['code'], language=data['language'])
            messages.success(request,"Submission received as ID #{0}".format(submission.pk))
            return HttpResponseRedirect(reverse("contest-submissions", args=(pk,)))
    else:
        form = SubmitForm(initial={"contest": pk, "language": request.user.userprofile.default_language}, problem_ids=problem_ids)
        if request.GET.get("id"):
            form.fields["problem_id"].initial = request.GET.get("id") 
    return render(request, "contests/contest_submit.djhtml", 
                  {"form": form,
                   "contest": contest,
                   "configs": configs,
                   "form_help_text": "At most 50KB. ASCII characters only.",
                   "letter": letter})


class SubmissionListView(AuthMixin, ContestContextMixin, ListView):
    template_name = "contests/contest_submissions.djhtml"
    paginate_by = 10
    login_required = True
    
    def get_queryset(self):
        user = self.request.user
        queryset = Submission.objects.filter(user=user, contest__pk=self.kwargs["pk"])
        return queryset.select_related()
    
    def get_context_data(self, **kwargs):
        context = super(SubmissionListView, self).get_context_data(**kwargs)
        context["mapping"] = services.get_problem_mapping(self.kwargs["pk"])
        return context


class ClarificationListView(ContestContextMixin, ListView):
    template_name = "contests/clarification_list.djhtml"
    paginate_by = 10
    
    def get_queryset(self):
        queryset = Clarification.objects.filter(contest__pk=self.kwargs["pk"]).select_related()
        if not self.request.user.is_superuser:
            queryset = queryset.filter(Q(public=True) | Q(request_user__pk=self.request.user.pk))
        return queryset.order_by("-pk")


class ClarificationCreateView(ContestContextMixin, JudgeFormMixin, CreateView):
    model = Clarification
    form_class = ClarificationForm
    login_required = True
    form_title = "Request Clarification"
    
    def form_valid(self, form):
        form.instance.request_user = self.request.user
        form.instance.contest = Contest.objects.get(pk=self.kwargs["pk"])
        self.success_url = reverse("contest-clarification-list", args=(self.kwargs["pk"],))
        # Notify all admins
        for user in User.objects.filter(is_superuser=True):
            notification_services.notify_user(
                user.username,
                "A new clarification message from {}".format(self.request.user.username))
        return super(ClarificationCreateView, self).form_valid(form)


class ClarificationUpdateView(ContestContextMixin, JudgeFormMixin, UpdateView):
    model = Clarification
    form_class = ClarificationUpdateForm
    permissions_required = ["contests.update_clarification"]
    pk_url_kwarg = "clarification_pk"
    form_title = "Update Clarification"
    
    def form_valid(self, form):
        # Notify the user
        if self.object.replied:
            notification_message = u"Clarification {} is responded.\n\n** Question **\n{}\n\n** Response **\n{}".format(
                self.object.pk,
                self.object.question,
                self.object.reply
            )
            if self.object.public:
                contest = Contest.objects.get(pk=self.kwargs["pk"])
                for user in contest.registrants.all():
                    notification_services.notify_user(user.username, notification_message)
            else:
                notification_services.notify_user(self.object.request_user.username, notification_message)
        self.success_url = reverse("contest-clarification-list", args=(self.kwargs["pk"],))
        return super(ClarificationUpdateView, self).form_valid(form)


def get_html(request, pk, problem, disposition):
    contest = Contest.objects.checked_get(request.user, pk=pk)
    config = get_object_or_404(ContestProblemConfiguration, contest=contest, problem_letter=problem)
    problem = config.problem
    if not problem.has_html():
        raise Http404()
    text = problem.htmltext
    response = HttpResponse(mimetype='text/html')
    response['Content-Disposition'] = '{0}; filename={1}.html'.format(disposition, problem)
    problem_services.write_html_text_to_response(text.data, response)
    return response


def get_pdf(request, pk, problem, disposition):
    contest = Contest.objects.checked_get(request.user, pk=pk)
    config = get_object_or_404(ContestProblemConfiguration, contest=contest, problem_letter=problem)
    problem = config.problem
    if not problem.has_pdf():
        raise Http404()
    text = problem.pdftext
    response = HttpResponse(mimetype="application/pdf")
    response['Content-Disposition'] = '{0}; filename={1}.pdf'.format(disposition, problem)
    response.write(base64.decodestring(text.data))
    return response


def standings_data(request, pk):
    """Returns a JSON string for generating standings in local browser.

    :param request: the request
    :param pk: the primary key of the contest

    The data contains following keys:
    + *users*, list of tuples (user_id, user_name), see services.generate_acm_standings_user_list
    + *userType*, list of tuples (user_id, user_type), see services.generate_acm_standings_submission_list
    + *submissions*, list of submissions data, see services.generate_acm_standings_submission_list for details
    + *problemCount*, int, the number of problems

    """
    def generate_new_data(force_frozen=True):
        return {"users": services.generate_acm_standings_user_list(contest),
                "usersType": services.generate_acm_standings_user_type_list(contest),
                "submissions": services.generate_acm_standings_submission_list(contest, force_frozen),
                "problemCount": problem_count}
    # FIXME contest = Contest.objects.checked_get(request.user, pk=pk)
    contest = Contest.objects.get(pk=pk)
    problem_count = ContestProblemConfiguration.objects.filter(contest=contest).count()
    if request.user.is_superuser:
        data = cache.get("acm_privileged_standings_data_{}".format(pk))
        if not data:
            data = generate_new_data(force_frozen=False)
            cache.set("acm_privileged_standings_data_{}".format(pk), data, 5)
    else:
        data = cache.get("acm_standings_data_{}".format(pk))
        if not data:
            data = generate_new_data(force_frozen=True)
            cache.set("acm_standings_data_{}".format(pk), data, 30)
    return HttpResponse(json.dumps(data), content_type="application/json")


@cache_page(60)
def shadow_data(request):
    """Returns a JSON string for covering a shadow over contests.

    This view function expect a "pk" value in querystring, which identifies a contest

    The JSON object contains following fields:
    + *users*, list of tuples (user_id, user_name), see services.generate_acm_standings_user_list
    + *submissions*, list of submissions data, see services.generate_acm_standings_submission_list

    """
    pk = request.GET["pk"]
    external_only = request.GET.get("external_only", False)
    # FIXME contest = Contest.objects.checked_get(pk=pk)
    contest = Contest.objects.get(pk=pk)
    external_shadow_data = services.generate_acm_standings_external_submission_data(contest)
    if not external_only or external_only == "false":
        user_list = services.generate_acm_standings_user_list(contest, shadow_id=pk)
        submission_list = services.generate_acm_standings_submission_list(contest, frozen_enabled=False, shadow_id=pk)
    else:
        user_list, submission_list = [], []
    user_list += external_shadow_data["user_list"]
    submission_list += external_shadow_data["submission_list"]
    data = {"users": list(user_list),
            "submissions": submission_list}
    return HttpResponse(json.dumps(data), content_type="application/json")


def standings_acm(request, contest):
    now = datetime.now()
    current_time = min(now, contest.end_time) - contest.start_time
    total_time =  contest.end_time - contest.start_time

    if request.user.is_active:
        user_type = MonthlyProfile.objects.filter(user=request.user)
        user_type = user_type[0].type if user_type.exists() else None
    else:
        user_type = None

    return render(request, "contests/standings.djhtml", {
        "contest": contest,
        "frozen_minutes": contest.frozen_minutes,
        "frozen": contest.is_frozen(),
        "superuser_mode": contest.is_ended() or request.user.is_superuser,
        "total_minute": int(math.ceil(total_time.total_seconds() / 60)),
        "current_minute": int(math.ceil(current_time.total_seconds()) / 60),
        "current_time": current_time,
        "total_time": total_time,
        "user_type": user_type,
        "shadow_list": [shadow.pk for shadow in contest.shadows.all()]
    })


@permission_required("admin")
def standings_oi(request, contest):
    table_head, table_body = services.standings.compute_OI_standings(contest.pk)
    return render(request, "contests/standings_oi.djhtml", {
        "table_head": table_head,
        "table_body": table_body,
        "contest": Contest.objects.get(pk=contest.pk),
    })


@permission_required("admin")
def scorecard_oi(request, pk):
    scorecard_data = services.standings.generate_scorecard(pk)
    result = render_to_string("contests/scorecard_oi.tex", {"data": scorecard_data})
    response = HttpResponse(mimetype='text/tex')
    response['Content-Disposition'] = 'attachment; filename=scorecard.tex'
    response.write(result)
    return response


def standings(request, pk, now=None):
    contest = Contest.objects.checked_get(request.user, pk=pk)
    if contest.contest_type == Contest.OI:
        return standings_oi(request, contest)
    else:
        return standings_acm(request, contest)


def ajax_query_submissions(request, pk):
    contest = Contest.objects.checked_get(request.user, pk=pk)
    if not contest.is_ended():
        if not request.user.is_staff:
            raise Http403()
    user = request.GET.get('user')
    problem = request.GET.get('problem')
    mapping = {v: k for k, v in services.get_problem_mapping(pk).items()}
    problem = mapping.get(problem)
    if not problem:
        return HttpResponseBadRequest("Problem does not exist.")
    delta = timedelta(0)
    qs = Submission.objects.filter(contest_id=pk, user_id=user, problem_id=problem).select_related().order_by('submit_time')
    if not qs.count():
        for shadow in contest.shadows.all():
            qs = Submission.objects.filter(contest=shadow, user_id=user, problem_id=problem).select_related().order_by('submit_time')
            if qs.count():
                delta = contest.start_time - shadow.start_time
                break
    ret = []
    for s in qs:
        seconds = int((s.submit_time - contest.start_time + delta).total_seconds())
        ret.append({
            'pk': s.pk,
            'time': '%d:%02d:%02d' % (seconds/3600, seconds/60 % 60, seconds%60),
            'verdict': "Waiting" if not (s.judgement and s.judgement.valid) else s.judgement.judgement,
        })
    return HttpResponse(json.dumps(ret), mimetype='application/json')


def compare_code(request):
    code1_id = request.GET.get('code1')
    code2_id = request.GET.get('code2')
    code1 = Submission.objects.checked_get(request.user, pk=code1_id)
    code2 = Submission.objects.checked_get(request.user, pk=code2_id)
    d = difflib.HtmlDiff(tabsize=4)
    return HttpResponse(d.make_file(code1.code.split('\n'), code2.code.split('\n')))


class ImportShadowView(ContestContextMixin, JudgeFormView):
    form_title = "Import External Shadow"
    form_class = ImportExternalSubmissionForm
    permission_required = ["submissions.add_externalsubmission"]

    def form_valid(self, form):
        data = form.cleaned_data
        contest = Contest.objects.get(pk=self.kwargs["pk"])
        with transaction.commit_on_success():
            imported = services.import_external_submission(contest, data["data"])
        messages.success(self.request,
                         "Successfully added {} external submissions. Because the result is"
                         "cached you may need to wait up to 1 minute to see them.".format(imported))
        return HttpResponseRedirect(reverse("contest-import-shadow", args=(self.kwargs["pk"],)))


class EditProblemsApiView(View):
    def get(self, request, *args, **kwargs):
        action = request.GET.get("action", "")
        if hasattr(self, action) and request.user.is_superuser:
            action_function = getattr(self, action)
            return action_function(request, *args, **kwargs)
        return HttpResponseBadRequest("BAD GET ACTION: {}".format(action))

    def post(self, request, *args, **kwargs):
        action = request.POST.get("action", "")
        if hasattr(self, action) and request.user.is_superuser:
            action_function = getattr(self, action)
            return action_function(request, *args, **kwargs)
        return HttpResponseBadRequest("BAD POST ACTION: {}".format(action))

    def get_problem_list(self, request, *args, **kwargs):
        """A get action that returns a list of problems in format (letter, problem_id, title)."""
        contest = Contest.objects.checked_get(request.user, pk=kwargs["pk"])
        config_list = ContestProblemConfiguration.objects.filter(contest=contest).select_related()
        problem_list = [(config.problem_id, config.problem.title) for config in config_list]
        return HttpResponse(json.dumps(problem_list), mimetype="application/json")

    def update_problem_list(self, request, *args, **kwargs):
        """A post action that update the problem list"""
        contest = Contest.objects.checked_get(request.user, pk=kwargs["pk"])
        problems = json.loads(request.POST["problem_list"])
        for pk in problems:
            if not Problem.objects.checked_filter(request.user, pk=pk).exists():
                return HttpResponseBadRequest('Bad problem ID {}'.format(pk))
        ContestProblemConfiguration.objects.filter(contest=contest).delete()
        letter = "A"
        for pk in problems:
            problem_obj = Problem.objects.get(pk=pk)
            ContestProblemConfiguration.objects.create(problem=problem_obj,
                                                       problem_letter = letter,
                                                       contest=contest)
            letter = chr(ord(letter) + 1)
        return HttpResponse('OK')


class EditProblemsView(AuthMixin, ContestContextMixin, TemplateView):
    permission_required = ["contests.change_contest"]
    template_name = "contests/edit_problems.djhtml"


@permission_required('submissions.delete_externalshadow')
def clear_shadow(request, pk):
    contest = Contest.objects.get(pk=pk)
    contest.externalsubmission_set.all().delete()
    messages.add_message(request, messages.SUCCESS, 'Shadows are cleared successfully')
    return HttpResponseRedirect(request.META['HTTP_REFERER'] or "/")


@permission_required('contest.change_contest', raise_exception=True)
def ajax_update_problems(request, pk):
    contest = Contest.objects.checked_get(request.user, pk=pk)
    problems = json.loads(request.POST.get('problems'))
    for problem in problems:
        pk = problem.get('problem_id')
        if not Problem.objects.filter(pk=pk).exists():
            return HttpResponse('Bad problem ID {}'.format(pk), status=500)
    ContestProblemConfiguration.objects.filter(contest=contest).delete()
    for problem in problems:
        problem_obj = Problem.objects.get(pk=problem['problem_id'])
        ContestProblemConfiguration.objects.create(problem=problem_obj,
                                                   problem_letter = problem['letter'],
                                                   contest=contest)
    return HttpResponse('OK')


@permission_required('contests.change_contest')
def manage_problems(request, pk):
    contest = Contest.objects.checked_get(request.user, pk=pk)
    configs = contest.contestproblemconfiguration_set.all()
    return smart_render(request, 'contests/in_contest/manage_problem.html',
                        {'contest': contest,
                         'configs': configs})


@permission_required('problems.change_problem')
def change_visibility(request, pk):
    hidden = request.GET.get("hidden", False) == "true"
    contest = Contest.objects.checked_get(request.user, pk=pk)
    configs = contest.contestproblemconfiguration_set.all()
    qs = Problem.objects.filter(pk__in=[c.problem_id for c in configs])
    qs.update(hidden=hidden)
    return display_message(request, 'Operation Succeeded', 'All problems are set hidden={} now'.format(hidden))

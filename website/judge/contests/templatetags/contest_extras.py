from django import template
from django.conf import settings
import pytz

register = template.Library()


def access_dict(value, arg):
    return value.get(arg)


def duration(delta):
    seconds = int(delta.total_seconds() + 1e-9)
    return "{:d}:{:02d}:{:02d}".format(seconds / 3600, seconds / 60 % 60, seconds % 60)


def date_iso_format(date):
    timezone = pytz.timezone(settings.TIME_ZONE)
    date = timezone.localize(date)
    return date.isoformat()


register.filter("dict", access_dict)
register.filter("duration", duration)
register.filter("date_iso_format", date_iso_format)
from judge.contests.models import ContestProblemConfiguration, Contest
from django.contrib.auth.models import User
from judge.submissions.models import Submission, ExternalSubmission
from django.core.cache import cache
from datetime import timedelta
import json


def get_problem_mapping(pk):
    """Returns a mapping from problem ID to its letter in contest"""
    mapping = cache.get("contest_{}_problem_mapping".format(pk))
    if not mapping:
        mapping = {}
        configs = ContestProblemConfiguration.objects.filter(contest__pk=pk)
        for config in configs:
            mapping[config.problem_id] = config.problem_letter
        cache.set("contest_{}_problem_mapping", mapping, 60)
    return mapping


def get_reversed_problem_mapping(pk):
    """Returns a mapping from its letter in contest to problem ID"""
    return {k: v for k, v in get_problem_mapping(pk).items()}


def sync_contest_registrants(pk):
    """Add users that submitted in this contest to its registrants"""
    contest = Contest.objects.get(pk=pk)
    user_ids = Submission.objects.filter(contest_id=pk).select_related().values_list("user_id").distinct().order_by("user__id")
    for user_id in user_ids:
        contest.registrants.add(User.objects.get(pk=user_id[0]))


def import_external_submission(contest, data):
    """Import a list of external submissions to this contest.

    External Submissions are submissions that will be counted in Standings, but is not submitted in this online judge.

    This function expects a list of dict, which contains following keys:
    + *time*, int, seconds after contest's beginning when the submission is submitted
    + *user*, string, the user's name submitted this submissions
    + *problem*, string of 1 char, the problem's letter
    + *is_accepted*, bool, indicating whether this submission is accepted
    + *code*, optional string, the submission's code

    :param data: string, json serialized list, external submission data

    """
    ext_list = []
    data = json.loads(data)
    for entry in data:
        ext_list.append(ExternalSubmission(
            contest=contest,
            time=contest.start_time + timedelta(seconds=entry["time"]),
            name=entry["user"],
            problem=entry["problem"],
            is_accepted=entry["is_accepted"],
            code=entry.get("code", ""),
        ))
    ExternalSubmission.objects.bulk_create(ext_list)
    return len(ext_list)
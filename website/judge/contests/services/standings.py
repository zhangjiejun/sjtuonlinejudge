from datetime import datetime, timedelta
from judge.submissions.models import Submission, TestCaseResult, ExternalSubmission
from judge.contests.models import Contest, ContestProblemConfiguration
from judge.problems.models import TestCase
from .common import get_problem_mapping


def generate_acm_standings_external_submission_data(contest, frozen_enabled=True):
    """Generate the external submission list and user list of a contest

    :param contest:
    :param frozen_enabled:

    """
    name_id_mapping = {}
    id_counter = 0
    submission_list = []
    for submission in ExternalSubmission.objects.filter(contest=contest):
        if submission.name not in name_id_mapping:
            id_counter += 1
            name_id_mapping[submission.name] = "se{}_{}".format(contest.pk, id_counter)
        submission_list.append({
            "user": name_id_mapping[submission.name],
            "problem": submission.problem,
            "pending": False,
            "ac": submission.is_accepted,
            "time": int((submission.time - contest.start_time).total_seconds())
        })
    return {"user_list": [(uid, name) for name, uid in name_id_mapping.items()],
            "submission_list": submission_list}


def generate_acm_standings_submission_list(contest, frozen_enabled=True, shadow_id=None):
    """Generate the submission list of a contest

    Returns a list of dict, which contains following keys:
    + *user*, a string, the user's ID of this submission
    + *problem*, a string, the problem's ID of this submission
    + *pending*, a bool, indicating whether this submission is already judged
    + *ac*, a bool, whether this submission is accepted
    + *time*, a int, the time (in seconds) after contest's beginning when this submission is submitted

    :param contest: the contest object
    :param frozen_enabled: a bool indicating whether frozen is on
    :param shadow_id: a string, non-None value means to generate for shadow with this ID
    :return: a list of submissions

    """
    mapping = get_problem_mapping(contest.pk)
    queryset = (Submission.objects.filter(contest=contest)
                .select_related("judgement", "problem", "contest", "user", "user__userprofile")
                .order_by("submit_time"))
    now = datetime.now()
    submission_list = []
    if frozen_enabled:
        frozen_time = contest.get_frozen_time()
    for s in queryset:
        problem = mapping.get(s.problem_id)
        if not problem:
            # this problem does not belong to this contest at all
            # ignore this submission
            continue
        if s.judgement and s.judgement.judgement == "Compilation Error":
            continue
        pending = not s.judgement or not s.judgement.valid
        if frozen_enabled:
            pending = pending or contest.is_running() and s.submit_time >= frozen_time
        ac = not pending and (s.judgement.judgement == "Accepted")
        submission_list.append({"user": s.user_id,
                                "problem": mapping.get(s.problem_id),
                                "pending": pending,
                                "ac": ac,
                                "time": int((s.submit_time - contest.start_time).total_seconds())})
    if shadow_id:
        for entry in submission_list:
            entry["user"] = "s{}_{}".format(shadow_id, entry["user"])
    return submission_list


def generate_acm_standings_user_list(contest, shadow_id=None):
    """Returns the list of participants for a contest.

    :param contest: the contest object
    :param shadow_id: generate as user list of shadow

    The list's entries are like (user_id, user_name), where user_id is a unique identification string, and
    user_name is a string for display.

    """
    if shadow_id:
        def generate_entry(user):
            return "s{}_{}".format(shadow_id, user.pk), user.userprofile and user.userprofile.name
    else:
        def generate_entry(user):
            return user.pk, user.userprofile and user.userprofile.name
    return [generate_entry(user) for user in contest.registrants.all().select_related("userprofile")]


def generate_acm_standings_user_type_list(contest):
    """Returns the list of participants for a contest.

    The list's entries are like (user_id, user_name), where user_id is a unique identification string, and
    user_name is a string for display.

    :param contest: the contest object

    """
    def generate_entry(user):
        return user.pk, user.userprofile.profile_type
    return [generate_entry(user) for user in contest.registrants.all().select_related("userprofile")]


def compute_oi_standings(contest_pk):
    """Compute OI contest standings.

    ATTENTION! THIS FUNCTION SUFFERS SEVERE PERFORMANCE ISSUE (TOO MANY DATABASE QUERIES). SHOULD MAKE AVAILABLE TO
    ADMINS ONLY!

    :param contest_pk: the primary key of the contest
    :returns: a tuple (table_head, table_body) which are list and list of lists

    """
    contest = Contest.objects.get(pk=contest_pk)
    problem_config_list = ContestProblemConfiguration.objects.filter(contest_id=contest_pk).order_by("problem_letter")
    user_list = [(user.id, user.userprofile.name) for user in contest.registrants.all()]
    table_head = [u"Name", u"Total Score",] + [i.problem.title for i in problem_config_list]
    table_body = []
    for user_id, user_name in user_list:
        scores = []
        for problem_config in problem_config_list:
            submission = Submission.objects.filter(
                contest_id=contest_pk,
                user_id=user_id,
                problem=problem_config.problem
            ).order_by("-pk")
            score = 0
            if submission.exists():
                submission = submission[0]
                if submission.judgement and submission.judgement.valid:
                    test_cases = TestCase.objects.filter(problem=problem_config.problem)
                    accepted_results = TestCaseResult.objects.filter(submission=submission).filter(judgement="Accepted")
                    score = accepted_results.count() / max(1, float(test_cases.count())) * 100
            scores.append(score)
        table_body.append([user_name, sum(scores)] + map(str, scores))
    table_body.sort(cmp=lambda r1, r2: -cmp(r1[1], r2[1]))
    return table_head, table_body


def generate_oi_scorecard(contest_pk):
    """Generate OI scorecard in LaTeX format for a contest.

    ATTENTION! THIS FUNCTION SUFFERS SEVERE PERFORMANCE ISSUE (TOO MANY DATABASE QUERIES). SHOULD MAKE AVAILABLE TO
    ADMINS ONLY!

    :param contest_pk: the primary key of the contest
    :returns: a list of dict

    """
    contest = Contest.objects.get(pk=contest_pk)
    problem_config_list = ContestProblemConfiguration.objects.filter(contest_id=contest_pk).order_by("problem_letter")
    user_list = [(user.id, user.userprofile.name) for user in contest.registrants.all()]
    result = []
    for user_id, user_name in user_list:
        scores = []
        problem_stat = []
        total_submissions = 0
        total_time = 0
        for problem_config in problem_config_list:
            submissions = Submission.objects.filter(
                contest_id=contest_pk,
                user_id=user_id,
                problem=problem_config.problem
            ).exclude(judgement__judgement="Compilation Error").order_by("-pk").select_related()
            score = 0
            last_submit_time = None
            test_results = None

            if submissions.exists():
                submission = submissions[0]
                total_submissions += submissions.count()
                last_submit_time = int((submission.submit_time - contest.start_time).total_seconds())
                total_time += last_submit_time
                if submission.judgement and submission.judgement.valid:
                    test_cases = TestCase.objects.filter(problem=problem_config.problem)
                    test_results = [i.judgement[0] for i in TestCaseResult.objects.filter(submission=submission)]
                    accepted_results = TestCaseResult.objects.filter(submission=submission).filter(judgement="Accepted")
                    score = accepted_results.count() / max(1, float(test_cases.count())) * 100
                    test_results = "".join(test_results)
                    test_results = [test_results[i:i+20] for i in range(0, len(test_results), 20)]
            scores.append(score)
            problem_stat.append({
                "problem_title": problem_config.problem.title,
                "submission_count": submissions.count(),
                "submission_time": "n/a" if not last_submit_time else "{:02}:{:02}:{:02}".format(
                    last_submit_time / 3600,
                    last_submit_time / 60 % 60,
                    last_submit_time % 60),
                "test_results": test_results or ["n/a"],
                "score": score
            })
        result.append({
            "name": user_name,
            "total_score": sum(scores),
            "total_submission": total_submissions,
            "total_time": "{:02}:{:02}:{:02}".format(
                    total_time / 3600,
                    total_time / 60 % 60,
                    total_time % 60),
            "problem_stat": problem_stat
        })
    return result
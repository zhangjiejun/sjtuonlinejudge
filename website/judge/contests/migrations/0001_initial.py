# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Contest'
        db.create_table(u'contests_contest', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=256, db_index=True)),
            ('start_time', self.gf('django.db.models.fields.DateTimeField')(db_index=True)),
            ('end_time', self.gf('django.db.models.fields.DateTimeField')(db_index=True)),
            ('min_delay_seconds', self.gf('django.db.models.fields.IntegerField')()),
            ('max_delay_seconds', self.gf('django.db.models.fields.IntegerField')()),
            ('frozen_minutes', self.gf('django.db.models.fields.IntegerField')()),
            ('source', self.gf('django.db.models.fields.CharField')(db_index=True, max_length=256, blank=True)),
            ('hidden', self.gf('django.db.models.fields.BooleanField')(default=False, db_index=True)),
        ))
        db.send_create_signal(u'contests', ['Contest'])

        # Adding M2M table for field shadows on 'Contest'
        m2m_table_name = db.shorten_name(u'contests_contest_shadows')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('from_contest', models.ForeignKey(orm[u'contests.contest'], null=False)),
            ('to_contest', models.ForeignKey(orm[u'contests.contest'], null=False))
        ))
        db.create_unique(m2m_table_name, ['from_contest_id', 'to_contest_id'])

        # Adding model 'ContestProblemConfiguration'
        db.create_table(u'contests_contestproblemconfiguration', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('problem', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['problems.Problem'])),
            ('contest', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['contests.Contest'])),
            ('problem_letter', self.gf('django.db.models.fields.CharField')(max_length=10)),
        ))
        db.send_create_signal(u'contests', ['ContestProblemConfiguration'])

        # Adding model 'Clarification'
        db.create_table(u'contests_clarification', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('contest', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['contests.Contest'])),
            ('request_user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
            ('related_problem', self.gf('django.db.models.fields.CharField')(db_index=True, max_length=1, blank=True)),
            ('question', self.gf('django.db.models.fields.TextField')()),
            ('reply', self.gf('django.db.models.fields.TextField')(blank=True)),
            ('replied', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('public', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'contests', ['Clarification'])


    def backwards(self, orm):
        # Deleting model 'Contest'
        db.delete_table(u'contests_contest')

        # Removing M2M table for field shadows on 'Contest'
        db.delete_table(db.shorten_name(u'contests_contest_shadows'))

        # Deleting model 'ContestProblemConfiguration'
        db.delete_table(u'contests_contestproblemconfiguration')

        # Deleting model 'Clarification'
        db.delete_table(u'contests_clarification')


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'contests.clarification': {
            'Meta': {'object_name': 'Clarification'},
            'contest': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contests.Contest']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'public': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'question': ('django.db.models.fields.TextField', [], {}),
            'related_problem': ('django.db.models.fields.CharField', [], {'db_index': 'True', 'max_length': '1', 'blank': 'True'}),
            'replied': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'reply': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'request_user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"})
        },
        u'contests.contest': {
            'Meta': {'ordering': "['-id']", 'object_name': 'Contest'},
            'end_time': ('django.db.models.fields.DateTimeField', [], {'db_index': 'True'}),
            'frozen_minutes': ('django.db.models.fields.IntegerField', [], {}),
            'hidden': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'db_index': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'max_delay_seconds': ('django.db.models.fields.IntegerField', [], {}),
            'min_delay_seconds': ('django.db.models.fields.IntegerField', [], {}),
            'problems': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['problems.Problem']", 'through': u"orm['contests.ContestProblemConfiguration']", 'symmetrical': 'False'}),
            'shadows': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'shadows_rel_+'", 'blank': 'True', 'to': u"orm['contests.Contest']"}),
            'source': ('django.db.models.fields.CharField', [], {'db_index': 'True', 'max_length': '256', 'blank': 'True'}),
            'start_time': ('django.db.models.fields.DateTimeField', [], {'db_index': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '256', 'db_index': 'True'})
        },
        u'contests.contestproblemconfiguration': {
            'Meta': {'ordering': "['contest', 'problem_letter']", 'object_name': 'ContestProblemConfiguration'},
            'contest': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contests.Contest']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'problem': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['problems.Problem']"}),
            'problem_letter': ('django.db.models.fields.CharField', [], {'max_length': '10'})
        },
        u'problems.problem': {
            'Meta': {'ordering': "['id']", 'object_name': 'Problem'},
            'hidden': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'db_index': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'input_file': ('django.db.models.fields.CharField', [], {'max_length': '256', 'blank': 'True'}),
            'output_file': ('django.db.models.fields.CharField', [], {'max_length': '256', 'blank': 'True'}),
            'residing_judge': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'residing_judge_id': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'source': ('django.db.models.fields.CharField', [], {'db_index': 'True', 'max_length': '256', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '256', 'db_index': 'True'})
        }
    }

    complete_apps = ['contests']
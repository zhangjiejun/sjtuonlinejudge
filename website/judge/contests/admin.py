from django.contrib import admin
import judge.contests.models as models

class ContestAdmin(admin.ModelAdmin):
    filter_horizontal = ('shadows', "registrants")
    list_display = ('pk', 'title', 'source', 'start_time', 'get_duration', 'frozen_minutes', 'hidden', )
    list_filter = ('hidden', )
    search_fields = ('title', 'source', )
    save_as = True

admin.site.register(models.Contest, ContestAdmin)
admin.site.register(models.ContestProblemConfiguration)
admin.site.register(models.Clarification)

from django.db import models
from judge.problems.models import Problem
from django.shortcuts import get_object_or_404
from django.contrib.auth.models import User
from datetime import datetime, timedelta
from middlewares.http403handler import Http403


class ContestManager(models.Manager):
    def checked_get(self, user, *args, **kwargs):
        obj = super(ContestManager, self).get(*args, **kwargs)
        if user.is_superuser:
            return obj
        if obj.hidden or obj.is_scheduled():
            raise Http403
        return obj
    
    def checked_all(self, user, *args, **kwargs):
        queryset = super(ContestManager, self).all()
        if user.is_superuser:
            return queryset
        return queryset.filter(hidden=False)


class Contest(models.Model):
    ### Constant section ###
    # following constants defines contest types
    ACM_ICPC = 0
    OI = 1
    CONTEST_TYPES = (
        (ACM_ICPC, "ACM-ICPC"),
        (OI, "OI")
    )

    ### Field section ###
    title = models.CharField(max_length = 256, db_index = True)
    problems = models.ManyToManyField(Problem, through='ContestProblemConfiguration')
    start_time = models.DateTimeField(db_index = True)
    end_time = models.DateTimeField(db_index = True)
    min_delay_seconds = models.IntegerField()
    max_delay_seconds = models.IntegerField()
    frozen_minutes = models.IntegerField()
    shadows = models.ManyToManyField('self', blank=True)
    source = models.CharField(max_length=256, blank=True, db_index=True)
    hidden = models.BooleanField(db_index=True)
    contest_type = models.IntegerField(choices=CONTEST_TYPES, default=ACM_ICPC, db_index=True)
    registrants = models.ManyToManyField(User, blank=True)
    
    ### Define custom manager ###
    objects = ContestManager()

    class Meta:
        ordering = ['-id']

    def __unicode__(self):
        return self.title

    def get_duration(self):
        return self.end_time - self.start_time 

    def get_frozen_time(self):
        return self.end_time - timedelta(minutes=self.frozen_minutes)

    def get_time_passed(self):
        return min(datetime.now().replace(microsecond=0), self.end_time) - self.start_time
    
    def is_ended(self):
        return datetime.now() > self.end_time

    def is_scheduled(self):
        return datetime.now() < self.start_time

    def is_running(self):
        return not self.is_ended() and not self.is_scheduled()

    def is_frozen(self):
        return self.is_running() and datetime.now() >= (self.end_time - timedelta(minutes=self.frozen_minutes))
    
    def get_status(self):
        now = datetime.now()
        if now >= self.end_time:
            return "Ended"
        return "Scheduled" if now < self.start_time else "Running" 

    @classmethod
    def access(cls, user, pk):
        contest = get_object_or_404(cls, pk=pk)
        if not user.is_superuser:
            if contest.hidden or contest.is_scheduled():
                raise Http403
        return contest
    
    @classmethod
    def access_all(cls, user):
        if not user.is_superuser:
            return cls.objects.filter(hidden=False)
        else:
            return cls.objects.all()


class ContestProblemConfiguration(models.Model):
    problem = models.ForeignKey(Problem)
    contest = models.ForeignKey(Contest)
    problem_letter = models.CharField(max_length=10)

    def __unicode__(self):
        return '%s. %s. %s' % (self.contest.title, self.problem_letter, self.problem.title)
    
    class Meta:
        ordering = ['contest', 'problem_letter']


class Clarification(models.Model):
    contest = models.ForeignKey(Contest, db_index=True)
    request_user = models.ForeignKey(User, db_index=True)
    related_problem = models.CharField(db_index=True, blank=True, max_length=1)
    question = models.TextField()
    reply = models.TextField(blank=True)
    replied = models.BooleanField(default=False)
    public = models.BooleanField(default=False)
    
    def __unicode__(self):
        return unicode(self.contest) + u' - ' + unicode(self.question)

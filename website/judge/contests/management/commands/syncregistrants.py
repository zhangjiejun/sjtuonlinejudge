from django.core.management.base import BaseCommand, CommandError
from judge.contests.models import Contest
from judge.contests import services


class Command(BaseCommand):
    args = "[<contest_id contest_id ...>]"
    help = "Add all user that submitted a code in contest to the contest's registrants"

    def handle(self, *args, **kwargs):
        if args:
            for contest_id in args:
                services.sync_contest_registrants(contest_id)
                self.stdout.write("Successfully synced {}".format(contest_id))
        else:
            for contest in Contest.objects.all():
                services.sync_contest_registrants(contest.pk)
                self.stdout.write("Successfully synced {}".format(contest.id))
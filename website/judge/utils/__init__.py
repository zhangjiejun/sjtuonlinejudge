from django.shortcuts import render
from django.contrib import messages
from django.http import HttpResponseRedirect
from django.conf import settings
from django.views.generic.edit import FormView
from django import forms
from django.views.generic.list import ListView
import upload
import re


class Http403(Exception):
    pass


class AuthMixin(object):
    """Adds login-required & permission-required to a class-based view
    
    Attributes:
    (boolean) login_required -- Whether the view requires login
    (list) permission_required -- A list of permissions required by the view
    
    """
    login_required = False
    permission_required = []
    
    def dispatch(self, request, *args, **kwargs):
        if self.login_required and self.request.user.is_anonymous():
            return HttpResponseRedirect(settings.LOGIN_URL + "?next=" + self.request.get_full_path())
        for permission in self.permission_required:
            if not self.request.user.has_perm(permission):
                raise Http403("Permission Denied: " + permission)
        return super(AuthMixin, self).dispatch(request, *args, **kwargs)


class JudgeFormMixin(AuthMixin):
    """A mixin adds several utility form attributes to a form view, inherits AuthMixin
    
    Attrubutes:
    (str) template_name -- Default template "general_form.djhtml"
    (str) form_help_text -- A paragraph of help text for this form
    (str) form_title -- The title of this form
    
    """ 
    template_name = "general_form.djhtml"
    form_help_text = ""
    form_title = ""
    is_wide_form = False
    no_cancel = False
    
    def get_form_title(self):
        return self.form_title
    
    def get_form_help_text(self):
        if self.form_help_text:
            return self.form_help_text
        if hasattr(self, "form_class") and hasattr(self.form_class, "HELP_TEXT"):
            return self.form_class.HELP_TEXT
        return ""
    
    def get_context_data(self, **kwargs):
        context = super(JudgeFormMixin, self).get_context_data(**kwargs)
        context["form_title"] = self.get_form_title()
        context["form_help_text"] = self.get_form_help_text()
        context["no_cancel"] = self.no_cancel
        context["is_wide_form"] = self.is_wide_form
        return context


def singleton(cls):
    instances = {}

    def getinstance():
        if cls not in instances:
            instances[cls] = cls()
        return instances[cls]
    return getinstance


class JudgeFormView(JudgeFormMixin, FormView):
    pass


class FilteringListView(ListView):
    def get_context_data(self, **kwargs):
        context = super(FilteringListView, self).get_context_data(**kwargs)
        get = self.request.GET.copy()
        get.pop("page", None)
        context["url_base"] = get.urlencode()
        return context


class GeneralUploadForm(forms.Form):
    file = forms.FileField()


def sort_by_numbers(numbers):
    """This function will sort a list using numeric order so the result will be more natural

    For example, in a regular sort comparison, "test10.in" < "test2.in". However the more natural
    comparison should be "test2.in" < "test10.in". This sort function will do the later comparison.

    Parameters:
    numbers -- a list of strings to sort.

    """
    numbers.sort(lambda x, y: cmp(map(int, re.findall(r'\d+', x)), map(int, re.findall(r'\d+', y))))


""" DEPRECATED BELOW """


def smart_render(request, template, *args, **kwargs):
    template = template.replace(".html", ".djhtml")
    return render(request, template, *args, **kwargs)


def smart_render_to_response(request, template, *args, **kwargs):
    if kwargs.has_key('context_instance'):
        del kwargs['context_instance']
    return smart_render(request, template, *args, **kwargs)
    

def add_message_and_return(request, message, level = messages.INFO):
    messages.add_message(request, level, message)
    return HttpResponseRedirect(request.META.get('HTTP_REFERER') or '/')


def display_message(request, title, content, status=200):
    return render(request, 'message.djhtml', {'title': title, 'content': content}, status=status)


def settings_context_processor(request):
    return {"oj_settings": settings.OJ}
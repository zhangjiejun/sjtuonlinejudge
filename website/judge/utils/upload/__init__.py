"""This module contains utility functions for manipulating uploaded files"""


__author__ = 'zhangjiejun1992'


import tempfile
import zipfile
import shutil


def save_upload_to_file(from_fp, dest_fp=None):
    """Save the uploaded file from_fp to a destination file object dest_fp.

    Parameters:
    from_fp -- The file pointer of uploaded file to save
    dest_fp -- the destination file-like object

    Return value:
    The destination file pointer.

    """
    for chunk in from_fp.chunks():
        dest_fp.write(chunk)
    dest_fp.seek(0)
    return dest_fp


def unzip_upload_to_dir(zip_fp, dest_dir):
    """Unzip a uploaded ZIP file to a destination directory.

    Parameters:
    fp -- the file pointer of uploaded ZIP
    dest_dir -- the destination directory

    """

    with tempfile.NamedTemporaryFile() as temp_fp:
        save_upload_to_file(zip_fp, temp_fp)
        zipfile_obj = zipfile.ZipFile(temp_fp, "r")
        zipfile_obj.extractall(dest_dir)
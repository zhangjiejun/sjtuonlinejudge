from django.db import models
from django.contrib.auth.models import User
from judge.contests.models import Contest
from judge.problems.models import Problem, TestCase
from django.shortcuts import get_object_or_404
from middlewares.http403handler import Http403
from datetime import datetime
from django.conf import settings
from django.db.models.signals import post_save
from django.db import transaction
from django.core.urlresolvers import reverse
from django.http import Http404
from django.db.models import Q


SUPPORTED_LANGUAGES = settings.SUPPORTED_LANGUAGES


VERDICT_CHOICES = (
    ('Waiting', 'Waiting'),
    ('Accepted', 'Accepted'),
    ('Wrong Answer', 'Wrong Answer'),
    ('Runtime Error', 'Runtime Error'),
    ('Memory Limit Exceeded', 'Memory Limit Exceeded'),
    ('Time Limit Exceeded', 'Time Limit Exceeded'),
    ('Output Limit Exceeded', 'Output Limit Exceeded'),
    ('Compilation Error', 'Compilation Error'),
    ('Contact Staff', 'Contact Staff'),
)


class SubmissionManager(models.Manager):
    def checked_get(self, user, *args, **kwargs):
        try:
            submission = self.get(*args, **kwargs)
            if not user.is_superuser:
                contest = submission.contest
                deny = contest and contest.is_running() and not user == submission.user
                deny = deny or not settings.OJ["PUBLIC_SUBMISSIONS"] and user != submission.user
                deny = deny or submission.user.is_superuser
                if deny:
                    raise Http403("Permission denied")
            return submission
        except Submission.DoesNotExist:
            raise Http404()

    def checked_filter(self, user, *args, **kwargs):
        queryset = self.checked_all().filter(*args, **kwargs)
        return queryset

    def checked_all(self, user):
        queryset = self.all()
        if not user.is_superuser:
            if user.is_authenticated() and not user.is_anonymous():
                queryset = queryset.filter(
                    (Q(contest=None)
                     | (Q(contest__end_time__lte=datetime.now()) & Q(contest__hidden=False)))
                    & Q(user__is_superuser=False)
                    | Q(user=user)
                )
            else:
                queryset = queryset.filter(
                    (Q(contest=None)
                     | (Q(contest__end_time__lte=datetime.now()) & Q(contest__hidden=False)))
                    & Q(user__is_superuser=False)
                )
        return queryset


class Submission(models.Model):
    ALLOWED_USER_REJUDGE_JUDGEMENTS = {"Submitter Failure", "Judging Timed Out"}

    contest = models.ForeignKey(Contest, blank=True, null=True, db_index=True)
    problem = models.ForeignKey(Problem, db_index=True)
    user = models.ForeignKey(User, db_index=True)
    code = models.TextField()
    language = models.CharField(max_length=30, db_index=True, choices=SUPPORTED_LANGUAGES)
    submit_time = models.DateTimeField(auto_now_add = True, db_index = True)
    judgement = models.ForeignKey("SubmissionJudgement", blank=True, null=True, related_name="+")

    objects = SubmissionManager()
    
    def get_absolute_url(self):
        return reverse("submission-detail", args=[self.pk])

    def is_log_visible(self):
        try:
            return self.judgement.judgement == "Compilation Error"
        except:
            return False

    def allow_user_rejudge(self):
        return self.judgement and self.judgement.judgement in self.ALLOWED_USER_REJUDGE_JUDGEMENTS
    
    def judge(self, judgement, time=None, memory=None, effective_time=None, judge=None):
        if not effective_time:
            effective_time = datetime.now()
        with transaction.commit_on_success():
            SubmissionJudgement.objects.filter(submission=self, valid=True).update(valid=False);
            new_judgement = SubmissionJudgement(submission=self, judgement=judgement, time=time,
                                                memory=memory, effective_time=effective_time,
                                                valid=True, judge=judge)
            new_judgement.save()
            self.judgement = new_judgement
            self.save()
    
    def cancel_all_judgement(self):
        SubmissionJudgement.objects.filter(valid=True, submission=self).update(valid=False)
    
    def is_waiting(self):
        """Returns true if the verdict should be considered waiting"""
        return self.judgement and self.judgement.valid

    def __unicode__(self):
        return str(self.id)

    class Meta:
        ordering = ["-id"]


class SubmissionJudgement(models.Model):
    submission = models.ForeignKey(Submission)
    judgement = models.CharField(max_length=30, db_index=True)
    time = models.IntegerField(blank=True, null=True, help_text="Execution time in milliseconds")
    memory = models.IntegerField(blank=True, null=True, help_text="Execution memory in KBytes")
    effective_time = models.DateTimeField(db_index=True)
    valid = models.BooleanField(db_index=True)
    judge = models.ForeignKey(User, blank=True, null=True)
    stat = models.TextField(help_text="This should be a JSON string which contains statistic information"
                                      " (for example time, memory usage)",
                            blank=True)
    
    objects = models.Manager()
    
    def __unicode__(self):
        return u"{} - {}".format(self.submission_id, self.judgement) if self.valid else "---"


class TestCaseResult(models.Model):
    submission = models.ForeignKey(Submission)
    test_case = models.ForeignKey(TestCase)
    judgement = models.CharField(max_length=30)
    stat = models.TextField(help_text="This should be a JSON string which contains statistic information"
                                      " (for example time, memory usage)",
                            blank=True)

    def __unicode__(self):
        return u"s{}t{}: {}".format(self.submission_id, self.test_case.test_id, self.judgement)


class SubmissionLog(models.Model):
    submission = models.OneToOneField(Submission, db_index=True)
    time = models.DateTimeField(auto_now_add=True, db_index=True)
    log = models.TextField()


class ExternalSubmission(models.Model):
    contest = models.ForeignKey(Contest, db_index = True)
    time = models.DateTimeField(db_index = True)
    name = models.CharField(max_length=256)
    problem = models.CharField(max_length=1)
    is_accepted = models.BooleanField()
    code = models.TextField(blank = True)

    def __unicode__(self):
        return u'{0}. {1}: {2}'.format(self.contest, self.problem, 'Accepted' if self.is_accepted else 'Rejected')


def check_judge(sender, **kwargs):
    instance = kwargs['instance']
    import judge.submissions.services as services
    if kwargs["created"]:
        services.dispatch_submission_to_task_queue_for_judge(instance)


post_save.connect(check_judge, Submission, dispatch_uid='check_judge')


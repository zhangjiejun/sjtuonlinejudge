"""Module that syncs test data from remote server to local"""

from django.conf import settings as global_settings
from judge.utils import singleton
import os
import stat
import subprocess

localjudge_settings = global_settings.OJ["LOCALJUDGE"]
WORKING_DIR = localjudge_settings["WORKING_DIR"]


class SyncError(Exception):
    pass


@singleton
class RsyncSynchornizer(object):
    def sync(self, pk):
        rset = localjudge_settings["RSYNC"]
        tests_path = os.path.join(WORKING_DIR, "tests")
        password_path = os.path.join(WORKING_DIR, "password")
        with open(password_path, "w") as f:
            f.write(rset["PASSWORD"])
            os.chmod(password_path, stat.S_IRUSR)
        command = "{}@{}::{}/{}/*".format(rset["USER"], rset["HOST"], rset["SRC"], pk)
        try:
            subprocess.check_call(["rsync", "-r", "--password-file=" + password_path, command, tests_path])
            subprocess.check_call(["chmod", "-R", "500", tests_path])
            subprocess.check_call(["rm", password_path])
        except subprocess.CalledProcessError:
            raise SyncError("failed to sync tests")


@singleton
class FtpSynchornizer(object):
    """ NEED FURTHER ADJUSTMENT, NOT WORKING NOT """
    
    def sync(self, pk):
        pass
    
#		fset = settings["FTP"]
#		url = fset["HOST"]
#        username = fset["USERNAME"]
#        password = fset["PASSWORD"]
#        logger.info('Connecting to FTP server {0}:{1}@{2}'.format(url, username, password))
#        ftp = FTP(url, username, password)
#        logger.info('Changing to problem\'s directory')
#        ftp.cwd(str(submission.problem.pk))
#        logger.info('Listing all files')
#        files = ftp.nlst()
#        if 'checker' in files:
#            logger.info('Discovered checker, changing directory to "checker"')
#            ftp.cwd('checker')
#            checker_files = ftp.nlst()
#            logger.info('Creating local checker directory')
#            os.makedirs(CHECKER_DIR)
#            logger.info('Retrieving checker files')
#            for f in checker_files:
#                current_file = os.path.join(CHECKER_DIR, f)
#                logger.info('Retrieving {0} to {1}'.format(f, current_file))
#                ftp.retrbinary('RETR {0}'.format(f), open(current_file, 'wb').write)
#                os.chmod(current_file, stat.S_IXUSR | stat.S_IXGRP | stat.S_IXOTH)
#            ftp.cwd('..')
#        else:
#            logger.info('No checker discovered')
#        
#        # Fetch all tests
#        os.makedirs(TESTS_DIR)
#        for testcase in submission.problem.testcase_set.all():
#            input_filename = testcase.input_filename
#            local_filename = os.path.join(TESTS_DIR, input_filename)
#            logger.info('Retriving input {}'.format(input_filename))
#            ftp.retrbinary('RETR {}'.format(input_filename), open(local_filename, 'wb').write)
#            os.chmod(local_filename, stat.S_IRUSR | stat.S_IWUSR)
#            
#            output_filename = testcase.output_filename
#            local_filename = os.path.join(TESTS_DIR, output_filename)
#            logger.info('Retriving output {}'.format(output_filename))
#            ftp.retrbinary('RETR {}'.format(output_filename), open(local_filename, 'wb').write)
#            os.chmod(local_filename, stat.S_IRUSR | stat.S_IWUSR)
#        ftp.quit()
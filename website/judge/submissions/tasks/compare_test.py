from unittest import TestCase
import compare
import tempfile

__author__ = 'zhangjiejun1992'


class TestCompareBase(TestCase):
    def setUp(self):
        self.f1 = tempfile.NamedTemporaryFile()
        self.f2 = tempfile.NamedTemporaryFile()

    def tearDown(self):
        self.f1.close()
        self.f2.close()


class TestByteByByte(TestCompareBase):
    def test_diff(self):
        self.f1.write("1234")
        self.f2.write("2345")
        self.f1.flush()
        self.f2.flush()
        self.assertFalse(compare.byte_by_byte(self.f1.name, self.f2.name))

    def test_equal(self):
        self.f1.write("1234")
        self.f2.write("1234")
        self.f1.flush()
        self.f2.flush()
        self.assertTrue(compare.byte_by_byte(self.f1.name, self.f2.name))

    def test_diff_whitespace(self):
        self.f1.write("1234")
        self.f2.write("1234 ")
        self.f1.flush()
        self.f2.flush()
        self.assertFalse(compare.byte_by_byte(self.f1.name, self.f2.name))


class TestIgnoreWhiteSpace(TestCompareBase):
    def test_diff(self):
        self.f1.write("1234")
        self.f2.write("2345")
        self.f1.flush()
        self.f2.flush()
        self.assertFalse(compare.ignore_white_space(self.f1.name, self.f2.name))

    def test_equal(self):
        self.f1.write("1234")
        self.f2.write("1234")
        self.f1.flush()
        self.f2.flush()
        self.assertTrue(compare.ignore_white_space(self.f1.name, self.f2.name))

    def test_diff_whitespace(self):
        self.f1.write("1234")
        self.f2.write("1234 ")
        self.f1.flush()
        self.f2.flush()
        self.assertTrue(compare.ignore_white_space(self.f1.name, self.f2.name))


class TestFloat(TestCompareBase):
    def test_diff(self):
        """Test different float"""
        self.f1.write("1.123456789")
        self.f2.write("1.123456799")
        self.f1.flush()
        self.f2.flush()
        self.assertFalse(compare.float_by_float(self.f1.name, self.f2.name))

    def test_equal_sequence(self):
        """Test 2 equal sequences of floats"""
        print >>self.f1, "{:.18g} {:.18g} {:.18g} {:.18g}".format(1.12345678901179, 1e100 + 1e87, -1e100, 1.1e-100)
        print >>self.f2, "{:.18g} {:.18g} {:.18g} {:.18g}".format(1.12345678901552, 1e100 + 1e90, -1e100, 1.2e-100)
        self.f1.flush()
        self.f2.flush()
        self.assertTrue(compare.float_by_float(self.f1.name, self.f2.name))

    def test_diff_sequence(self):
        """Test 2 different sequence of floats"""
        print >>self.f1, "{:.18g} {:.18g} {:.18g} {:.18g}".format(1.12345678901179, 1.1e100, -1e100, 1.1e-100)
        print >>self.f2, "{:.18g} {:.18g} {:.18g} {:.18g}".format(1.12345678901552, 1e100, -1e100, 1.2e-100)
        self.f1.flush()
        self.f2.flush()
        self.assertFalse(compare.float_by_float(self.f1.name, self.f2.name))
"""This module defines some common checkers"""
import subprocess


def byte_by_byte(path_std, path_user):
    """Call UNIX Command "diff" to compare, byte by byte"""
    return_code = subprocess.call(["diff", "--brief", path_std, path_user])
    return return_code == 0


def ignore_white_space(path_std, path_user):
    """Call UNIX command "diff" to compare, ignore white spaces"""
    return_code = subprocess.call(["diff", "--ignore-all-space", "--brief", path_std, path_user])
    return return_code == 0


def float_by_float(path_std, path_user):
    """Compare two files token-by-token, if a token can be intepreted as a float then
    accept if their absolute/relative error does not exceed 1e-9.

    WARNING: this function is not supposed for very large files, as it loads the whole file to memory.

    """
    with open(path_std) as f1, open(path_user) as f2:
        list_std = f1.read().split()
        list_user = f2.read().split()
        if len(list_std) != len(list_user):
            return False

        for token_std, token_user in zip(list_std, list_user):
            try:
                float_std = float(token_std)
            except (TypeError, ValueError):
                # Not a float, compare as strings
                if token_std != token_user:
                    return False
                continue

            # Now token_user is expected to be a float
            try:
                float_user = float(token_user)
            except (TypeError, ValueError):
                return False

            relative_error = abs((float_user - float_std) / float_std)
            absolute_error = abs(float_std - float_user)
            if relative_error > 1e-9 or absolute_error > 1e-9:
                return False
    return True
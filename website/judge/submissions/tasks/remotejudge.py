import importlib
from django.core.cache import cache
from celery.exceptions import SoftTimeLimitExceeded
from judge.submissions.models import Submission
from judge.submissions import services
from django.conf import settings
from django.db import transaction


def judge_remote(pk):
    """Dynamically import a submitter module, and invoke to judge the submission.
    
    :param pk: The primary key of the submission

    """
    try:
        submission = Submission.objects.get(pk=pk)
        problem = submission.problem
        oj = problem.residing_judge
        try:
            module = importlib.import_module("." + oj, "judge.submissions.tasks.submitters")
            judge = module.judge
        except ImportError:
            services.give_judgement(submission, "Not Implemented")
        else:
            username, password = settings.OJ_ACCOUNTS[oj]
            result = judge(problem.residing_judge_id, submission.language, submission.code,
                           username, password)
            with transaction.commit_on_success():
                services.give_judgement(submission, result["verdict"])
                services.write_submission_log(submission, result.get("compile_message", ""))
    except SoftTimeLimitExceeded:
        services.give_judgement(submission, "Judging Timed Out")
    except Exception:
        services.give_judgement(submission, "Submitter Failure")
    finally:
        cache.delete(services.get_submission_task_id_cache_key(pk))


def judge_remote_webdriver(pk):
    try:
        submission = Submission.objects.get(pk=pk)
        problem = submission.problem
        oj = problem.residing_judge
        try:
            module = importlib.import_module("." + oj + "webdriver", "judge.submissions.tasks.submitters")
            judge = module.judge
        except ImportError:
            services.give_judgement(submission, "Not Implemented!")
        else:
            username, password = settings.OJ_ACCOUNTS[oj]
            result = judge(problem.residing_judge_id, submission.language, submission.code,
                           username, password)
            with transaction.commit_on_success():
                services.give_judgement(submission, result["verdict"])
                services.write_submission_log(submission, result.get("compile_message", ""))
    except SoftTimeLimitExceeded:
        services.give_judgement(submission, "Judging Timed Out")
    except Exception:
        services.give_judgement(submission, "Submitter Failure")
    finally:
        cache.delete(services.get_submission_task_id_cache_key(pk))

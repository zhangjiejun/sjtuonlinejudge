from time import sleep
import logging
import mechanize
import urlparse
import re

logger = logging.getLogger(__name__)

LANGUAGE_MAP = {'C': '1', 'C++': '3', 'Java': '2', 'Pascal': '4'}

def judge(problem_id, language, code, USERNAME, PASSWORD, *args, **kwargs):
    br = mechanize.Browser()
    br.set_handle_robots(False)
    br.set_handle_refresh(False)

    # The contest volume home page
    logger.info('Getting real ID')
    problem_id = int(problem_id)
    br = mechanize.Browser()
    logger.info("OPENING CONTEST VOLUME HOME PAGE")
    html = br.open('http://uva.onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&category=2').read()

    # Determine the volume
    logger.info("GOTO THE VOLUME")
    volume = problem_id / 100
    volume_id = problem_id % 100
    r = re.compile(ur'<td><a href="(?P<url>[^"]*)">Volume [A-Z]+</a></td>')
    target_url = r.findall(html)[volume - 100].replace('amp;', '')
    target_url = urlparse.urljoin(br.geturl(), target_url)
    response = br.open(target_url)
    html = response.read()

    # Now we are inside the volume, determine the problem
    r = re.compile(ur'<td><a href="(?P<url>index.php\?option=com_onlinejudge&amp;Itemid=8&amp;category=\d+&amp;page=show_problem&amp;problem=(?P<real_id>\d+))">\d*&nbsp;-&nbsp;(?P<title>[^<]+)')
    problems = r.findall(html)
    real_id = problems[volume_id][1]

    # Login Constants
    LOGIN_URL = 'http://uva.onlinejudge.org/'
    USERNAME_FIELD = 'username'
    PASSWORD_FIELD = 'passwd'
    FORM_ID = 0

    # Login step
    logger.info('Logging in to UVA')
    br.open(LOGIN_URL)
    br.select_form(nr = FORM_ID)
    br[USERNAME_FIELD] = USERNAME
    br[PASSWORD_FIELD] = PASSWORD
    br.submit()

    # Submit Constants
    SUBMIT_URL = 'http://uva.onlinejudge.org/index.php?option=com_onlinejudge&Itemid=8&page=submit_problem&problemid={0}&category=229'.format(real_id)
    FORM_ID = 1

    # Add missing language options not recognized by mechanize
    logger.info('Submitting')
    br.open(SUBMIT_URL)
    br.select_form(nr = 1)
    language_control = br.find_control('language')
    mechanize.Item(language_control, {'value': '1'})
    mechanize.Item(language_control, {'value': '2'})
    mechanize.Item(language_control, {'value': '3'})
    mechanize.Item(language_control, {'value': '4'})

    # Now submit
    try:
        br['language'] = [LANGUAGE_MAP[language]]
    except KeyError:
        return {'verdict': 'Language Not Supported'}
    br['code'] = code
    response = br.submit()

    html = response.read()
    r = re.compile('<div class="message">Submission received with ID \d+</div>')
    m = r.search(html)
    if m is None:
        return {'verdict': 'Submit Failure'}

    # Watch the result
    RESULT_URL = 'http://uva.onlinejudge.org/index.php?option=com_onlinejudge&Itemid=9'
    TERMINATING_VERDICT = {
        'Accepted', 'Time limit exceeded', 'Compile error',
        'Wrong answer', 'Runtime error', 'Memory limit exceeded',
        'Presentation error', 'Output limit exceeded', 'Restricted function',
    }
    while True:
        logger.info('Watching the result')
        retry_count = 3
        while retry_count > 0:
            retry_count -= 1
            try:
                html = br.open(RESULT_URL).readlines()
            except:
                if retry_count > 0:
                    logger.info('Retry count: {}'.format(retry_count))
                    continue
                else:
                    raise
            else:
                break
        r = re.compile('<tr class="sectiontableentry1">')
        index = 0
        for line in html:
            if r.search(line) is not None:
                r = re.compile('<td>(?P<verdict>((?!</td>).)*)</td>')
                m = r.search(html[index + 4])
                verdict = m.group('verdict')

                # there's a '<a>' tag in Compilation error, remove it
                if 'Compilation error' in verdict:
                    return {'verdict': 'Compilation Error'}

                logger.info("GOT {}".format(verdict))
                if verdict in TERMINATING_VERDICT:
                    return {'verdict': verdict.title()}
                break
            index += 1
        sleep(1)

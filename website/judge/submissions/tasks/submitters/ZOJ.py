import mechanize
import urllib2
import logging
import re
from time import sleep

logger = logging.getLogger(__name__)

def judge(problem_id, language, code, USERNAME, PASSWORD, *args, **kwargs):
    LOGIN_URL = "http://acm.zju.edu.cn/onlinejudge/login.do"
    LANGUAGE_MAP = {'C': '1',
                    'C++': '2',
                    'Pascal': '3',
                    'Java': '4'}
    USERNAME_FIELD = 'handle'
    PASSWORD_FIELD = 'password'
    FORM_ID = 0

    PROB_URL = "http://acm.zju.edu.cn/onlinejudge/showProblem.do?problemCode=" + str(problem_id)
    SUBMIT_URL_PREFIX = "http://acm.zju.edu.cn/onlinejudge/submit.do?problemId="
    LANGUAGE_FIELD = 'languageId'
    CODE_FIELD = 'source'
    SUBMIT_FORM_ID = 0

    RESULT_URL = "http://acm.zju.edu.cn/onlinejudge/showRuns.do?contestId=1&handle=" + USERNAME
    TERMINATING_RESULT = [
        'Accepted',
        'Presentation Error',
        'Wrong Answer',
        'Time Limit Exceeded',
        'Memory Limit Exceeded',
        'Segmentation Fault',
        'Non-zero Exit Code',
        'Floating Point Error',
        'Compilation Error',
        'Output Limit Exceeded',
        'Runtime Error',
        'System Error',
        'Validator Error'
    ]
    RE_RESULT = ['Segmentation Fault', 'Non-zero Exit Code', 'Floating Point Error']


    # Start 
    browser = mechanize.Browser()
    browser.set_handle_robots(False)

    # Fetching submit ID
    logger.info("Fetching submit ID")
    html = urllib2.urlopen(PROB_URL).read()
    r = re.compile(r'"/onlinejudge/submit\.do\?problemId=(?P<submitid>\d+)"')
    m = r.finditer(html).next()
    SUBMIT_ID = m.group('submitid')
    SUBMIT_URL = SUBMIT_URL_PREFIX + str(SUBMIT_ID)

    # Login
    tried = 0
    while True:
        logger.info("Logging in")
        browser.open(LOGIN_URL)
        browser.select_form(nr=FORM_ID)
        browser[USERNAME_FIELD] = USERNAME
        browser[PASSWORD_FIELD] = PASSWORD
        browser.submit()

        # Submit
        logger.info("Submitting")
        browser.open(SUBMIT_URL)
        browser.select_form(nr = SUBMIT_FORM_ID)
        browser[LANGUAGE_FIELD] = [LANGUAGE_MAP[language]]
        browser[CODE_FIELD] = code
        browser.submit()

        if browser.title() != 'ZOJ :: Problems :: Submit Success':
            if tried > 10:
                return {'verdict': 'Submit Failure'}
            logger.info('Failed to submit, wait a second and try again..')
            sleep(1)
            tried += 1
        else:
            break

    while True:
        sleep(1)
        response = browser.open(RESULT_URL)
        html = response.read()
        r = re.compile(r'<td class="runJudgeStatus">.*?<span(?P<verdict>(.*?))</td>')
        m = r.finditer(html.replace('\n', '')).next()

        verdict = m.group('verdict')
        for item in TERMINATING_RESULT:
            if verdict.find(item) != -1:
                if item in RE_RESULT:
                    return {'verdict': 'Runtime Error'}
                return {'verdict': item}

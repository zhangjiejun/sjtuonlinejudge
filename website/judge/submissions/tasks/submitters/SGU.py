from bs4 import BeautifulSoup
from time import sleep

import logging
import mechanize
import socket
import re

logger = logging.getLogger(__name__)

def judge(problem_id, language, code, USERNAME, PASSWORD, *args, **kwargs):
    socket.setdefaulttimeout(10000)

    URL = 'http://acm.sgu.ru/submit.php'
    LANGUAGE_MAP = {
        'Pascal': 'Delphi 7.0',
        'C': 'GNU C (MinGW, GCC 4)',
        'C++': 'GNU CPP (MinGW, GCC 4)',
        'Java': 'JAVA 1.6',
    }

    logger.info('Submitting')

    tried = 0
    while True:
        br = mechanize.Browser()
        br.open(URL)
        br.select_form(nr = 1)
        br['id'] = USERNAME
        br['pass'] = PASSWORD
        br['problem'] = problem_id
        br['source'] = code
        br['elang'] = [LANGUAGE_MAP[language]]

        response = br.submit()
        html = response.read()
        r = re.compile('Your solution was successfully submitted. Wait for reply.')
        if not r.search(html):
            if tried > 10:
                return {'verdict': 'Submit Failure'}
            logger.info('Failed to submit, wait a second and try again..')
            sleep(1)
        else:
            break

    while True:
        logger.info('Watching the Result')
        response = br.open('http://acm.sgu.ru/status.php?id={}&idmode=1'.format(USERNAME))
        html = response.read()
        soup = BeautifulSoup(html)
        result = soup.find('td', attrs={'class': 'btab'}).text
        logger.info("GOT {}".format(result))
        if result.startswith('Running') or result.startswith('Waiting'):
            sleep(1)
            continue
        return {'verdict': result.title()}


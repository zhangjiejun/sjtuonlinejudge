from time import sleep
from selenium.webdriver.firefox.firefox_profile import FirefoxProfile
from selenium.webdriver.common.keys import Keys
from selenium import webdriver
import logging
from selenium.webdriver.support.ui import Select
from datetime import datetime
import traceback
import pygtk
pygtk.require('2.0')
import gtk


logger = logging.getLogger(__name__)
LANGUAGE_MAP = {'C': '10', 'C++': '1', 'Java': '23', 'Pascal': '4'}


def judge(problem_id, language, code, USERNAME, PASSWORD, *args, **kwargs):
    firefoxProfile = FirefoxProfile()
    firefoxProfile.set_preference('permissions.default.stylesheet', 2)
    firefoxProfile.set_preference('permissions.default.image', 2)
    logger.info('Log in')
    driver = webdriver.Firefox(firefoxProfile)
    try:
        logger.info("Logging in")
        driver.get("http://codeforces.com/enter")

        input_element = driver.find_element_by_name('handle')
        input_element.click()
        input_element.send_keys(USERNAME)

        input_element = driver.find_element_by_name('password')
        input_element.click()
        input_element.send_keys(PASSWORD)

        input_element.submit()

        logger.info("Submitting")
        driver.get("http://codeforces.com/problemset/submit")
        input_element = driver.find_element_by_name("submittedProblemCode")
        input_element.click()
        input_element.send_keys(problem_id)

        select = Select(driver.find_element_by_name("programTypeId"))
        select.select_by_value(LANGUAGE_MAP[language])

        input_element = driver.find_element_by_name("source")
        input_element.click()
        #input_element.send_keys("/* {} */".format(datetime.now()))
        #input_element.send_keys(Keys.RETURN)
        clipboard = gtk.clipboard_get()
        clipboard.set_text(code)
        clipboard.store()
        input_element.send_keys(Keys.CONTROL + "v")
        #input_element.send_keys(code)
        input_element.submit()

        url = driver.current_url
        if not url.endswith("http://codeforces.com/problemset/status"):
            return {"verdict": "Submitter Failure"}

        while True:
            logger.info("Polling the result")
            driver.get("http://codeforces.com/submissions/{}".format(USERNAME))
            row = driver.find_element_by_xpath("//table[@class='status-frame-datatable']//tr[2]")
            td = row.find_element_by_class_name("status-cell")
            if td.find_elements_by_class_name("verdict-rejected") or td.find_elements_by_class_name("verdict-accepted") or td.find_elements_by_class_name("information-box-link") or td.find_elements_by_class_name("verdict-failed"):
                verdict = td.text
                logger.info('Got final status: {}'.format(verdict))
                k = verdict.find(" on test")
                if k != -1:
                    verdict = verdict[:k]
                return {'verdict': verdict.title()}
            else:
                logger.info("Got waiting status: {}".format(td.text))
            sleep(2)
    except:
        logger.error(traceback.format_exc())
        return {"verdict": "Submitter Failure"}
    finally:
        driver.close()

from time import sleep
from bs4 import BeautifulSoup
import mechanize
import logging
import re

logger = logging.getLogger(__name__)

LANGUAGE_MAP = {
    'C' : '1',
    'C++': '3',
    'Java': '2',
    'Pascal': '4',
}

def judge(problem_id, language, code, USERNAME, PASSWORD, *args, **kwargs):
    br = mechanize.Browser()
    br.set_handle_robots(False)
    br.set_handle_refresh(False)

    # Login Constants
    LOGIN_URL = 'https://icpcarchive.ecs.baylor.edu'
    USERNAME_FIELD = 'username'
    PASSWORD_FIELD = 'passwd'
    FORM_ID = 1

    # Login step
    logger.info('Logging in to VALLA')
    br.open(LOGIN_URL)
    br.select_form(nr = FORM_ID)
    br[USERNAME_FIELD] = USERNAME
    br[PASSWORD_FIELD] = PASSWORD
    br.submit()

    # Submit Constants
    SUBMIT_URL = 'https://icpcarchive.ecs.baylor.edu/index.php?option=com_onlinejudge&Itemid=25'
    FORM_ID = 2

    # Add missing language options not recognized by mechanize
    br.open(SUBMIT_URL)
    br.select_form(nr = FORM_ID)
    language_control = br.find_control('language')
    mechanize.Item(language_control, {'value': '2'})
    mechanize.Item(language_control, {'value': '3'})
    mechanize.Item(language_control, {'value': '4'})

    # Now submit
    logger.info('Submitting')
    br['localid'] = problem_id
    br['language'] = [LANGUAGE_MAP[language]]
    br['code'] = code
    response = br.submit()

    html = response.read()
    r = re.compile('<div class="message">Submission received with ID \d+</div>')
    m = r.search(html)
    if m is None:
        return {'verdict': 'Submitter Failure'}

    # Watch the result
    RESULT_URL = 'https://icpcarchive.ecs.baylor.edu/index.php?option=com_onlinejudge&Itemid=9'
    TERMINATING_VERDICT = {'Accepted', 'Time limit exceeded', 'Compilation Error', 'Submitter Failure',
                           'Wrong answer', 'Runtime error', 'Memory limit exceeded',
                           'Presentation error', 'Output limit exceeded', 'Restricted function'}
    while True:
        logger.info('Watching the result')
        html = br.open(RESULT_URL).readlines()
        r = re.compile('<tr class="sectiontableentry1">')
        index = 0
        for line in html:
            if r.search(line) is not None:
                r = re.compile('<td>(?P<verdict>((?!</td>).)*)</td>')
                m = r.search(html[index + 4])
                verdict = m.group('verdict')

                if not m:
                    break

                # there's a '<a>' tag in Compilation error, remove it
                if 'Compilation error' in verdict:
                    verdict = 'Compilation Error'
                # change 'Submission error' to 'Submitter Failure' for user rejudge
                if 'Submission error' in verdict:
                    verdict = 'Submitter Failure'

                logger.info("GOT {}".format(verdict))
                if verdict in TERMINATING_VERDICT:
                    return {'verdict': verdict.title()}
                break
            index += 1
        sleep(2)

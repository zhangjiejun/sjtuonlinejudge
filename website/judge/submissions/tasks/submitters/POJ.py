from time import sleep
import mechanize
import logging
import re

logger = logging.getLogger(__name__)

def judge(problem_id, language, code, USERNAME, PASSWORD, *args, **kwargs):
    # Initialize
    URL = "http://poj.org"
    SUBMIT_URL = "http://poj.org/submit"
    LANGUAGE_MAP = {'G++': '0', 'GCC': '1', 'Java': '2', 'Pascal': '3', 'C++': '0',
                    'C': '5', 'Python 2.7': '4'}
    
    # The following constants define the field name for username
    # and password, and the form ID of the login form.
    #
    # Please, run the following code in python interpreter and observe
    # the output, to determine their values, if you experience any trouble
    # in login in the future:
    #
    # > import mechanize
    # > b = mechanize.Browser()
    # > b.open("http://poj.org")    # you should change this line if
    # >                               poj changes its URL.
    # > for f in b.forms():
    # >     print(f)
    # >   
    #
    USERNAME_FIELD = 'user_id1'
    PASSWORD_FIELD = 'password1'
    FORM_ID = 2                     # zero-based

    # The following constants define the field name for submission
    # information. Please refer to the steps for resolving login troubles
    # if you experience any trouble.
    #
    PROBLEM_ID_FIELD = 'problem_id'
    LANGUAGE_FIELD = 'language'
    CODE_FIELD = 'source'
    SUBMIT_FORM_ID = 2

    # Status URL
    RESULT_URL = "http://poj.org/status?problem_id=&user_id={0}&result=&language=".format(USERNAME)
    TERMINATING_RESULT = {'Accepted', 'Presentation Error', 'Time Limit Exceeded',
                          'Memory Limit Exceeded', 'Wrong Answer', 'Runtime Error',
                          'Output Limit Exceeded', 'Compile Error', 'System Error',
                          'Validator Error'}

    # This is our browser for submitting
    browser = mechanize.Browser()
    browser.set_handle_robots(False)

    # Login step
    logger.info("Logging in")
    browser.open(URL)
    browser.select_form(nr=FORM_ID)
    browser[USERNAME_FIELD] = USERNAME
    browser[PASSWORD_FIELD] = PASSWORD
    browser.submit()

    # Now goto submit page
    logger.info("Submitting")
    browser.open(SUBMIT_URL)
    browser.select_form(nr=SUBMIT_FORM_ID)
    browser[PROBLEM_ID_FIELD] = problem_id
    browser[LANGUAGE_FIELD] = [LANGUAGE_MAP[language]]
    browser[CODE_FIELD] = code
    browser.submit()

    if browser.title() != 'Problem Status List':
        return {'verdict': 'Submit Failure'}

    # Last step: watch the results
    while True:
        response = browser.open(RESULT_URL)
        html = response.read()
        r = re.compile(r'<tr align=center><td>(?P<run_id>\d+)</td>(<td>((?!</td>).)+</td>)' + 
                         '{2}<td>(?P<verdict>((?!</td>).)+)</td>')
        m = r.finditer(html).next()

        run_id = m.group('run_id')
        verdict = m.group('verdict')

        logger.info('Runid={}, Verdict={}'.format(run_id, verdict))

        if 'Compile Error' in verdict:
            logger.info('Getting compile message')
            html = browser.open('http://poj.org/showcompileinfo?solution_id=' + run_id).read()
            r = re.compile('<pre>(?P<message>((?!</pre>).)+)</pre>', re.DOTALL)
            m = r.search(html)
            return {'compile_message': m.group('message'), 'verdict': 'Compilation Error'}
        r = re.compile('<font color=[^>]+>(?P<verdict>.+)</font>')
        m = r.match(verdict)
        verdict = m.group('verdict')
        if verdict in TERMINATING_RESULT:
            return {'verdict': verdict}
        sleep(1)


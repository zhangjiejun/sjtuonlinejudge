import logging
from time import sleep
import mechanize
import re

logger = logging.getLogger(__name__)

def judge(problem_id, language, code, USERNAME, PASSWORD, *args, **kwargs):
    # Initialize
    URL = "http://acm.hdu.edu.cn"
    SUBMIT_URL = "http://acm.hdu.edu.cn/submit.php"
    LANGUAGE_MAP = {'Python 2.7': '2', 'GCC': '1', 'C++': '0', 'C': '3',
                    'Pascal': '4', 'Java': '5'}
    
    USERNAME_FIELD = 'username'
    PASSWORD_FIELD = 'userpass'
    FORM_ID = 2                     # zero-based

    PROBLEM_ID_FIELD = 'problemid'
    LANGUAGE_FIELD = 'language'
    CODE_FIELD = 'usercode'
    SUBMIT_FORM_ID = 2

    RESULT_URL = "http://acm.hdu.edu.cn/status.php?first=&pid=&user={0}&lang=0&status=0".format(USERNAME)
    TERMINATING_VERDICTS = {
        'Accepted',
        'Presentation Error',
        'Wrong Answer',
        'Runtime Error',
        'Time Limit Exceeded',
        'Memory Limit Exceeded',
        'Output Limit Exceeded',
        'Compilation Error',
        'System Error',
    }

    browser = mechanize.Browser()
    browser.addheaders = [('User-agent', 'Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.0.1) Gecko/2008071615 Fedora/3.0.1-1.fc9 Firefox/3.0.1')]
    browser.set_handle_robots(False)
    browser.set_handle_refresh(False)

    # Login step
    browser.open(URL)
    browser.select_form(nr=FORM_ID)
    browser[USERNAME_FIELD] = USERNAME
    browser[PASSWORD_FIELD] = PASSWORD
    browser.submit()

    logger.info("logged in")

    # Now goto submit page
    browser.open(SUBMIT_URL)
    browser.select_form(nr=SUBMIT_FORM_ID)
    browser[PROBLEM_ID_FIELD] = problem_id
    browser[LANGUAGE_FIELD] = [LANGUAGE_MAP[language]]
    browser[CODE_FIELD] = code
    browser.submit()

    logger.info("submitted")

    if browser.title() != 'Realtime Status':
        return {'verdict': 'Submit Failure'}

    # Last step: watch the results
    while True:
        response = browser.open(RESULT_URL)
        content = response.read()
        pos = content.find(r'<table width=100% border=0 align=center cellspacing=2 ' +
                            'style="border-bottom:#879BFF 1px dashed" class=table_text>')
        if pos == -1:
            logger.info("ERROR! Cannot locate table")
            return {'verdict': 'Contact Staff'}
        else:
            content = content[pos:]
            r = re.compile(r'<font color=[^>]+>(?P<verdict>((?!</font>).)+)</font>', re.DOTALL)
            m = r.finditer(content).next()
            verdict = m.group('verdict')

            r = re.compile(r'<td height=22px>(?P<run_id>\d+)</td>')
            m = r.finditer(content).next()
            run_id = m.group('run_id')
            logger.info('Got {}'.format(verdict))
            for v in TERMINATING_VERDICTS:
                if verdict.startswith(v):
                    if v == 'Compilation Error' and language != 'Python 2.7':
                        html = browser.open('http://acm.hdu.edu.cn/viewerror.php?rid=' + run_id).read()
                        r = re.compile('<pre>(?P<message>.+)</pre>', re.DOTALL)
                        m = r.search(html)
                        return {'verdict': 'Compilation Error',
                                'compile_message': m.group('message')}
                    return {'verdict': v.title()}
        sleep(1)

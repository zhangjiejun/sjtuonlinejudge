import mechanize
import urllib2
import re
import logging
from time import sleep

logger = logging.getLogger(__name__)

def judge(problem_id, language, code, USERNAME, PASSWORD, *args, **kwargs):
    br = mechanize.Browser()
    br.set_handle_robots(False)
    br.set_handle_refresh(False)

    LANGUAGE_MAP = {
        'C++': 'C++',
        'Java': 'JAVA',
        'C': 'C',
    }
    RESULT_URL = 'http://judge.u-aizu.ac.jp/onlinejudge/webservice/status_log?user_id={0}&limit=1'.format(USERNAME)
    SUBMIT_URL = 'http://judge.u-aizu.ac.jp/onlinejudge/description.jsp?id={0}'.format(problem_id)

    # get last submit id
    response = br.open(RESULT_URL)
    result = response.read().split()
    last_submit_id = result[result.index('<run_id>') + 1]
    logger.info('Last submit id = {}'.format(last_submit_id))

    # submit
    logger.info("Submitting")
    response = br.open(SUBMIT_URL)
    br.select_form(nr = 2)
    br['language'] = [LANGUAGE_MAP[language]]
    br['sourceCode'] = code
    br['userID'] = USERNAME
    br['password'] = PASSWORD
    br['problemNO'] = problem_id
    br.submit()

    TERMINATING_RESULT = {
        "Accepted",
		"Compile Error",
		"Runtime Error",
		"Time Limit Exceeded",
		"Wrong Answer",
	    "Memory Limit Exceeded",
		"Output Limit Exceeded",
		"Presentation Error",
    }

    while True:
        sleep(1)
        response = br.open(RESULT_URL)
        xml = response.read()
        result = xml.split()
        submit_id = result[result.index('<run_id>') + 1]
        logger.info('Got ID {}'.format(submit_id))

        if submit_id == last_submit_id:
            logger.info('Current submit id equals to last submit id')
            continue

        for verdict in TERMINATING_RESULT:
            if verdict in xml:
                if verdict == 'Compile Error':
                    verdict = 'Compilation Error'
                return {'verdict': verdict}


from time import sleep
import mechanize
import logging
import re

logger = logging.getLogger(__name__)

LANGUAGE_MAP = {'C' : '20', 
                'C++': '21',
                'Java': '12',
                'Pascal': '3',
		'Python 2.7': '10' }

def judge(problem_id, language, code, USERNAME, PASSWORD, *args, **kwargs):
    br = mechanize.Browser()
    br.set_handle_robots(False)
    br.set_handle_refresh(False)

    # Go Submit
    retried = 0
    while True:
        logger.info('Submitting')
        br.open('http://acm.timus.ru/submit.aspx');
        br.select_form(nr=1)
        br['JudgeID'] = USERNAME                        
        br['Language'] = [LANGUAGE_MAP[language]]       # Select language
        br['ProblemNum'] = problem_id                   # Fill in problem ID
        br['Source'] = code                             # Fill in code
        br.submit()

        # Check if successfully submitted
        if br.title() != 'Judgement results @ Timus Online Judge':
            if retried > 10:
                return {'verdict': 'Submitter Failure'}
            retried += 1
            logger.info('Try again after 5 seconds')
            sleep(5)
        else:
            break

    RESULT_URL = 'http://acm.timus.ru/status.aspx?author={0}'.format(USERNAME[:-2])
    TERMINATING_RESULTS = ('Accepted',
                           'Compilation error',
                           'Time limit exceeded',
                           'Wrong answer',
                           'Output limit exceeded',
                           'Presentation Error',
                           'Memory limit exceeded',
                           'Restricted Function')

    while True:
        sleep(1)
        logger.info("Watching result")
        html = br.open(RESULT_URL).read()
        r = re.compile('<TD class="verdict_\w+">(?P<verdict>((?!</TD>).)+)</TD>')
        m = r.search(html)
        v = m.group('verdict')
        logger.info("Got " + v)
        # there's a '<a>' tag in Compilation error, remove it
        if 'Compilation error' in v:
            cer = re.compile('<A HREF="(?P<ceinfo>(.*?))">')
            cem = cer.search(v)
            ce_url = "http://acm.timus.ru/" + cem.group('ceinfo')
            ce_html = br.open(ce_url).read()
            return {'verdict': 'Compilation Error', 'compile_message': ce_html}
        if v.startswith('Runtime error'):
            return {'verdict': 'Runtime Error'}
        if v in TERMINATING_RESULTS:
            return {'verdict': v.title()}

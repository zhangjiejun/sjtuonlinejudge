from time import sleep
from bs4 import BeautifulSoup
import mechanize
import logging

logger = logging.getLogger(__name__)

def judge(problem_id, language, code, USERNAME, PASSWORD, *args, **kwargs):
    # Initialize
    LANGUAGE_MAP = {'Pascal': '2', 'Java': '3', 'C++': '0', 'C': '1'}
    
    browser = mechanize.Browser()
    browser.set_handle_robots(False)
    browser.set_handle_refresh(False)

    # Login step
    logger.info("Logging in")
    browser.open('http://acm.uestc.edu.cn/login.php')
    browser.select_form(nr=0)
    browser['username'] = USERNAME
    browser['password'] = PASSWORD
    browser.submit()

    # Now goto submit page
    logger.info("Submitting")
    browser.open('http://acm.uestc.edu.cn/submit.php?pid={}'.format(problem_id))
    browser.select_form(nr=0)
    try:
        browser['language'] = [LANGUAGE_MAP[language]]
    except KeyError:
        return {'verdict': 'Language Not Supported'}
    browser['code'] = code
    browser.submit()

    if browser.title() != 'Online Status - UESTC Online Judge':
        return {'verdict': 'Submit Failure'}

    # Last step: watch the results
    while True:
        response = browser.open('http://acm.uestc.edu.cn/status.php?id={}&cid=0&pid=&result=&language=&search.x=0&search.y=0&search=Search'.format(USERNAME))
        html = response.read()
        soup = BeautifulSoup(html)
        div = soup.find('div', attrs={'class': 'list'})
        ul = div.find('ul')
        pk_li = ul.find_all('li')[0]
        verdict_li = ul.find_all('li')[3]
        verdict = verdict_li.text.strip()
        pk = pk_li.text.strip()
        if not verdict in ['Waiting', 'Compiling', 'Judging']:
            if verdict.startswith('Compile Error'):
                response = browser.open('http://acm.uestc.edu.cn/submission.php?sid={}'.format(pk))
                return {'verdict': 'Compilation Error', 'compile_message': response.read()}
            else:
                return {'verdict': verdict}
        sleep(1)

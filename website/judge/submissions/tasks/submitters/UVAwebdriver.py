from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.firefox_profile import FirefoxProfile
from time import sleep
from selenium import webdriver
import logging
import mechanize
import re
import pygtk
pygtk.require('2.0')
import gtk

logger = logging.getLogger(__name__)

LANGUAGE_MAP = {'C': '1', 'C++': '3', 'Java': '2', 'Pascal': '4'}

def judge(problem_id, language, code, USERNAME, PASSWORD, *args, **kwargs):
    firefoxProfile = FirefoxProfile()
    firefoxProfile.set_preference('permissions.default.stylesheet', 2)
    firefoxProfile.set_preference('permissions.default.image', 2)
    logger.info('Log in')
    driver = webdriver.Firefox(firefoxProfile)
    try:
        driver.implicitly_wait(30)
        driver.get('http://uva.onlinejudge.org/')
        
        input_element = driver.find_element_by_name('username')
        input_element.click()
        input_element.send_keys(USERNAME)

        input_element = driver.find_element_by_name('passwd')
        input_element.click()
        input_element.send_keys(PASSWORD)

        input_element.submit()

        logger.info('Submitting')
        driver.get('http://uva.onlinejudge.org/index.php?option=com_onlinejudge&Itemid=25')
        input_element = driver.find_element_by_name('localid')
        input_element.click()
        input_element.send_keys(problem_id)
        
        input_elements = driver.find_elements_by_name('language')
        input_element = input_elements[int(LANGUAGE_MAP[language]) - 1]
        input_element.click()

        input_element = driver.find_element_by_name('code')
        clipboard = gtk.clipboard_get()
        clipboard.set_text(code)
        clipboard.store()
        input_element.send_keys(Keys.CONTROL + "v")
        """
        input_element.click()
        for line in code.split('\n'):
            input_element.send_keys(line)
            input_element.send_keys('\n')
        """
        input_element.submit()

        element = driver.find_element_by_class_name('message')
        text = element.text
        r = re.compile('Submission received with ID (?P<id>\d+)')
        m = r.search(text)
        if not m:
            return {'verdict': 'Submission Failure'}
        
        TERMINATING_VERDICT = {
            'Accepted', 'Time limit exceeded', 'Compilation error',
            'Wrong answer', 'Runtime error', 'Memory limit exceeded',
            'Presentation error', 'Output limit exceeded', 'Restricted function',
        }

        while True:
            logger.info('Watching the result')
            driver.get('http://uva.onlinejudge.org/index.php?option=com_onlinejudge&Itemid=9')
            tr = driver.find_element_by_class_name('sectiontableentry1')
            tds = tr.find_elements_by_tag_name('td')
            verdict = tds[3].text
            logger.info('Got {}'.format(verdict))
            if verdict in TERMINATING_VERDICT:
                return {'verdict': verdict.title()}
            sleep(2)
    finally:
        driver.close()

import remotejudge
from django.conf import settings
from django.core.cache import cache
from celery.task import task
from celery.exceptions import SoftTimeLimitExceeded
from judge.submissions.models import Submission, SubmissionJudgement, SubmissionLog, TestCaseResult
from judge.submissions.tasks.sync import RsyncSynchornizer, SyncError
from judge.problems.models import Problem
import filecmp
import threading
import subprocess
import logging
import os
import signal
import resource
import collections
import timeit
import json


COMPILER_EXEC_USER = 'bigdog'                      # The user used to execute compiler
COMPILATION_TIMEOUT = 10                           # Max time for compiling
COMPILATION_MEM = 512                              # Max memory for compiler
EXEC_USER = 'bigdog'                               # The user to execute compiler & user's program
WORKING_DIR = '/home/bigdog'                       # The working directory
TESTS_DIR = os.path.join(WORKING_DIR, 'tests')
CHECKER_DIR = os.path.join(TESTS_DIR, 'checker') # The checker's directory
logger = logging.getLogger(__name__)
localjudge_settings = settings.OJ["LOCALJUDGE"]


class LanguageNotSupported(Exception):
    pass


class LocalSourceCode(object):
    """ Provide an interface for storing/compiling/executing submissions """
    
    EXTENSION = {
        "C++": "cpp",
        "C": "c",
        "Java": "java",
        "Pascal": "pas",
        "Python 2.7": "py"
    }
    
    def __init__(self, language, code, *args, **kwargs):
        self.log_messages = []
        self.language = language
        if not self.EXTENSION.has_key(language):
            raise LanguageNotSupported("{} language not supported".format(language))
        if language != "Java":
            self.code_filename = "code." + self.EXTENSION.get(language)
        else:
            self.code_filename = "Main.java"
            
        path = os.path.join(WORKING_DIR, self.code_filename)
        with open(path, "w") as f:
            f.write(code)
        if language == "Java":
            self.main_class = kwargs.get("java_main_class") or "Main"
    
    def log(self, msg, *args, **kwargs):
        try:
            message = msg.format(*args, **kwargs)
        except:
            message = msg
        self.log_messages.append(message)
        logger.info(message[:128])
        
    def compile(self, *args, **kwargs):
        language = self.language
        code_filename = self.code_filename
        if language == 'C++':
            compile_command = ['c++', code_filename, '-o', "code"]
        elif language == 'C':
            compile_command = ['cc', code_filename, '-o', "code"]
        elif language == 'Java':
            compile_command = ['javac', self.main_class + ".java"]
        elif language == 'Pascal':
            compile_command = ['fpc', '-Std', code_filename, '-o{}'.format("code"), '-O2']
        elif language == 'Python 2.7':
            return
        else:
            raise LanguageNotSupported()
        
        self.log("COMMAND: {}", compile_command)
    
        kill_event = threading.Event()
        def kill_compilation_process():
            command = "pkill -SIGKILL -u {}".format(COMPILER_EXEC_USER)
            self.log("killing compiler (timeout)")
            self.log("running commands {0}".format(command))
            subprocess.call(command, shell=True)
            kill_event.set()
            return
        
        try:
            self.log("compiling")
            mem_limit = -1 if language == "Java" else 256
            p = subprocess.Popen(compile_command,stdout=subprocess.PIPE, stderr=subprocess.PIPE, cwd=WORKING_DIR,
                                 preexec_fn=SecurePreexec(COMPILATION_TIMEOUT, mem_limit, nproc_limit=50, exec_user=COMPILER_EXEC_USER))
            watchdog = threading.Timer(COMPILATION_TIMEOUT, kill_compilation_process)
            watchdog.start()
            stdoutdata, stderrdata = p.communicate()
            watchdog.cancel()
            if kill_event.is_set():
                kill_event.clear()
                self.log("timed out")
                return (False, "Compilation timed out ({0}s).".format(COMPILATION_TIMEOUT))
            kill_event.clear()
            if p.returncode != 0:
                self.log("compilation failed with exit code {0}".format(p.returncode))
                if language == 'Pascal':
                    return (False, stdoutdata)
                else:
                    return (False, stderrdata)
        except MemoryError:
            self.log("compiler memory error")
            return (False, 'Compiler consumed too much memory (256MB).')
        return (True, "")
    
    def execute(self, *args, **kwargs):
        pass


class Alarm(Exception):
    pass


def alarm_handler(signum, frame):
    raise Alarm()


class SecurePreexec(object):
    def __init__(self, time_limit, memory_limit, nproc_limit=5, exec_user=EXEC_USER):
        self.time_limit = time_limit
        self.memory_limit = memory_limit
        self.nproc_limit = nproc_limit
        self.exec_user = exec_user

    def __call__(self):
        def tr(n):
            return 2000000000 if n == -1 else n
        
        soft, hard = resource.getrlimit(resource.RLIMIT_AS)
        resource.setrlimit(resource.RLIMIT_AS, (min(tr(soft), self.memory_limit*1024*1024),  min(tr(hard), self.memory_limit*1024*1024)))

        soft, hard = resource.getrlimit(resource.RLIMIT_CPU)
        resource.setrlimit(resource.RLIMIT_CPU, (min(tr(soft), self.time_limit), min(tr(hard), self.time_limit)))

        soft, hard = resource.getrlimit(resource.RLIMIT_NPROC)
        resource.setrlimit(resource.RLIMIT_NPROC, (min(tr(soft), self.nproc_limit), min(tr(hard), self.nproc_limit)))
        
        uid = int(subprocess.check_output('id -u ' + self.exec_user, shell=True))
        gid = int(subprocess.check_output('id -g ' + self.exec_user, shell=True))
        os.setgid(gid)
        os.setuid(uid)
        os.seteuid(uid)


def sync_tests(pk):
    RsyncSynchornizer().sync(pk)


class StatusNotifier(object):
    def __init__(self, pk):
        self.pk = pk
        self.key = "code_status_{}".format(pk)
    
    def notify(self, status):
        cache.set(self.key, status, 600)


@task
def judge_local(pk):
    submission = None
    logger.info('Cleaning Working Directory')
    subprocess.check_call('rm -rf {0}'.format(os.path.join(WORKING_DIR, '*')), shell=True)
    notifier = StatusNotifier(pk)
    notifier.notify("Preparing")
    try:
        submission = Submission.objects.get(pk=pk)
        test_all_cases = submission.problem.problem_type == Problem.OI
        TestCaseResult.objects.filter(submission=submission).delete()
        source = LocalSourceCode(submission.language, submission.code)
        
        source.log("sync tests")
        try:
            sync_tests(submission.problem_id)
        except SyncError:
            source.log("sync failed")
            submission.judge("Contact Staff")
            return

        notifier.notify("Compiling")
        compilation_start_time = timeit.default_timer()
        success, message = source.compile()
        compilation_time = timeit.default_timer() - compilation_start_time
        source.log("Compilation takes {:.5}s".format(compilation_time))
        if not success:
            source.log(message)
            submission.judge("Compilation Error")
            return

        final_verdict = "Accepted"
        # A mapping from Test ID to its corresponding verdict
        test_results = collections.OrderedDict()
        for testcase in submission.problem.testcase_set.all().order_by("test_id"):
            test_case_stat = {}
            test_id = testcase.test_id
            notifier.notify("Testing {}".format(test_id))
            inf = os.path.join(WORKING_DIR, submission.problem.input_file or "input.txt")
            outf = os.path.join(WORKING_DIR, submission.problem.output_file or "output.txt")
            stdoutf = os.path.join(WORKING_DIR, "stdoutput.txt")
            tests_input = os.path.join(TESTS_DIR, testcase.input_filename)
            tests_output = os.path.join(TESTS_DIR, testcase.output_filename)

            # Preparing test files
            source.log("*** test #{} ***\npreparing {}...".format(test_id, inf))
            subprocess.check_call(["mv", tests_input, inf])
            subprocess.check_call(["chmod", "666", inf])
            with open(outf, 'w'):
                pass
            subprocess.check_call(["chmod", "666", outf])
            
            source.log("executing")
            env = None
            if submission.language in ['C', 'C++', 'Pascal']:
                exec_cmd = [os.path.join(WORKING_DIR, 'code')]
            elif submission.language == 'Java':
                env = os.environ
                env['CLASSPATH'] = (WORKING_DIR + ':' +
                                    (env.get('CLASSPATH') or ''))
                exec_cmd = ['java', '-Djava.security.manager',
                            '-Xmx{0}m'.format(testcase.memory_limit),
                            '-Xss{0}m'.format(testcase.memory_limit),
                            'Main']
            elif submission.language == 'Python 2.7':
                exec_cmd = ['python', os.path.join(WORKING_DIR, 'code.py')]

            test_verdict = None
            time_limit = testcase.time_limit
            memory_limit = testcase.memory_limit if submission.language != 'Java' else -1
            with open(inf, 'r') as infp, open(outf, 'w') as outfp:
                signal.signal(signal.SIGALRM, alarm_handler)
                signal.alarm(testcase.time_limit)
                try:
                    execution_start_time = timeit.default_timer()
                    if submission.language == 'Java':
                        p = subprocess.Popen(exec_cmd, env=env, stdin=infp, stdout=outfp, 
                                preexec_fn=SecurePreexec(time_limit, -1, 10, 'bigdog'),
                                cwd=WORKING_DIR)
                    else:
                        p = subprocess.Popen(exec_cmd, env=env, stdin=infp, stdout=outfp,
                                preexec_fn=SecurePreexec(time_limit, memory_limit, 2, 'bigdog'),
                                cwd=WORKING_DIR)
                    p.wait()
                    logger.info("User program terminated. Return code: {}".format(p.returncode))
                except Alarm:
                    test_verdict = "Time Limit Exceeded"
                    # submission.judge('Time Limit Exceeded')
                    # return
                except MemoryError:
                    test_verdict = "Memory Limit Exceeded"
                    # submission.judge('Memory Limit Exceeded')
                    # return
                except:
                    test_verdict = "Runtime Error"
                    # submission.judge('Runtime Error')
                    # return
                else:
                    if p.returncode != 0:
                        if -p.returncode == signal.SIGKILL:
                            test_verdict = "Memory Limit Exceeded"
                            # submission.judge("Memory Limit Exceeded")
                        else:
                            test_verdict = "Runtime Error"
                            # submission.judge("Runtime Error")
                        # return
                finally:
                    signal.alarm(0)
                    subprocess.call(['pkill -u bigdog'], shell=True)
            test_case_stat["time"] = timeit.default_timer() - execution_start_time
            if test_verdict:
                test_results[test_id] = test_verdict
                if not test_all_cases:
                    submission.judge(test_verdict)
                    return
                if final_verdict == "Accepted":
                    final_verdict = test_verdict
                TestCaseResult.objects.create(test_case=testcase, submission=submission, judgement=test_verdict,
                                              stat=json.dumps(test_case_stat))
                continue
                
            source.log('comparing to {0}'.format(testcase.output_filename))
            subprocess.check_call(['mv', tests_output, stdoutf])
            
            with open(outf, "r") as f:
                source.log("--- user output (first 50KB) ---")
                source.log(f.read(1024 * 50))
            with open(stdoutf, "r") as f:
                source.log("--- std output (first 50KB) ---")
                source.log(f.read(1024 * 50))
            
            if os.path.exists(os.path.join(CHECKER_DIR, "checker.py")):
                exec_list = ["python", os.path.join(CHECKER_DIR, "checker.py"), inf, stdoutf, outf]
            elif os.path.exists(os.path.join(CHECKER_DIR, "checker")):
                exec_list = [os.path.join(CHECKER_DIR, "checker"), inf, stdoutf, outf]
            else:
                exec_list = []
            
            if exec_list:
                source.log("running checker: {0}".format(exec_list))
                try:
                    p = subprocess.Popen(exec_list, cwd=CHECKER_DIR, stdout=subprocess.PIPE)
                    stdoutdata, stderrdata = p.communicate()
                    if p.returncode != 0:
                        raise subprocess.CalledProcessError(p.returncode, exec_list)
                    verdict = stdoutdata.strip()
                    source.log("checker reports: {}", verdict)
                    if verdict != 'Accepted':
                        logger.info('Non-accepted verdict')
                        test_verdict = "Wrong Answer"
                        # submission.judge('{}'.format(verdict))
                        # return
                    else:
                        test_verdict = "Accepted"
                except subprocess.CalledProcessError:
                    test_verdict = "Checker Failure"
                    # source.log("checker failure")
                    # submission.judge("Contact Staff")
                    # return
            else:
                result = filecmp.cmp(outf, stdoutf)
                source.log("default byte-by-byte compare: {}", result)
                if not result:
                    test_verdict = "Wrong Answer"
                    # submission.judge('Wrong Answer')
                    # return
                else:
                    test_verdict = "Accepted"

            test_results[test_id] = test_verdict
            TestCaseResult.objects.create(test_case=testcase, submission=submission, judgement=test_verdict,
                                          stat=json.dumps(test_case_stat))
            if test_verdict != "Accepted" and not test_all_cases:
                submission.judge(test_verdict)
                return
            elif final_verdict == "Accepted":
                final_verdict = test_verdict
        source.log("Final Verdict: {}", final_verdict)
        source.log("Individual Tests Report:")
        for test_id, verdict in test_results.items():
            source.log("Test {}: {}".format(test_id, verdict))
        submission.judge(final_verdict)
    except UnicodeEncodeError:
        if submission:
            submission.judge("Illegal Character")
    except:
        # Unhandled exception, set to 'Contact Staff' and re-raise
        if submission:
            submission.judge("Contact Staff")
            TestCaseResult.objects.filter(submission=submission).delete()
        raise
    finally:
        logger.info("SAVING LOG")
        notifier.notify("!{}".format(submission.judgement.judgement))
        qs = SubmissionLog.objects.filter(submission=submission)
        if qs.exists():
            log = qs[0]
        else:
            log = SubmissionLog(submission=submission)
        log.log = "\n".join(source.log_messages)
        log.save()
        cache.delete("task_id_for_submission_{}".format(pk))


@task
def judge_remote_webdriver(pk):
    remotejudge.judge_remote_webdriver(pk)


@task
def judge_remote(pk):
    remotejudge.judge_remote(pk)
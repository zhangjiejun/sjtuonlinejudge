from django.conf.urls import patterns, url
from django.core.urlresolvers import reverse
from django.views.generic import RedirectView
from judge.submissions.views import SubmissionListView, SubmissionDetailView,\
    ManualRejudgeView, LogDetailView
from django.views.generic.detail import DetailView
from judge.submissions.models import SubmissionLog

urlpatterns = patterns('judge.submissions.views',
    url(r"^list/$", SubmissionListView.as_view(), name="submission-list"),
    url(r"^(?P<pk>\d+)/$", SubmissionDetailView.as_view(), name="submission-detail"),
    url(r"^(?P<pk>\d+)/log/$", LogDetailView.as_view(), name="submission-log"),
    url(r"^(?P<pk>\d+)/system-rejudge/$", "system_rejudge", name="submission-system-rejudge"),
    url(r"^(?P<pk>\d+)/manual-rejudge/$", ManualRejudgeView.as_view(), name="submission-manual-rejudge"),
    url(r"^api/get-status/$", "get_status", name="submission-get-status"),
)

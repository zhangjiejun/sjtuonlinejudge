from django import forms
from judge.submissions.models import SubmissionJudgement


class SubmissionJudgementForm(forms.ModelForm):
    class Meta:
        model = SubmissionJudgement
        fields = ("judgement", "time", "memory")
from django.contrib import admin
from judge.submissions.services import dispatch_submission_to_task_queue_for_judge
import judge.submissions.models as models

class SubmissionAdmin(admin.ModelAdmin):
    list_display = ('pk', 'user', 'contest', 'problem')
    list_filter = ('contest', 'problem')
    actions = ['rejudge']
    exclude = ("judgement",)

    def queryset(self, request):
        queryset = super(SubmissionAdmin, self).queryset(request)
        return queryset.select_related("problem", "contest", "user", "user__userprofile")

    def rejudge(self, request, queryset):
        queryset.update(judgement=None)
        for s in queryset:
            dispatch_submission_to_task_queue_for_judge(s)

admin.site.register(models.Submission, SubmissionAdmin)
admin.site.register(models.SubmissionJudgement)
admin.site.register(models.TestCaseResult)
admin.site.register(models.ExternalSubmission)
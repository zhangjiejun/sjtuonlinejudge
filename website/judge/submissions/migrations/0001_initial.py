# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):
    depends_on = (
        ("contests", "0001_initial.py"),
    )

    def forwards(self, orm):
        # Adding model 'Submission'
        db.create_table(u'submissions_submission', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('contest', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['contests.Contest'], null=True, blank=True)),
            ('problem', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['problems.Problem'])),
            ('user', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
            ('code', self.gf('django.db.models.fields.TextField')()),
            ('language', self.gf('django.db.models.fields.CharField')(max_length=30, db_index=True)),
            ('submit_time', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, db_index=True, blank=True)),
            ('judgement', self.gf('django.db.models.fields.related.ForeignKey')(blank=True, related_name='+', null=True, to=orm['submissions.SubmissionJudgement'])),
        ))
        db.send_create_signal(u'submissions', ['Submission'])

        # Adding model 'SubmissionJudgement'
        db.create_table(u'submissions_submissionjudgement', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('submission', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['submissions.Submission'])),
            ('judgement', self.gf('django.db.models.fields.CharField')(max_length=30, db_index=True)),
            ('time', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('memory', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('effective_time', self.gf('django.db.models.fields.DateTimeField')(db_index=True)),
            ('valid', self.gf('django.db.models.fields.BooleanField')(default=False, db_index=True)),
            ('judge', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'], null=True, blank=True)),
        ))
        db.send_create_signal(u'submissions', ['SubmissionJudgement'])

        # Adding model 'TestCaseResult'
        db.create_table(u'submissions_testcaseresult', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('submission', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['submissions.Submission'])),
            ('test_case', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['problems.TestCase'])),
            ('judgement', self.gf('django.db.models.fields.CharField')(max_length=30)),
        ))
        db.send_create_signal(u'submissions', ['TestCaseResult'])

        # Adding model 'SubmissionLog'
        db.create_table(u'submissions_submissionlog', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('submission', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['submissions.Submission'], unique=True)),
            ('time', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, db_index=True, blank=True)),
            ('log', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal(u'submissions', ['SubmissionLog'])

        # Adding model 'ExternalSubmission'
        db.create_table(u'submissions_externalsubmission', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('contest', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['contests.Contest'])),
            ('time', self.gf('django.db.models.fields.DateTimeField')(db_index=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=256)),
            ('problem', self.gf('django.db.models.fields.CharField')(max_length=1)),
            ('is_accepted', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('code', self.gf('django.db.models.fields.TextField')(blank=True)),
        ))
        db.send_create_signal(u'submissions', ['ExternalSubmission'])


    def backwards(self, orm):
        # Deleting model 'Submission'
        db.delete_table(u'submissions_submission')

        # Deleting model 'SubmissionJudgement'
        db.delete_table(u'submissions_submissionjudgement')

        # Deleting model 'TestCaseResult'
        db.delete_table(u'submissions_testcaseresult')

        # Deleting model 'SubmissionLog'
        db.delete_table(u'submissions_submissionlog')

        # Deleting model 'ExternalSubmission'
        db.delete_table(u'submissions_externalsubmission')


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'contests.contest': {
            'Meta': {'ordering': "['-id']", 'object_name': 'Contest'},
            'end_time': ('django.db.models.fields.DateTimeField', [], {'db_index': 'True'}),
            'frozen_minutes': ('django.db.models.fields.IntegerField', [], {}),
            'hidden': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'db_index': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'max_delay_seconds': ('django.db.models.fields.IntegerField', [], {}),
            'min_delay_seconds': ('django.db.models.fields.IntegerField', [], {}),
            'problems': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['problems.Problem']", 'through': u"orm['contests.ContestProblemConfiguration']", 'symmetrical': 'False'}),
            'shadows': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'shadows_rel_+'", 'blank': 'True', 'to': u"orm['contests.Contest']"}),
            'source': ('django.db.models.fields.CharField', [], {'db_index': 'True', 'max_length': '256', 'blank': 'True'}),
            'start_time': ('django.db.models.fields.DateTimeField', [], {'db_index': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '256', 'db_index': 'True'})
        },
        u'contests.contestproblemconfiguration': {
            'Meta': {'ordering': "['contest', 'problem_letter']", 'object_name': 'ContestProblemConfiguration'},
            'contest': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contests.Contest']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'problem': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['problems.Problem']"}),
            'problem_letter': ('django.db.models.fields.CharField', [], {'max_length': '10'})
        },
        u'problems.problem': {
            'Meta': {'ordering': "['id']", 'object_name': 'Problem'},
            'hidden': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'db_index': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'input_file': ('django.db.models.fields.CharField', [], {'max_length': '256', 'blank': 'True'}),
            'output_file': ('django.db.models.fields.CharField', [], {'max_length': '256', 'blank': 'True'}),
            'residing_judge': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'residing_judge_id': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'source': ('django.db.models.fields.CharField', [], {'db_index': 'True', 'max_length': '256', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '256', 'db_index': 'True'})
        },
        u'problems.testcase': {
            'Meta': {'ordering': "['test_id']", 'object_name': 'TestCase'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'input_filename': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'is_pretest': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'memory_limit': ('django.db.models.fields.IntegerField', [], {}),
            'output_filename': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'problem': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['problems.Problem']"}),
            'test_id': ('django.db.models.fields.IntegerField', [], {}),
            'time_limit': ('django.db.models.fields.IntegerField', [], {})
        },
        u'submissions.externalsubmission': {
            'Meta': {'object_name': 'ExternalSubmission'},
            'code': ('django.db.models.fields.TextField', [], {'blank': 'True'}),
            'contest': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contests.Contest']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_accepted': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'problem': ('django.db.models.fields.CharField', [], {'max_length': '1'}),
            'time': ('django.db.models.fields.DateTimeField', [], {'db_index': 'True'})
        },
        u'submissions.submission': {
            'Meta': {'ordering': "['-id']", 'object_name': 'Submission'},
            'code': ('django.db.models.fields.TextField', [], {}),
            'contest': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contests.Contest']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'judgement': ('django.db.models.fields.related.ForeignKey', [], {'blank': 'True', 'related_name': "'+'", 'null': 'True', 'to': u"orm['submissions.SubmissionJudgement']"}),
            'language': ('django.db.models.fields.CharField', [], {'max_length': '30', 'db_index': 'True'}),
            'problem': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['problems.Problem']"}),
            'submit_time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'db_index': 'True', 'blank': 'True'}),
            'user': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"})
        },
        u'submissions.submissionjudgement': {
            'Meta': {'object_name': 'SubmissionJudgement'},
            'effective_time': ('django.db.models.fields.DateTimeField', [], {'db_index': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'judge': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']", 'null': 'True', 'blank': 'True'}),
            'judgement': ('django.db.models.fields.CharField', [], {'max_length': '30', 'db_index': 'True'}),
            'memory': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'submission': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['submissions.Submission']"}),
            'time': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'valid': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'db_index': 'True'})
        },
        u'submissions.submissionlog': {
            'Meta': {'object_name': 'SubmissionLog'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'log': ('django.db.models.fields.TextField', [], {}),
            'submission': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['submissions.Submission']", 'unique': 'True'}),
            'time': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'db_index': 'True', 'blank': 'True'})
        },
        u'submissions.testcaseresult': {
            'Meta': {'object_name': 'TestCaseResult'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'judgement': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'submission': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['submissions.Submission']"}),
            'test_case': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['problems.TestCase']"})
        }
    }

    complete_apps = ['submissions']
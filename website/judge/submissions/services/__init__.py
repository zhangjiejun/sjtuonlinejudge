from judge.submissions.models import SubmissionJudgement, SubmissionLog
from datetime import datetime
from django.db import transaction
from django.core.cache import cache
import celery


def get_submission_task_id_cache_key(pk):
    return "task_id_for_submission_{}".format(pk)


def dispatch_submission_to_task_queue_for_judge(submission):
    """Dispatch a submission to proper task queue for judge.
    
    Keyword arguments:
    submission -- The submission object to judge
    
    Return value:
    A Celery task object
    
    """
    cache_key = get_submission_task_id_cache_key(submission.pk)
    existing_task_id = cache.get(cache_key)
    if existing_task_id:
        celery.task.control.revoke(existing_task_id)
        cache.delete(cache_key)
    import judge.submissions.tasks as tasks
    SubmissionJudgement.objects.filter(submission=submission,valid=True).update(valid=False)
    if submission.problem.residing_judge in ["LOCAL", "TopCoder"]:
        task = tasks.judge_local.delay(submission.pk)
    elif submission.problem.residing_judge in ["UVA", "CODEFORCES"]:
        task = tasks.judge_remote_webdriver.delay(submission.pk)
    else:
        task = tasks.judge_remote.delay(submission.pk)
    cache.set(cache_key, task.id)
    return task


def give_judgement(submission, judgement, effective_time=None, judge=None, stat=None):
    """Give a judgement to a submission.

    Keyword arguments:
    :param submission: -- The submission object
    :param judgement: -- The judgement
    :param effective_time: -- A datetime object. when should this judgement take effect. Default to the current time
    :param judge: -- The judge responsible for this judgement. Default to System.
    :param stat: -- A dict object. Statistical information like running time/consumed memory for this submission.
    
    Return Value:
    None
    
    """
    if not effective_time:
        effective_time = datetime.now()
    with transaction.commit_on_success():
        SubmissionJudgement.objects.filter(submission=submission, valid=True).update(valid=False);
        new_judgement = SubmissionJudgement.objects.create(submission=submission, judgement=judgement,
                                                           effective_time=effective_time, valid=True, judge=judge)
        submission.judgement = new_judgement
        submission.save(update_fields=["judgement"])


def write_submission_log(submission, log_content):
    """Write a submission's log, overwrite existing.

    :param submission: The submission object
    :param log_content: The content of the log

    """
    queryset = SubmissionLog.objects.filter(submission=submission)
    if queryset.exists():
        queryset.update(time=datetime.now(), log=log_content)
    else:
        SubmissionLog.objects.create(submission=submission, time=datetime.now(), log=log_content)


def append_submission_log(submission, log_content, delimiter="\n"):
    """Append to a submission's log, will not overwrite existing.

    :param submission: The submission object
    :param log_content: The content of the log

    """
    queryset = SubmissionLog.objects.filter(submission=submission)
    if queryset.exists():
        new_content = ''.join([queryset[0], delimiter, log_content])
        queryset.update(time=datetime.now(), log=new_content)
    else:
        SubmissionLog.objects.create(submission=submission, time=datetime.now(), log=log_content)
from django.test import TestCase
from django.conf import settings
from judge.submissions.tasks.submitters import POJ, HDU, VALLA, TIMUS, UVA, ZOJ, AIZU, SGU, UESTC

A_PLUS_B_PROGRAM = '''
#include <iostream>
using namespace std;

int main() {
    int a, b;
    while (cin >> a >> b) {
        cout << a + b << endl;
    }
    return 0;
}
'''

GENERAL_RE_PROGRAM = '''
#include <vector>
using namespace std;

int main() {
    vector<int> a;
    a[1] = 1;
    return 0;
}
'''

GENERAL_WA_PROGRAM = '''
#include <iostream>
using namespace std;
int main() {
    cout << "Hello, World" << endl;
    return 0;
}
'''

GENERAL_TLE_PROGRAM = '''
#include <iostream>
using namespace std;

int main() {
    while(1);
}
'''

GENERAL_CE_PROGRAM = '''
#include <iostream>
using namespace std;

int main() {
    whi le(1);
}
'''

class GeneralSubmitterTest(object):
    def test_general_re(self):
        """Tests that a submitter handles Runtime Error correctly"""
        result = self.judge(self.general_id, 'C++', GENERAL_RE_PROGRAM,
                            self.username, self.password)
        self.assertTrue(result.get('verdict').startswith('Runtime Error'))

    def test_general_wa(self):
        """Tests that a submitter handles Wrong Answer correctly"""
        result = self.judge(self.general_id, 'C++', GENERAL_WA_PROGRAM,
                            self.username, self.password)
        self.assertTrue(result.get('verdict').startswith('Wrong Answer'))

    def test_general_ce(self):
        """Tests that a submitter handles Compilation Error correctly"""
        result = self.judge(self.general_id, 'C++', GENERAL_CE_PROGRAM,
                            self.username, self.password)
        self.assertEqual(result.get('verdict'), 'Compilation Error')

    def test_general_tle(self):
        """Tests that a submitter handles Time Limit Exceeded correctly"""
        result = self.judge(self.general_id, 'C++', GENERAL_TLE_PROGRAM,
                            self.username, self.password)
        self.assertTrue(result.get('verdict').startswith('Time Limit Exceeded'))

class APlusBTest(object):
    def test_a_plus_b(self):
        result = self.judge(self.aplusb_id, 'C++', A_PLUS_B_PROGRAM, self.username, self.password)
        self.assertEqual(result['verdict'], 'Accepted')

class PojSubmitterTest(TestCase, GeneralSubmitterTest):
    def setUp(self):
        self.username, self.password = settings.OJ_ACCOUNTS['POJ']
        self.judge = POJ.judge
        self.general_id = '1000'

    def test_ac_1000(self):
        """Tests that POJ Submitter handles AC properly"""
        ac_program = A_PLUS_B_PROGRAM
        result = POJ.judge('1000', 'C++', ac_program, self.username, self.password)
        self.assertEqual(result.get('verdict'), 'Accepted')
        
class HduSubmitterTest(TestCase, GeneralSubmitterTest):
    def setUp(self):
        self.username, self.password = settings.OJ_ACCOUNTS['HDU']
        self.judge = HDU.judge
        self.general_id = '1000'

    def test_ac_1000(self):
        """Tests that HDU Submitter handles AC properly"""
        ac_program = A_PLUS_B_PROGRAM
        result = self.judge('1000', 'C++', ac_program, self.username, self.password)
        self.assertEqual(result.get('verdict'), 'Accepted')

class VallaSubmitterTest(TestCase, GeneralSubmitterTest):
    def setUp(self):
        self.username, self.password = settings.OJ_ACCOUNTS['VALLA']
        self.judge = VALLA.judge
        self.general_id = '4000'

class TimusSubmitterTest(TestCase, GeneralSubmitterTest):
    def setUp(self):
        self.username, self.password = settings.OJ_ACCOUNTS['TIMUS']
        self.judge = TIMUS.judge
        self.general_id = '1000'

    def test_ac_1000(self):
        """Tests that TIMUS Submitter handles AC properly"""
        ac_program = A_PLUS_B_PROGRAM
        result = self.judge('1000', 'C++', ac_program, self.username, self.password)
        self.assertEqual(result.get('verdict'), 'Accepted')

class UvaSubmitterTest(TestCase, GeneralSubmitterTest):
    def setUp(self):
        self.username, self.password = settings.OJ_ACCOUNTS['UVA']
        self.judge = UVA.judge
        self.general_id = '12315'

class ZojSubmitterTest(TestCase, GeneralSubmitterTest, APlusBTest):
    def setUp(self):
        self.username, self.password = settings.OJ_ACCOUNTS['ZOJ']
        self.judge = ZOJ.judge
        self.general_id = '1001'
        self.aplusb_id = '1001'

class AizuSubmitterTest(TestCase, GeneralSubmitterTest):
    def setUp(self):
        self.username, self.password = settings.OJ_ACCOUNTS['AIZU']
        self.judge = AIZU.judge
        self.general_id = '0000'

class SguSubmitterTest(TestCase, GeneralSubmitterTest):
    def setUp(self):
        self.username, self.password = settings.OJ_ACCOUNTS['SGU']
        self.judge = SGU.judge
        self.general_id = '200'

class UestcSubmitterTest(TestCase, GeneralSubmitterTest, APlusBTest):
    def setUp(self):
        self.username, self.password = settings.OJ_ACCOUNTS['UESTC']
        self.judge = UESTC.judge
        self.general_id = '1000'
        self.aplusb_id = '1000'

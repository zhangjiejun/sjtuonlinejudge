from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.contrib.auth.decorators import permission_required
from judge.submissions.models import Submission, SubmissionJudgement, TestCaseResult
from judge.utils import AuthMixin, JudgeFormView, Http403
from django.views.generic.detail import DetailView
import judge.utils as utils
from django.utils.datetime_safe import datetime
from judge.submissions.services import dispatch_submission_to_task_queue_for_judge
from judge.submissions.forms import SubmissionJudgementForm
from django.core.cache import cache
from django.http.response import HttpResponse
from django.shortcuts import render
import json
import difflib
from django.contrib import messages
from django.conf import settings


def apply_filter(queryset, d, key, filter_type=None, dict_key=None):
    dict_key = dict_key or key.split("__")[0]
    if d.get(dict_key, None):
        kwargs = {key + (filter_type or "__contains"): d[dict_key]}
        print kwargs
        return queryset.filter(**kwargs)
    else:
        return queryset


class SubmissionListView(utils.FilteringListView):
    template_name = "submissions/submission_list.djhtml"
    paginate_by = 100

    def get_context_data(self, **kwargs):
        context = super(SubmissionListView, self).get_context_data(**kwargs)
        context["can_view_others_code"] = settings.OJ.get("PUBLIC_SUBMISSIONS", False)
        return context
    
    def get_queryset(self):
        """ will handle filter options in querystring """
        queryset = Submission.objects.checked_all(self.request.user)
        queryset = apply_filter(queryset, self.request.GET, "user__username", "__contains")
        queryset = apply_filter(queryset, self.request.GET, "problem__pk", "__exact")
        queryset = apply_filter(queryset, self.request.GET, "contest__pk", "__exact")
        queryset = apply_filter(queryset, self.request.GET, "judgement__judgement", "__exact", "verdict")
        return queryset.select_related("user", "user__profile", "judgement", "problem", "contest")


class SubmissionDetailView(AuthMixin, DetailView):
    template_name = "submissions/submission_detail.djhtml"
    login_required = True
    
    def get_queryset(self):
        queryset = Submission.objects.checked_all(self.request.user)
        return queryset.select_related()
    
    def get_context_data(self, **kwargs):
        context = super(SubmissionDetailView, self).get_context_data(**kwargs)
        context["test_results"] = (TestCaseResult.objects.filter(submission__pk=self.kwargs["pk"])
                                   .select_related().order_by("test_case__test_id"))
        if self.request.user.is_superuser:
            context["judgements"] = (SubmissionJudgement.objects.filter(submission__pk=self.kwargs["pk"])
                                     .order_by("-pk"))
        return context


class ManualRejudgeView(JudgeFormView):
    form_class = SubmissionJudgementForm
    form_title = "Rejudge"
    permission_required = ["submissions.add_submissionjudgement"]
    
    def form_valid(self, form):
        submission = Submission.objects.get(pk=self.kwargs["pk"])
        data = form.cleaned_data
        submission.judge(data["judgement"], data["time"], data["memory"], datetime.now(), self.request.user)
        return HttpResponseRedirect(reverse("submission-detail", args=(submission.pk,)))


class LogDetailView(AuthMixin, DetailView):
    permission_required = ["view_submissionlog"]
    template_name = "submissions/view_log.djhtml"
    model = Submission


def get_status(request):
    query = ["code_status_{}".format(i) for i in request.GET.get("list", "").split(",")]
    return HttpResponse(json.dumps(cache.get_many(query)), mimetype="application/json")


# @permission_required("submissions.add_submissionjudgement")
def system_rejudge(request, pk):
    queryset = Submission.objects.filter(pk=pk)
    allow_rejudge = request.user.is_superuser
    if queryset.exists():
        submission = queryset[0]
        if submission.judgement and submission.judgement.judgement in Submission.ALLOWED_USER_REJUDGE_JUDGEMENTS:
            allow_rejudge = True
    if not allow_rejudge:
        raise Http403()
    queryset.update(judgement=None)
    dispatch_submission_to_task_queue_for_judge(queryset[0])
    messages.success(request, "Rejudged")
    return HttpResponseRedirect(reverse("submission-detail", args=(pk,)))
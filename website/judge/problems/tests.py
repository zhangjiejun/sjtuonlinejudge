from django.http import Http404
from django.test import TestCase
from django.test.client import Client
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User
from judge.problems.models import Problem, HtmlText, PdfText
from judge.contests.models import Contest
from middlewares.http403handler import Http403
from datetime import timedelta, datetime


class SimpleAccessTest(TestCase):
    def setUp(self):
        self.superuser = User.objects.create_user(username='admin', password='admin', email='')
        self.superuser.is_superuser = True
        self.superuser.is_staff = True
        self.superuser.save()
        self.p1 = Problem.objects.create(title='A+B', source='A', residing_judge='POJ',
                                         residing_judge_id='1000', hidden=False)
        self.p2 = Problem.objects.create(title='A+B Hidden', source='A', residing_judge='POJ',
                                         residing_judge_id='1000', hidden=True)

    def test_access_hidden_problems(self):
        """Tests that a unprivileged user can not access hidden problems, and
        a privileged user can access hidden problems.
        """
        client = Client()
        response = client.get(reverse('judge.problems.views.problem_detail',
                              args=(self.p2.pk,)))
        self.assertEqual(response.status_code, 403)
        client.login(username='admin', password='admin')
        response = client.get(reverse('judge.problems.views.problem_detail',
                              args=(self.p2.pk,)))
        self.assertEqual(response.status_code, 200)

    def test_access_problem_doesnot_exist(self):
        """Tests that when a problem that does not exist is accessed, 404 is returned
        instead of 500"""
        client = Client()
        response = client.get(reverse('judge.problems.views.problem_detail', args=(3,)))
        self.assertEqual(response.status_code, 404)

class ProblemInRunningContestAccessTest(TestCase):
    fixtures = ['simple_fixture']

    def setUp(self):
        now = datetime.now()
        delta = timedelta(hours = 1)
        Contest.objects.filter(pk=1).update(start_time=now, end_time=now + delta)

    def test_access_problem(self):
        """Tests that unprivileged users can not access problems in a running contest,
        while a privileged user can.
        """
        client = Client()
        response = client.get(reverse('judge.problems.views.problem_detail', args=(1,)))
        self.assertEqual(response.status_code, 403)
        client.login(username='admin', password='admin')
        response = client.get(reverse('judge.problems.views.problem_detail', args=(1,)))
        self.assertEqual(response.status_code, 200)


class ProblemInEndedContestAccessTest(TestCase):
    fixtures = ['simple_fixture']
    
    def setUp(self):
        now = datetime.now()
        delta = timedelta(hours=2)
        Contest.objects.all().update(start_time=now-delta-delta, end_time=now-delta)

    def test_access_problem(self):
        """Tests that any user can access a problem in a ended contest"""
        client = Client()
        response = client.get(reverse('judge.problems.views.problem_detail', args=(1,)))
        self.assertEqual(response.status_code, 200)
        client.login(username='admin', password='admin')
        response = client.get(reverse('judge.problems.views.problem_detail', args=(1,)))
        self.assertEqual(response.status_code, 200)


class ProblemModelContestEndedTestCase(TestCase):
    fixtures = ['simple_fixture']

    def setUp(self):
        now = datetime.now()
        delta = timedelta(hours=1)
        Contest.objects.update(start_time=now-delta-delta, end_time=now-delta)
        problem = Problem.objects.get(pk=1)
        HtmlText.objects.filter(problem=problem).delete()
        PdfText.objects.create(problem=problem, data='123')

    def test_is_in_contest(self):
        """Tests that all problem is not in contest"""
        for problem in Problem.objects.all():
            self.assertEqual(problem.is_in_contest(), False)
    
    def test_has_html(self):
        """Tests that all problems except pk=1 has HTML text (in this fixture)"""
        for problem in Problem.objects.all():
            expected_result = problem.pk != 1
            self.assertEqual(problem.has_html(), expected_result)

    def test_has_pdf(self):
        """Tests that all problems except pk=1 has no PDF text"""
        for problem in Problem.objects.all():
            expected_result = problem.pk == 1
            self.assertEqual(problem.has_pdf(), expected_result)

    def test_access(self):
        """Test the access policy:
        * Raise Http403 when a unprivileged user accesses hidden problem
        * Raise Http404 when a user accesses a problem that does not exist
        """
        user = User.objects.get(username='acm')
        admin = User.objects.get(username='admin')
        self.assertRaises(Http403, Problem.objects.checked_get, user, pk=6)
        Problem.objects.checked_get(admin, pk=6)
        self.assertRaises(Http404, Problem.objects.checked_get, user, pk=11)
        self.assertRaises(Http404, Problem.objects.checked_get, admin, pk=11)

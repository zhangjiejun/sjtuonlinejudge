from django.conf import settings
from celery.task import task
import logging
import tempfile
import shutil
from judge.problems.models import Problem, HtmlText, TestCase
from celery.exceptions import SoftTimeLimitExceeded

logger = logging.getLogger(__name__)

@task
def crawl_a_problem(oj, oj_id, source=None):
    """Dynamically import a crawler module, and use it to crawl the specified problem"""
    logger.info(source)
    try:
        try:
            _temp = __import__('judge.problems.tasks.crawlers.{}'.format(oj), fromlist=['crawl'])
            crawl = _temp.crawl
        except ImportError:
            logger.warning('Online Judge {0} is not supported'.format(oj))
            return
        result = crawl(oj_id)

        qs = Problem.objects.filter(residing_judge=oj, residing_judge_id=oj_id)
        problem = qs[0]
        if oj == "TopCoder":
            logger.info("Uploading Test Cases for TopCoder problem")
            try:
                from judge.problems import services
                from crawlers import TopCoder
                dir = tempfile.mkdtemp()
                br = TopCoder.login()
                test_list = TopCoder.crawl_test_cases(br, oj_id, result["method_signature"], dir)
                sample_tests = ["<p><strong>Following tests are the first 5 system tests. "
                                "Not necessarily identical to above Examples!</strong></p>"]
                for i, test in enumerate(test_list[:5]):
                    with open(test[0]) as in_f, open(test[1]) as out_f:
                        sample_tests.extend([
                            "<h2>Sample Input {}</h2>"
                            "<p><pre>{}</pre></p>"
                            "<h2>Sample Output {}</h2>"
                            "<p><pre>{}</pre></p>".format(i + 1, in_f.read(), i + 1, out_f.read())
                        ])
                result["text"] += "".join(sample_tests)
                TestCase.objects.filter(problem=problem).delete()
                services.upload_test_list(problem, test_list, 2, 64, 1)
            finally:
                shutil.rmtree(dir)

        problem.title = result['title']
        problem.source = source or result['source']
        problem.hidden = True
        problem.save()
        qs = HtmlText.objects.filter(problem=problem)
        if qs.exists():
            qs.update(data=result['text'])
        else:
            HtmlText.objects.create(problem=problem, data=result['text'])
    except SoftTimeLimitExceeded:
        logger.critical('Time Limit Exceeded!')
        Problem.objects.filter(residing_judge=oj, residing_judge_id=oj_id).update(title='Crawling Time Limit Exceeded')

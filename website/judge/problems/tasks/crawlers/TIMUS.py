import re
import mechanize
import urlparse
from bs4 import BeautifulSoup
from django.utils.encoding import smart_unicode

def crawl(problem_id):
    ret = {'title': '',
           'text': '',
           'source': 'Ural'}

    url = 'http://acm.timus.ru/print.aspx?space=1&num={0}'.format(problem_id)
    br = mechanize.Browser()
    response = br.open(url)
    header = response.info()

    # resolve the encoding
    r = re.compile('charset=(?P<encoding>\S+)')
    m = r.search(header['Content-type'])
    encoding = m.group('encoding') if m else 'utf-8'
    html = smart_unicode(response.read(), encoding=encoding)
    soup = BeautifulSoup(html)

    # get title
    title_tag = soup.find('h2', attrs={'class': 'problem_title'})
    title = title_tag.text
    title = title[title.find('.')+1:].strip()
    ret['title'] = title

    # get source
    source_div = soup.find('div', attrs={'class': 'problem_source'})
    #if source_div:
    #    source_div.find('b').extract()
    #    ret['source'] = source_div.text

    # inline styles
    link_tags = soup.find_all('link')
    style = u''
    for link in link_tags:
        if 'stylesheet' in link['rel']:
            url = urlparse.urljoin(br.geturl(), link['href'])
            print "Inlining stylesheet: ", url
            r = mechanize.urlopen(url)
            style = soup.new_tag('style')
            style.string = r.read()
        link.replace_with(style)

    # take care of the images
    img_tags = soup.find_all('img')
    for img_tag in img_tags:
        url = urlparse.urljoin(br.geturl(), img_tag['src'])
        img_tag['src'] = url

    ret['text'] = unicode(soup)
    return ret

if __name__ == '__main__':
    print crawl('1031')


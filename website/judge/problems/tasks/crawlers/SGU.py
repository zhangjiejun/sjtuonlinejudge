import urlparse
import urllib2
import traceback
import mechanize
import re
from bs4 import BeautifulSoup
from django.utils.encoding import smart_unicode
import logging

logger = logging.getLogger(__name__)

def crawl(problem_id):
    ret = {
        'title': '',
        'text': '',
        'source': 'SGU',
    }

    url = 'http://acm.sgu.ru/problem.php?contest=0&problem={0}'.format(problem_id)
    br = mechanize.Browser()
    response = br.open(url)
    header = response.info()
    
    # resolve the encoding
    r = re.compile('charset=(?P<encoding>\S+)')
    m = r.search(header['Content-type'])
    encoding = m.group('encoding') if m else 'utf-8'
    html = smart_unicode(response.read(), encoding=encoding)
    soup = BeautifulSoup(html)

    # get title
    ret['title'] = soup.find(re.compile('h\d')).text
    r = re.compile(r'\d+. (?P<title>.+)')
    ret['title'] = r.search(ret['title']).group('title')

    submit_a = soup.find('a', text='[submit]')
    if submit_a:
        submit_a.extract()
    forum_a = soup.find('a', text='[forum]')
    if forum_a:
        forum_a.extract()

    try:
        hr = soup.find_all('hr')[-1]
        junk = [i for i in hr.find_all_next()]
        for i in junk:
            i.extract()
        hr.extract()
    except:
        pass

    # remove scripts
    print 'Removing scripts'
    scripts = soup.find_all('script')
    for s in scripts:
        s.extract()

    # inline styles
    print 'Inlining styles'
    style = ''
    link_tags = soup.head.find_all('link')
    link_tags = soup.find_all('link')
    for link in link_tags:
        if 'stylesheet' in link['rel']:
            url = urlparse.urljoin(br.geturl(), link['href'])
            logger.info('Inlining stylesheet: {}'.format(url))
            try:
                r = mechanize.urlopen(url)
            except urllib2.HTTPError:
                logger.info(traceback.format_exc())
                continue
            new_tag = soup.new_tag('style', type='text/css')
            new_tag.string = r.read()
            soup.head.append(new_tag)
        link.extract()

    # take care of the images
    img_tags = soup.find_all('img')
    for img_tag in img_tags:
        url = urlparse.urljoin(br.geturl(), img_tag['src'])
        img_tag['src'] = url

    ret['text'] = unicode(soup)
    return ret

if __name__=='__main__':
    print crawl('100')


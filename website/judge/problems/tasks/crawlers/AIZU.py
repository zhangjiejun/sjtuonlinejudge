import mechanize
import re
import urlparse
import logging
from bs4 import BeautifulSoup
from django.utils.encoding import smart_unicode

logger = logging.getLogger(__name__)

def crawl(problem_id):
    ret = {'title': '',
           'text': '',
           'source': 'AIZU'}
    
    br = mechanize.Browser()
    url = 'http://judge.u-aizu.ac.jp/onlinejudge/description.jsp?id={0}'.format(problem_id)
    response = br.open(url)

    header = response.info()
    r = re.compile('charset=(?P<encoding>\S+)')
    m = r.search(header['Content-type'])
    encoding = m.group('encoding') if m else 'utf-8'
    org_html = smart_unicode(response.read(), encoding=encoding)
    html = re.sub('<br>','<br/>',org_html)

    soup = BeautifulSoup(html)
    content_div = soup.find('div', attrs = {'id': 'contents'})

    title = soup.find('h1', attrs = {'class': 'title'})
    ret['title'] = title.text

    # construct our new page
    new_soup = BeautifulSoup('<html><head></head><body style="background-color: white;"></body></html>')

    # inline styles

    link_tags = soup.find_all('link')
    for link in link_tags:
        if 'stylesheet' in link['rel']:
            url = urlparse.urljoin(br.geturl(), link['href'])
            logger.info('Inlining stylesheet: {}'.format(url))
            r = mechanize.urlopen(url)
            new_tag = soup.new_tag('style', type='text/css')
            new_tag.string = r.read()
            new_soup.head.append(new_tag)
        link.extract()

    new_soup.body.append(content_div)

    # remove subinfo
    subinfo_div = new_soup.find('div', attrs = {'class': 'subinfo'})
    if subinfo_div :
        subinfo_div.extract()

    # remove bottom
    bottom_hr = new_soup.find('hr')
    if bottom_hr :
        bottom_hr.extract()

    img_tags = new_soup.find_all('img')
    for img_tag in img_tags:
        url = urlparse.urljoin(br.geturl(), img_tag['src'])
        img_tag['src'] = url

    ret['text'] = unicode(new_soup)
    return ret

if __name__ == '__main__':
    crawl('1067')


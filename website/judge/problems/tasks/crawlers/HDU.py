import urlparse
import mechanize
import re
import os
import logging
from bs4 import BeautifulSoup
from django.utils.encoding import smart_unicode

logger = logging.getLogger(__name__)

def crawl(problem_id):
    ret = {'title': '', 'text': '', 'source': 'HDU'}
    br = mechanize.Browser()
    br.addheaders = [('User-agent', 'Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.0.1) Gecko/2008071615 Fedora/3.0.1-1.fc9 Firefox/3.0.1')]
    url = 'http://acm.hdu.edu.cn/showproblem.php?pid={0}'.format(problem_id)
    response = br.open(url)
    header = response.info()
    
    # resolve the encoding
    r = re.compile('charset=(?P<encoding>\S+)')
    m = r.search(header['Content-type'])
    # encoding = m.group('encoding') if m else 'utf-8'
    encoding = 'gbk'
    org_html = smart_unicode(response.read(), encoding=encoding)
    html = re.sub('<br>','<br/>',org_html)
    soup = BeautifulSoup(html)

    ret['title'] = soup.find('h1').text
    source_div = soup.find('div', text='Source')
    if source_div:
        source_div = source_div.find_next('div', attrs={'class': 'panel_content'})
        ret['source'] = source_div.text.strip()
        # Eliminate the anchor
        source_div.string = ret['source']

    # remove source
    source_div = soup.find('div',text='Source')
    if source_div:
        source_div.find_next('div', attrs={'class': 'panel_content'}).extract()
        source_div.find_next('div', attrs={'class': 'panel_bottom'}).extract()
        source_div.extract()

    # remove unnecessary informations
    recommend_div = soup.find('div', text='Recommend')
    if recommend_div:
        recommend_div.find_next('div', attrs={'class': 'panel_content'}).extract()
        recommend_div.find_next('div', attrs={'class': 'panel_bottom'}).extract()
        recommend_div.extract()

    # remove the fixed-width layout, which looks ugly
    table = soup.find('table', attrs={'style': 'word-wrap:break-word'})
    del table['width']
    
    tbody = table
    trs = tbody.find_all('tr', recursive=False)
    trs[0].extract()
    trs[1].extract()
    trs[2].extract()
    trs[4].extract()
    tr = trs[3]

    links = tr.td.find('center', recursive=False)
    if links:
        links.extract()
    
    ac_count = tr.find('span', attrs={'style': 'font-family:Arial;font-size:12px;font-weight:bold;color:green'})
    r = re.compile(r'Time Limit: \d+/\d+ MS \(Java/Others\)')
    time_limit = r.findall(str(ac_count))
    r = re.compile(r'Memory Limit: \d+/\d+ K \(Java/Others\)')
    memory_limit = r.findall(str(ac_count))
    try:
        ac_count.string = "{0}, {1}".format(time_limit[0], memory_limit[0])
    except:
        ac_count.string = 'Time Limit/Memory Limit not available'
    
    # remove scripts
    scripts = soup.find_all('script')
    for s in scripts:
        s.extract()

    # inline styles
    link_tags = soup.head.find_all('link')
    for link in link_tags:
        if 'stylesheet' in link['rel']:
            url = urlparse.urljoin(br.geturl(), link['href'])
            logger.info('inlining style ' + url)
            response = mechanize.urlopen(url)
            new_tag = soup.new_tag('style', type='text/css')
            new_tag.string = smart_unicode(response.read(), encoding=encoding)
            soup.head.append(new_tag)
        link.extract()

    # take care of the images
    img_tags = soup.find_all('img')
    for img_tag in img_tags:
        url = urlparse.urljoin(br.geturl(), img_tag['src'])
        img_tag['src'] = url

    ret['text'] = unicode(soup)
    return ret

if __name__=="__main__":
    print crawl('3509')


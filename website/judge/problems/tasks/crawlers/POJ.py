import mechanize
import urlparse
import logging
import re
from bs4 import BeautifulSoup

logger = logging.getLogger(__name__)

def crawl(problem_id):
    ret = {'title': u'', 'text': u'', 'source': u'POJ'}
    url = 'http://poj.org/problem?id={pid}'.format(pid=problem_id)
    br = mechanize.Browser()
    response = br.open(url)
    header = response.info()
    
    r = re.compile('charset=(?P<encoding>\S+)')
    m = r.search(header['Content-type'])
    encoding = m.group('encoding')
    html = response.read().decode(encoding)
    html = re.sub('<br>','<br/>',html)
    soup = BeautifulSoup(html)

    title_div = soup.find('div', attrs={'class': 'ptt'})
    ret['title'] = title_div.text

    # get the source and remove it
    source_p = soup.find('p', text='Source')
    if source_p:
        ret['source'] = source_p.next_sibling.text
        source_p.next_sibling.extract()
        source_p.extract()
    
    # remove the top navigation table
    soup.find('table', attrs={'style': 'BORDER-COLLAPSE: collapse'}).extract()

    # remove the language select
    soup.find('div', attrs={'style': 'position: absolute; right: 10px;'}).extract()

    t = soup.find('b', text='Total Submissions:')
    t.parent.parent.extract()

    main_table = soup.find('table', attrs={'background': 'images/table_back.jpg'})
    junk = [s for s in main_table.next_siblings]
    for s in junk:
        s.extract()

    # remove scripts
    script_tags = soup.find_all('script')
    for script_tag in script_tags:
        script_tag.extract()

    # remove ad.
    ad = soup.find('a', attrs={'href': 'http://poj.org/summerschool/pku_acm_train.htm'})
    if ad:
        ad.extract()

    # inline styles
    link_tags = soup.find_all('link')
    for link in link_tags:
        if 'stylesheet' in link['rel']:
            url = urlparse.urljoin(br.geturl(), link['href'])
            logger.info('Inline stylesheet: {}'.format(url))
            r = mechanize.urlopen(url)
            new_tag = soup.new_tag('style', type='text/css')
            new_tag.string = r.read()
            soup.head.append(new_tag)
        link.extract()

    # take care of the images
    img_tags = soup.find_all('img')
    for img_tag in img_tags:
        url = urlparse.urljoin(br.geturl(), img_tag['src'])
        img_tag['src'] = url

    ret['text'] = unicode(soup)
    return ret

if __name__ == '__main__':
    result = crawl('1000')
    print result['text']

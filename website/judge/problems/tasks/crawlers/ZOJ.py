from bs4 import BeautifulSoup
import mechanize
import urlparse
import logging

logger = logging.getLogger(__name__)

def crawl(problem_id):
    ret = {'title': '', 'text': '', 'source': 'ZOJ'}

    logger.info('Opening problem\'s page')
    url = "http://acm.zju.edu.cn/onlinejudge/showProblem.do?problemCode=" + str(problem_id)
    br = mechanize.Browser()
    response = br.open(url)
    html = response.read()
    soup = BeautifulSoup(html)

    content_div = soup.find('div', attrs = {'id': 'content_body'})
    ret['title'] = content_div.find('span', attrs = {'class': 'bigProblemTitle'}).text
    center_tags = content_div.find_all('center')
    center_tags.pop().extract()
    strong_tags = content_div.find_all('strong')
    if strong_tags:
        ret['source'] = strong_tags.pop().text

    # Now let me create the webpage for you
    new_soup = BeautifulSoup('<html><head></head><body style="margin: 20px;"></body></html>')

    # inline styles
    link_tags = soup.find_all('link')
    for link in link_tags:
        if 'stylesheet' in link['rel']:
            url = urlparse.urljoin(br.geturl(), link['href'])
            logger.info("Inlining stylesheet: {}".format(url))
            r = mechanize.urlopen(url)
            style = soup.new_tag('style')
            style.string = r.read()
            new_soup.head.append(style)

    new_soup.body.append(content_div)

    # take care of the images
    img_tags = new_soup.find_all('img')
    for img_tag in img_tags:
        url = urlparse.urljoin(br.geturl(), img_tag['src'])
        img_tag['src'] = url

    ret['text'] = unicode(new_soup)
    return ret

if __name__ == "__main__":
    crawl('1002')

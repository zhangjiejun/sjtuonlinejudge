from judge.problems.services import topcoder
from django.conf import settings
from mechanize import Browser
from bs4 import BeautifulSoup, Comment
import tempfile
import shutil
import re
import os
import logging


logger = logging.getLogger(__name__)


def crawl(topcoder_id):
    logger.info("Initiated Crawling TopCoder {}".format(topcoder_id))

    br = login()
    result = crawl_problem_statement(br, topcoder_id)
    return result


def login():
    br = Browser()
    br.open("http://community.topcoder.com/tc?&module=Login")
    br.select_form(nr=1)
    br["username"], br["password"] = settings.OJ_ACCOUNTS["TopCoder"]
    br.submit()
    return br


def crawl_problem_statement(br, topcoder_id):
    response = br.open("http://community.topcoder.com/stat?c=problem_statement&pm={}".format(topcoder_id))
    soup = BeautifulSoup(response.read())

    # Convert "Single Round Match 585 Round 1 - Division II, Level Three" to
    # "Single Round Match 585 Round 1 - Division II"
    source = soup.find("a", href=re.compile(r"/tc\?module=ProblemDetail")).text
    position = source.find(", Level")
    if position != -1:
        source = source[:position]

    return {
        "text": unicode(soup.find("td", attrs={"class": "problemText"})),
        "title": soup.find("td", text="Class:").find_next_sibling().text,
        "source": source,
        "method_signature": soup.find("td", text="Method signature:").find_next_sibling().text,
    }


def crawl_test_cases(br, topcoder_id, raw_method_signature, dir):
    # Proceed to the solution page which contains all system tests
    br.open("http://community.topcoder.com/stat?c=problem_statement&pm={}".format(topcoder_id))
    link = br.links(url_regex=r"tc\?module=ProblemDetail").next()
    br.follow_link(link)
    link = [i for i in br.links(url_regex=r"/stat\?c=problem_solution")][-1]
    response = br.follow_link(link)

    html = response.read()
    soup = BeautifulSoup(html)
    comment = soup.find(text=lambda e: isinstance(e, Comment) and unicode(e).strip() == "System Testing")
    tests_table = comment.find_next_sibling()
    test_rows = tests_table.find_all("tr", attrs={"valign": "top"})

    method_signature = topcoder.parse_method_signature(raw_method_signature)
    parameters_type = method_signature["parameters"]
    return_value_type = method_signature["return_value"]

    test_list = []

    for test_id, row in enumerate(test_rows):
        cells = row.find_all("td", attrs={"class": "statText"})
        raw_input = cells[0].text
        raw_output = cells[1].text
        input = topcoder.parse_raw_test_data(parameters_type, raw_input)
        output = topcoder.parse_raw_test_data([return_value_type], raw_output)
        input_file_path = os.path.join(dir, "{}.in".format(test_id))
        output_file_path = os.path.join(dir, "{}.out".format(test_id))
        with open(input_file_path, "w") as f:
            f.write(topcoder.format_test_data(parameters_type, input))
            f.write("\n")
        with open(output_file_path, "w") as f:
            f.write(topcoder.format_test_data([return_value_type], output))
            f.write("\n")
        test_list.append((input_file_path, output_file_path))

    return test_list
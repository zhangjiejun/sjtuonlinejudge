import mechanize
import re
import urlparse
import logging
from bs4 import BeautifulSoup
from django.utils.encoding import smart_unicode


logger = logging.getLogger(__name__)


def crawl(problem_id):
    ret = {'title': '',
           'text': '',
           'source': 'CODEFORCES'}
    logger.info("Crawling CODEFORCES {}".format(problem_id))
    br = mechanize.Browser()
    url = "http://codeforces.com/problemset/problem/{}/{}".format(problem_id[:-1], problem_id[-1])
    logger.info("Opening {}".format(url))
    response = br.open(url)

    header = response.info()
    r = re.compile('charset=(?P<encoding>\S+)')
    m = r.search(header['Content-type'])
    encoding = m.group('encoding') if m else 'utf-8'
    html = smart_unicode(response.read(), encoding=encoding)

    soup = BeautifulSoup(html)
    content_div = soup.find('div', attrs = {'class': 'problem-statement'})

    title = soup.find('div', attrs = {'class': 'title'})
    ret['title'] = title.text

    # get source
    ret["source"] = soup.find("table", attrs={"class", "rtable"}).find("tr").text

    # construct our new page
    new_soup = BeautifulSoup('<html><head></head><body style="background-color: white;"></body></html>')

    # inline styles
    link_tags = soup.find_all('link')
    for link in link_tags:
        if 'stylesheet' in link['rel']:
            url = urlparse.urljoin(br.geturl(), link['href'])
            logger.info('Inlining stylesheet: {}'.format(url))
            r = mechanize.urlopen(url)
            new_tag = soup.new_tag('style', type='text/css')
            new_tag.string = r.read()
            new_soup.head.append(new_tag)
        link.extract()

    new_soup.body.append(content_div)

    img_tags = new_soup.find_all('img')
    for img_tag in img_tags:
        url = urlparse.urljoin(br.geturl(), img_tag['src'])
        img_tag['src'] = url

    ret['text'] = unicode(new_soup)
    return ret

if __name__ == '__main__':
    logger.setLevel(logging.INFO)
    print crawl('329A')["source"]
import mechanize
import urlparse
import logging
import re
from bs4 import BeautifulSoup

logger = logging.getLogger(__name__)

def crawl(problem_id):
    ret = {'title': u'', 'text': u'', 'source': u'UESTC'}
    url = 'http://acm.uestc.edu.cn/problem.php?pid={pid}'.format(pid=problem_id)
    br = mechanize.Browser()
    response = br.open(url)
    header = response.info()
    
    r = re.compile('charset=(?P<encoding>\S+)')
    m = r.search(header['Content-type'])
    encoding = m.group('encoding')
    html = response.read().decode(encoding)
    soup = BeautifulSoup(html, 'html5lib')

    title_div = soup.find('div', attrs={'id': 'big_title'})
    ret['title'] = title_div.find('h2').text.strip()

    source_p = soup.find('h2', text='Source')
    if source_p:
        source_p = source_p.find_next_siblings('p')[0]
        ret['source'] = source_p.text.strip()

    soup.find('div', attrs={'id': 'header_all'}).extract()
    soup.find('div', attrs={'id': 'menu_all'}).extract()
    soup.find('div', attrs={'id': 'footer_all'}).extract()
    soup.find('div', attrs={'id': 'auto_content'})['style'] = 'width: auto;'
    big_title = soup.find('div', attrs={'id': 'big_title'})
    h3 = big_title.find('h3')
    h4 = h3.find_all('span', attrs={'class': 'h4'})
    h3.string = 'Time Limit: {}, Memory Limit: {}'.format(h4[0].text, h4[1].text)
    for div in soup.find_all('div', attrs={'class': 'pmenu_all'}):
        div.extract()
    
    # remove scripts
    script_tags = soup.find_all('script')
    for script_tag in script_tags:
        script_tag.extract()

    # inline styles
    link_tags = soup.find_all('link')
    for link in link_tags:
        if 'stylesheet' in link['rel']:
            url = urlparse.urljoin(br.geturl(), link['href'])
            logger.info('Inlining stylesheet: '.format(url))
            r = mechanize.urlopen(url)
            new_tag = soup.new_tag('style', type='text/css')
            new_tag.string = r.read()
            soup.body.append(new_tag)
        link.extract()

    # take care of the images
    img_tags = soup.find_all('img')
    for img_tag in img_tags:
        url = urlparse.urljoin(br.geturl(), img_tag['src'])
        img_tag['src'] = url

    ret['text'] = unicode(soup)
    return ret

if __name__ == '__main__':
    result = crawl('1001')

from django.core.urlresolvers import reverse
from django.db import models
from django.core.exceptions import ObjectDoesNotExist
from django.http import Http404
from middlewares.http403handler import Http403
from django.conf import settings


SUPPORTED_OJ = settings.SUPPORTED_OJ


class PdfText(models.Model):
    """Store a problem's PDF description
    
    Since PDF is a binary format, we store it in Base64 format.
    """
    problem = models.OneToOneField('Problem')
    data = models.TextField()
    
    def __unicode__(self):
        return u'PDF Text for: {0}'.format(unicode(self.problem))


class HtmlText(models.Model):
    """Represents a problem's HTML description"""
    problem = models.OneToOneField('Problem')
    data = models.TextField()
    
    def __unicode__(self):
        return u'HTML Text for: {0}'.format(unicode(self.problem))


class ProblemManager(models.Manager):
    def checked_get(self, user, *args, **kwargs):
        try:
            problem = self.get(*args, **kwargs)
            if not user.is_superuser:
                if problem.is_in_contest():
                    raise Http403("Access denied, because this problem is used in a "
                                  "running contest. Please go to that contest to solve "
                                  "this problem.")
                if problem.hidden:
                    raise Http403()
            return problem
        except ObjectDoesNotExist:
            raise Http404()

    def checked_filter(self, user, *args, **kwargs):
        return self.checked_all(user).filter(*args, **kwargs)

    def checked_all(self, user):
        all = self.all()
        if not user.is_superuser:
            all = all.filter(hidden=False)
        return all


class Problem(models.Model):
    """This model represent a problem"""

    # Constant section
    ACM_ICPC = 0
    OI = 1
    PROBLEM_TYPES = (
        (ACM_ICPC, "ACM-ICPC"),
        (OI, "OI")
    )
    CHECKER_STRICT = 0
    CHECKER_IGNORE_WHITESPACE = 1
    CHECKER_FLOAT = 2
    CHECKER_TYPES = (
        (CHECKER_STRICT, "Strict (byte-by-byte)"),
        (CHECKER_IGNORE_WHITESPACE, "Ignore all whitespaces"),
        (CHECKER_FLOAT, "Compare doubles, accepting 1e-9 relative/absolute error")
    )

    title = models.CharField(max_length=256, db_index=True)
    source = models.CharField(max_length=256, blank=True, db_index=True)
    residing_judge = models.CharField(max_length=30, choices=SUPPORTED_OJ)
    residing_judge_id = models.CharField(max_length=30, blank=True)
    hidden = models.BooleanField(db_index=True)
    input_file = models.CharField(max_length=256, blank=True)
    output_file = models.CharField(max_length=256, blank=True)
    problem_type = models.IntegerField(choices=PROBLEM_TYPES, db_index=True, default=ACM_ICPC)
    checker_type = models.IntegerField(choices=CHECKER_TYPES, default=CHECKER_STRICT)

    # Install Custom Manager
    objects = ProblemManager()

    def get_absolute_url(self):
        return reverse('judge.problems.views.problem_detail', args=(self.pk))

    def get_previous_pk(self):
        qs = self.__class__.objects.filter(pk__lt=self.pk).order_by('-pk')
        return qs[0].pk if qs.exists() else 0

    def get_next_pk(self):
        qs = self.__class__.objects.filter(pk__gt=self.pk).order_by('pk')
        return qs[0].pk if qs.exists() else 0
    
    def get_accepted_count(self):
        return self.submission_set.filter(verdict='Accepted').count()
    
    def get_submission_count(self):
        return self.submission_set.all().count()

    def is_in_contest(self):
        for contest in self.contest_set.all():
            if contest.is_running():
                return True
        return False

    def has_pdf(self):
        try:
            a = self.pdftext
        except ObjectDoesNotExist:
            return False
        else:
            return True
    
    def has_html(self):
        try:
            a = self.htmltext
        except ObjectDoesNotExist:
            return False
        else:
            return True

    def __unicode__(self):
        return "%s. %s %s. %s" % (self.id, self.residing_judge, self.residing_judge_id, self.title)
    
    class Meta:
        ordering = ['id']
        permissions = (('superuser', 'Have full access to problems'),)


class TestCase(models.Model):
    """ Represent a test case for local problem
    
    This model does not store the test input/output data directly. 
    It stores the input/output file name on the FTP server

    """
    problem = models.ForeignKey('Problem')
    test_id = models.IntegerField()
    time_limit = models.IntegerField(help_text='In second')
    memory_limit = models.IntegerField(help_text='In MB')
    input_filename = models.CharField(max_length=255)
    output_filename = models.CharField(max_length=255)
    is_pretest = models.BooleanField()

    class Meta:
        ordering = ['test_id']

    def __unicode__(self):
        return u'Problem {0} #{1}'.format(self.problem.pk, self.test_id)
from unittest import TestCase
from StringIO import StringIO
import topcoder

__author__ = 'zhangjiejun1992'


class TestTopCoder(TestCase):
    METHOD_SIGNATURE_TEST_CASES = (
        (
            "int minimalHours(int n, String[] courseInfo)",
            "int",
            "minimalHours",
            ["int", "String[]"]
        ), (
            "int getCount(int[] distancesA, int[] distancesB, int[] distancesC)",
            "int",
            "getCount",
            ["int[]", "int[]", "int[]"]
        ), (
            "String getColor(long r, long g, long b, long k)",
            "String",
            "getColor",
            ["long", "long", "long", "long"]
        )
    )

    def test_parse_method_signature(self):
        for test_case in self.METHOD_SIGNATURE_TEST_CASES:
            result = topcoder.parse_method_signature(test_case[0])
            self.assertEqual(result["return_value"], test_case[1])
            self.assertEqual(result["method_name"], test_case[2])
            self.assertEqual(result["parameters"], test_case[3])

    def test_consume_int(self):
        stream = StringIO("1234, 5679, 9014")
        self.assertEqual(topcoder.consume_int(stream), 1234)
        self.assertEqual(stream.tell(), 4)
        self.assertEqual(topcoder.consume_int(stream), 5679)
        self.assertEqual(stream.tell(), 10)
        self.assertEqual(topcoder.consume_int(stream), 9014)
        self.assertEqual(stream.tell(), 16)

    def test_consume_string(self):
        stream = StringIO('".X.X.", "XX..X", ".XXX.", "...X.", ".X.XX"}')
        self.assertEqual(topcoder.consume_string(stream), ".X.X.")
        self.assertEqual(topcoder.consume_string(stream), "XX..X")
        self.assertEqual(topcoder.consume_string(stream), ".XXX.")
        self.assertEqual(topcoder.consume_string(stream), "...X.")
        self.assertEqual(topcoder.consume_string(stream), ".X.XX")
        self.assertEqual(stream.read(1), "}")

    def test_consume_array(self):
        stream = StringIO('{".X.X.", "XX..X", ".X.X.", "...X.", ".X.XX"},')
        array = topcoder.consume_array(stream, "String")
        self.assertEqual(array, [".X.X.", "XX..X", ".X.X.", "...X.", ".X.XX"])
        self.assertEqual(stream.read(1), ",")

    def test_parse_raw_test(self):
        result = topcoder.parse_raw_test_data(
            ("String[]", "int", "int"),
            '	{".X.X.", "XX..X", ".XXX.", "...X.", ".X.XX"}, 1, 2'
        )
        self.assertEqual(len(result), 3)
        self.assertEqual(result[0], [".X.X.", "XX..X", ".XXX.", "...X.", ".X.XX"])
        self.assertEqual(result[1], 1)
        self.assertEqual(result[2], 2)

    def test_format_test_data(self):
        result = topcoder.format_test_data(
            ("int[]", "int", "String[]", "String"),
            [
                [1, 2, 3, 4, 5, 4, 3, 2, 1, 0],
                100,
                ["123", "4123", "1239*!@#Y*(", "ADIosdfAAWli"],
                "IdfioaQQ#@"
            ]
        )
        print result
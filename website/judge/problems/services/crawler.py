"""This module defines services functions for crawlers"""
import string
import judge.problems.tasks as tasks
from judge.problems.models import Problem


class CrawlRequestError(Exception):
    """Inappropriate crawl request"""
    pass


crawl_list_generators = dict()


def get_crawl_list(oj, from_id, to_id):
    """Compute a list of problem IDs to crawl

    This function should be a dispatching function. It does not contain
    business logic, but it will dispatch the request to appropriate
    implementing functions, according to the oj parameter

    Parameters:
    oj -- a string indicating the name of OJ
    from_id -- the starting id
    to_id -- the to id

    Return value:
    A list of problem IDs (strings)

    """

    if oj in crawl_list_generators:
        return crawl_list_generators[oj](from_id, to_id)
    return _default_crawl_list_generator(from_id, to_id)


def initiate_crawl(oj, crawl_list, source=None):
    """Initiate crawl jobs.

    Parameters:
    oj -- a string, the OJ to crawl
    crawl_list -- a list of problem IDs to crawl
    source -- override the "Source" of a problem if not None. Default None.

    Returns:
    A tuple (created, recrawled) contains 2 lists of strings.

    """
    # Two lists that track IDs of created problems and re-crawled problems
    # Used to report information to user
    created, recrawled = [], []
    for pid in crawl_list:
        if Problem.objects.filter(residing_judge=oj, residing_judge_id=pid).exists():
            # Re-crawl Case: Should re-crawl an existing problem
            recrawled.append("{}. {}".format(oj, pid))
        else:
            # Creation Case: Should create a new problem instance
            created.append("{}. {}".format(oj, pid))
            Problem.objects.create(title='Crawling...', source=oj, residing_judge=oj,
                                   residing_judge_id=pid, hidden=True)
        tasks.crawl_a_problem.delay(oj, pid, source)
    return created, recrawled


def crawl_list_generator(oj):
    """Decorator. Register a function as crawl list generator"""
    def decorate(fn):
        crawl_list_generators[oj] = fn
        return fn
    return decorate


def _default_crawl_list_generator(from_id, to_id):
    """Generate range(from_id, to_id + 1)"""
    try:
        ret = range(int(from_id), int(to_id) + 1)
    except ValueError:
        raise CrawlRequestError()
    return map(str, ret)


@crawl_list_generator("CODEFORCES")
def _codeforces_generator(from_id, to_id):
    """Codeforces generator for ID like 123A 123B 123C"""
    try:
        from_num, from_letter = int(from_id[:-1]), from_id[-1]
        to_num, to_letter = int(to_id[:-1]), to_id[-1]
    except ValueError:
        raise CrawlRequestError("Codeforces ID should be like 123D")
    if not (from_letter in string.ascii_uppercase and to_letter in string.ascii_uppercase):
        raise CrawlRequestError("Codeforces ID should be like 123D")
    if from_num != to_num:
        raise CrawlRequestError("Codeforces from number must equal to to number")
    letter_range = map(chr, range(ord(from_letter), ord(to_letter) + 1))
    return ["{}{}".format(from_num, c) for c in letter_range]


@crawl_list_generator("AIZU")
def _aizu_generator(from_id, to_id):
    """Aizu generator will expand IDs to 4-digit by prepending zeros"""
    try:
        ret = range(int(from_id), int(to_id) + 1)
    except ValueError:
        raise CrawlRequestError()
    return map(lambda x: "{:04d}".format(x), ret)

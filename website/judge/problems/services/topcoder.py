import re
from StringIO import StringIO


def parse_method_signature(signature):
    """Parse a method signature in Problem Statement Page

    For example, "int minimalHours(int n, String[] courseInfo)" will parsed to:
    {
        "return_value": "int",
        "method_name": "minimalHours",
        "parameters": ["int", "String[]"]
    }

    :param signature: a string, raw method signature from web page.

    """
    r = re.compile(r"(?:\s|\(|\)|,)+")
    components = filter(None, r.split(signature))
    try:
        return {
            "return_value": components[0],
            "method_name": components[1],
            "parameters": components[2::2]
        }
    except IndexError:
        raise ValueError("Bad method signature {}".format(signature))


def parse_raw_test_data(parameters_type, raw_test):
    """Given a type list, parse a raw test data string (input and output) to Python objects.

    For example, '{".X.X.", "XX..X", ".XXX.", "...X.", ".X.XX"}, 1, 2' with types "String[], int, int" will be
    parsed to a list of list, int, int:
        [[".X.X.", "XX..X", ".XXX.", "...X.", ".X.XX"], 1, 2]

    :param parameters_type: a list of type names
    :param raw_test: raw test data string from web page
    :return: a list, the parsed test data

    """
    stream = StringIO(raw_test)
    result = []
    for typename in parameters_type:
        if typename.endswith("[]"):
            result.append(consume_array(stream, typename[:-2]))
        else:
            result.append(PRIMITIVE_CONSUMERS[typename](stream))
    return result


def format_test_data(parameters_type, test_data):
    """Format a test data to text format.

    + int, String: as-is
    + double: with 15 digits after decimal
    + arrays: first line is the length of the array, and follow its elements one per line

    :param parameters_type: a list of type names
    :param test_data: a list of Python objects, the parsed test data
    :return: a string, the formatted test data

    """
    result = []
    for typename, obj in zip(parameters_type, test_data):
        if typename.endswith("[]"):
            result.append(format_array(obj, typename[:-2]))
        else:
            result.append(PRIMITIVE_FORMATTERS[typename](obj))
    return "\n\n".join(result)


def consume_until_char(stream, char):
    if len(char) > 1:
        raise ValueError("char should be a single character")
    while True:
        t = stream.read(1)
        if not t or t == char:
            return


PRIMITIVE_CONSUMERS = {}


def primitive_consumer(typename):
    def decorator(fn):
        PRIMITIVE_CONSUMERS[typename] = fn
        return fn
    return decorator


@primitive_consumer("long")
@primitive_consumer("int")
def consume_int(stream):
    char = stream.read(1)
    while char and not char.isdigit() and not char in ["+", "-"]:
        char = stream.read(1)
    negative = char == "-"
    if char in ["+", "-"]:
        char = stream.read(1)
    result = 0
    while char.isdigit():
        result = result * 10 + int(char)
        char = stream.read(1)
    if char:
        stream.seek(stream.tell() - 1)
    return result if not negative else -result


@primitive_consumer("String")
def consume_string(stream):
    consume_until_char(stream, '"')
    char = stream.read(1)
    result = []
    while char:
        if char == '"':
            position = stream.tell()
            tmp = stream.read(2)
            if tmp == ", " or tmp.startswith("}") or not tmp:
                stream.seek(position)
                break
            stream.seek(position)
            result.append('"')
        else:
            result.append(char)
        char = stream.read(1)
    return ''.join(result)


@primitive_consumer("double")
def consume_double(stream):
    char = stream.read(1)
    read = []
    while char and not char.isdigit() and not char in ["+", "-", "."]:
        char = stream.read(1)
    while char and (char.isdigit() or char in ["+", "-", ".", "e", "E"]):
        read.append(char)
    return float(''.join(read))


def consume_array(stream, typename):
    consume_until_char(stream, "{")
    result = []
    consumer = PRIMITIVE_CONSUMERS[typename]
    while True:
        position = stream.tell()
        if stream.read(1) == "}":
            break
        stream.seek(position)
        result.append(consumer(stream))
    return result


PRIMITIVE_FORMATTERS = {}


def primitive_formatter(typename):
    def decorator(fn):
        PRIMITIVE_FORMATTERS[typename] = fn
        return fn
    return decorator


@primitive_formatter("long")
@primitive_formatter("int")
def format_int(obj):
    return str(obj)


@primitive_formatter("double")
def format_double(obj):
    return "{:.18g}".format(obj)


@primitive_formatter("String")
def format_string(obj):
    return obj


def format_array(array, typename):
    result = [str(len(array))]
    formatter = PRIMITIVE_FORMATTERS[typename]
    for obj in array:
        result.append(formatter(obj))
    return "\n".join(result)
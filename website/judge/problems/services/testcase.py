"""This module provides services for test cases.

"""


import os
import re
import ftplib
import shutil
import tempfile
import subprocess
from contextlib import contextmanager
from django.conf import settings
from judge.problems.models import TestCase, Problem
from django.db.models import Max
import judge.utils as utils

class DuplicateTestFileNameError(Exception):
    """Raised when there is duplicated test file name"""
    pass


def handle_bulk_upload(fp, problem, input_regexp=r"^(?P<testname>\w+)(\.in)?$",
                       output_format="{}.out", time_limit=5, memory_limit=256):
    """This function uploads a series of test cases from a ZIP archive.

    The steps of this function:
    1. Save the ZIP archive to a temp file
    2. Extract the ZIP archive to a temp directory
    3. Discover tests from the extracted directory
    4. Upload discovered tests to FTP server

    Authorization:
    This function is not responsible for authorizing the user. It assumes
    sufficient privileges have been granted to the current user.

    Parameters:
    fp -- the ZIP archive's file pointer
    problem -- the Problem model object
    input_regexp -- a regular expression, to matching input files
                    It has a default value r"^(?P<testname>\w+)(\.in)?$", which matches files like "a1.in", "a2.in"
    output_format -- a format string, to generate output files
                     It has a default value "{}.out" which simply append a ".out" to test case's name
    time_limit -- the time limit for all test cases (seconds)
    memory_limit -- the memory limit for all test cases (MBytes)

    """

    # Create a temp directory for the ZIP archive
    temp_dir = tempfile.mkdtemp()

    try:
        # Extract ZIP archive to the temp directory
        utils.upload.unzip_upload_to_dir(fp, temp_dir)

        # Generate file list using os.path.walk
        file_list = []

        # This function is used in os.path.walk
        def process_dir(file_list, dir_name, f_names):
            file_list += [os.path.abspath(os.path.join(dir_name, f)) for f in f_names]

        os.path.walk(temp_dir, process_dir, file_list)

        # Sort the file list in natural order
        utils.sort_by_numbers(file_list)

        # Generate test list
        test_list = discover_tests_from_list(file_list, input_regexp, output_format)

        # Get the queryset of current test cases
        existing_tests = TestCase.objects.filter(problem=problem)

        # Check for duplicate file names
        added_file_set = set([test.input_filename for test in existing_tests] +
                             [test.output_filename for test in existing_tests])
        for input_file, output_file in test_list:
            base = os.path.basename(input_file)
            if base in added_file_set:
                raise DuplicateTestFileNameError("Duplicated file name {}".format(base))
            added_file_set.add(base)
            base = os.path.basename(output_file)
            if base in added_file_set:
                raise DuplicateTestFileNameError("Duplicated file name {}".format(base))
            added_file_set.add(base)

        # Upload test file
        # We number each uploaded test case from existing maximum number plus 1
        initial_test_id = (existing_tests.aggregate(Max("test_id"))["test_id__max"] or 0) + 1
        return upload_test_list(problem, test_list, time_limit, memory_limit, initial_test_id)
    finally:
        shutil.rmtree(temp_dir)


def upload_checker(fp, pk, name=None, connection=None):
    """Helper routine for uploading a checker to the FTP server

    You may pass a FTP connection object for this upload. Please ensure that
    the current directory of the FTP connection is the root directory. The function
    ensures that the current directory of the connection is root at function return

    """
    if name is None:
        name = fp.name.replace('/', '_')
    need_quit = connection is None
    if not connection:
        connection = FTP(settings.TEST_CASE_FTP_URL)
        connection.login(settings.TEST_CASE_FTP_USERNAME, settings.TEST_CASE_FTP_PASSWORD)
    if not str(pk) in connection.nlst():
        connection.mkd(str(pk))
    connection.cwd(str(pk))
    if not 'checker' in connection.nlst():
        connection.mkd('checker')
    connection.cwd('checker')
    connection.storbinary('STOR {0}'.format(name), fp)
    connection.cwd('..')
    connection.cwd('..')
    if (need_quit):
        connection.quit()


def upload_test_list(problem, test_list, time_limit, memory_limit, initial_test_id):
    """Upload a list of tests. Will apply dos2unix.

    :param problem: the problem object
    :param test_list: a list of tuples ({{ input }}, {{output}})
    :param time_limit: the time limit (second)
    :param memory_limit: the memory limit (MB)
    :initial_test_id: int, the initial test id
    :return: a list of uploaded tests

    """
    # Open a FTP connection
    uploaded_list = []
    current_test_id = initial_test_id
    with open_ftp_connection() as ftp_conn:
        # Upload test cases one by one
        for input_file, output_file in test_list:
            # Apply dos2unix
            subprocess.check_call(["dos2unix", input_file])
            subprocess.check_call(["dos2unix", output_file])
            # Get name of input & output, and upload
            input_file_name = os.path.basename(input_file)
            output_file_name = os.path.basename(output_file)
            with open(input_file) as fi, open(output_file) as fo:
                upload_test_case_ftp(fi, problem.pk, input_file_name, ftp_conn)
                upload_test_case_ftp(fo, problem.pk, output_file_name, ftp_conn)
            # Create the test case object
            TestCase.objects.create(problem=problem, input_filename=input_file_name,
                                    output_filename=output_file_name, test_id=current_test_id,
                                    time_limit=time_limit, memory_limit=memory_limit)
            current_test_id += 1
            uploaded_list.append((input_file_name, output_file_name))
    return uploaded_list


def upload_test_case_ftp(fp, pk, name, conn):
    """Uploads a test case to the FTP server

    Parameters:
    fp -- The file object to upload
    pk -- The problem's primary key
    name -- The test case's saved file name
    conn -- The FTP connection to use

    """
    if not str(pk) in conn.nlst():
        conn.mkd(str(pk))
    conn.cwd(str(pk))
    conn.storbinary('STOR {0}'.format(name), fp)
    conn.cwd("..")


class TestsDiscoveringError(Exception):
    pass


class MissingGroupError(TestsDiscoveringError):
    """This exception is raised when no group found in the input regular expression"""
    pass


class InvalidOutputFormatError(TestsDiscoveringError):
    """This exception is raised when the output format string is invalid"""
    pass


def discover_tests_from_list(name_list, input_regexp, output_format):
    """Discovers pairs of test cases (input_file, output_file) from a list of file names

    The discovering is based on a regular expression and a format string. The process is:
        1. For each file names that match the regular expression.
        2. If there's a group named "testname" then extract that group. Otherwise extract the first group.
        3. Generate the corresponding output file name according to the output format string. The output format string
           can be like either "{}.out" or "{testname}.out".
        4. If the output file does exist, then add them to a discovered test case.

    For example, for tests like "(1.in, 1.out), (2.in, 2.out), ...", the input_regexp can be "^(\d+).in$" and
    the output_format can be "{}.out".

    Parameters:
    name_list -- a list of file names
    input_regexp -- the regular expression string for matching input
    output_format -- the format string for generating output file name from input file name

    Exceptions:
    TestsDiscoveringError -- If any error happens when discovering test cases

    Return value:
    Returns a list of tuple (input_filename, output_filename)
    """

    tests = []
    name_set = set(name_list)
    regexp = re.compile(input_regexp)
    for name in name_list:
        dir_name = os.path.dirname(name)
        base_name = os.path.basename(name)
        m = regexp.match(base_name)
        if m is not None:
            # Extract the test name group
            try:
                testname = m.group("testname")
            except IndexError:
                try:
                    testname = m.group(1)
                except IndexError:
                    raise MissingGroupError()
            # Construct the output file name
            try:
                output_base_name = output_format.format(testname, testname=testname)
            except IndexError, KeyError:
                raise
            output_file = os.path.join(dir_name, output_base_name)
            if output_file in name_set:
                tests.append((name, output_file))
    return tests


@contextmanager
def open_ftp_connection():
    """Returns a logged in FTP connection object for manipulating test files.

    This connection will be at the root folder storing all problems' test cases

    This function can be used with "with" clause, e.g.:
        with open_connection() as conn:
            ... use conn ...

    It will automatically quit when leaving the with block

    """
    connection = ftplib.FTP(settings.TEST_CASE_FTP_URL)
    connection.login(settings.TEST_CASE_FTP_USERNAME, settings.TEST_CASE_FTP_PASSWORD)
    try:
        yield connection
    finally:
        connection.quit()



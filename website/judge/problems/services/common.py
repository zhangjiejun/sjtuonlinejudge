from judge.problems.models import Problem, HtmlText
from django.db.models import Q
import json


def do_quick_search(user, keywords):
    """Filter for each keywords (space-separated).

    For each keyword, if a problem's neither following fields contain that keyword, this problem will be
    filtered out.

    * ID (equals)
    * Title (contains)
    * Source (contains)
    * OJ (equals)
    * OJ's ID (contains)

    :param user: the user object who issued this search
    :param keywords: string, the keywords

    """
    queryset = Problem.objects.checked_filter(user)
    for keyword in keywords.split():
        criteria = (Q(title__icontains=keyword) | Q(source__icontains=keyword) | Q(residing_judge=keyword)
                    | Q(residing_judge_id=keyword))
        try:
            keyword = int(keyword)
            criteria = criteria | Q(id=keyword)
        except (TypeError, ValueError):
            pass
        queryset = queryset.filter(criteria)
    return queryset


def serialize_queryset(queryset, fields):
    """Serialize queryset to JSON format"""
    values_list = queryset.values_list(*fields)
    return json.dumps([dict(zip(fields, values)) for values in values_list])


def write_html_text_to_response(text, response):
    """Write HTML text to a response object. Add default font-family style"""
    if isinstance(text, HtmlText):
        text = text.data
    default_style = (
        "<style>"
        "   body { font-family: 'Helvetica Neue', 'Helvetica', 'Arial', sans-serif; }"
        "</style>"
    )
    head_position = text.find("<head>")
    if head_position != -1:
        response.write(text[:0+head_position])
        response.write(default_style)
        response.write(text[0+head_position:])
    else:
        response.write(default_style)
        response.write(text)
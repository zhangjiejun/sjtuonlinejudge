from django.conf.urls import patterns, url
from judge.problems.views import *

urlpatterns = patterns('judge.problems.views',
#    url(r'^(?P<pk>\d+)/clear-test-cases/$', 'clear_test_cases'),
#    url(r'^(?P<pk>\d+)/list-test-cases/$', 'list_test_cases'),
#    url(r'^(?P<pk>\d+)/get-test-case-input/(?P<test_pk>\d+)/$', 'get_test_case_input'),
#    url(r'^(?P<pk>\d+)/get-test-case-output/(?P<test_pk>\d+)/$', 'get_test_case_output'),
#    url(r'^(?P<pk>\d+)/delete-test-case-(?P<test_id>\d+)/$', 'delete_test_case'),
#    url(r'^(?P<pk>\d+)/add-time-limit/$', 'add_time_limit'),
#    url(r'^(?P<pk>\d+)/add-memory-limit/$', 'add_memory_limit'),
#    url(r'^(?P<pk>\d+)/previous/$', 'previous_problem_detail'),
#    url(r'^(?P<pk>\d+)/next/$', 'next_problem_detail'),

    url(r'^(?P<pk>\d+)/pdf/$', 'get_pdf', {'disposition': 'inline'}, 'problem-view-pdf'),
    url(r'^(?P<pk>\d+)/download-pdf/$', 'get_pdf', {'disposition': 'attachment'}, 'problem-download-pdf'),
    url(r'^(?P<pk>\d+)/html/$', 'get_html', {'disposition': 'inline'}, 'problem-view-html'),
    url(r'^(?P<pk>\d+)/download-html/$', 'get_html', {'disposition': 'attachment'}, 'problem-download-html'),

    url(r'^(list/)?$', ProblemListView.as_view(), name="problem-list"),
    url(r'^detail/(?P<pk>\d+)/$', ProblemDetailView.as_view(), name="problem-detail"),
    url(r"^submit/(?P<pk>\d+)/$", ProblemSubmitView.as_view(), name="problem-submit"),
    url(r"^upload-html/(?P<pk>\d+)/$", UploadHtmlView.as_view(), name="problem-upload-html"),
    url(r"^upload-pdf/(?P<pk>\d+)/$", UploadPdfView.as_view(), name="problem-upload-pdf"),
    url(r'^add/$', 'add_problem', name="problem-add"),
    url(r'^upload-checker/(?P<pk>\d+)/$', UploadCheckerView.as_view(), name="problem-upload-checker"),
    url(r'^upload-test-case/(?P<pk>\d+)/$', 'upload_test_case', name="problem-upload-test-case"),

    url(r'^bulk-upload-test-case/(?P<pk>\d+)/$', BulkUploadTestCasesView.as_view(),
        name="problem-bulk-upload-test-case"),

    url(r"^crawl-request/$", CrawlRequestView.as_view(), name="problem-crawl-request"),

    url(r"^quick-search-api/$", QuickSearchApiView.as_view(), name="problem-quick-search-api"),
    url(r"^topcoder-serialization/$", TopCoderSerializationView.as_view(), name="problem-topcoder-serialization"),


#    url(r'^crawler/$', 'crawler'),
    # Ajax interfaces
#    url(r'^ajax/get-title/$', 'ajax_get_title'),
)

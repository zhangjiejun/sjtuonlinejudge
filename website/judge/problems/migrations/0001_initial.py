# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'PdfText'
        db.create_table(u'problems_pdftext', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('problem', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['problems.Problem'], unique=True)),
            ('data', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal(u'problems', ['PdfText'])

        # Adding model 'HtmlText'
        db.create_table(u'problems_htmltext', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('problem', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['problems.Problem'], unique=True)),
            ('data', self.gf('django.db.models.fields.TextField')()),
        ))
        db.send_create_signal(u'problems', ['HtmlText'])

        # Adding model 'Problem'
        db.create_table(u'problems_problem', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=256, db_index=True)),
            ('source', self.gf('django.db.models.fields.CharField')(db_index=True, max_length=256, blank=True)),
            ('residing_judge', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('residing_judge_id', self.gf('django.db.models.fields.CharField')(max_length=30, blank=True)),
            ('hidden', self.gf('django.db.models.fields.BooleanField')(default=False, db_index=True)),
            ('input_file', self.gf('django.db.models.fields.CharField')(max_length=256, blank=True)),
            ('output_file', self.gf('django.db.models.fields.CharField')(max_length=256, blank=True)),
        ))
        db.send_create_signal(u'problems', ['Problem'])

        # Adding model 'TestCase'
        db.create_table(u'problems_testcase', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('problem', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['problems.Problem'])),
            ('test_id', self.gf('django.db.models.fields.IntegerField')()),
            ('time_limit', self.gf('django.db.models.fields.IntegerField')()),
            ('memory_limit', self.gf('django.db.models.fields.IntegerField')()),
            ('input_filename', self.gf('django.db.models.fields.CharField')(max_length=255)),
            ('output_filename', self.gf('django.db.models.fields.CharField')(max_length=255)),
        ))
        db.send_create_signal(u'problems', ['TestCase'])

        # Adding model 'LocalProblem'
        db.create_table(u'problems_localproblem', (
            (u'problem_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['problems.Problem'], unique=True, primary_key=True)),
            ('time_limit', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('memory_limit', self.gf('django.db.models.fields.CharField')(max_length=30, blank=True)),
            ('input_regexp', self.gf('django.db.models.fields.CharField')(max_length=256)),
            ('output_format_string', self.gf('django.db.models.fields.CharField')(max_length=256)),
        ))
        db.send_create_signal(u'problems', ['LocalProblem'])

        # Adding model 'UVAMapping'
        db.create_table(u'problems_uvamapping', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('display_id', self.gf('django.db.models.fields.CharField')(max_length=10, db_index=True)),
            ('real_id', self.gf('django.db.models.fields.CharField')(max_length=10, db_index=True)),
        ))
        db.send_create_signal(u'problems', ['UVAMapping'])

        # Adding model 'VallaMapping'
        db.create_table(u'problems_vallamapping', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('display_id', self.gf('django.db.models.fields.CharField')(max_length=10, db_index=True)),
            ('real_id', self.gf('django.db.models.fields.CharField')(max_length=10, db_index=True)),
        ))
        db.send_create_signal(u'problems', ['VallaMapping'])


    def backwards(self, orm):
        # Deleting model 'PdfText'
        db.delete_table(u'problems_pdftext')

        # Deleting model 'HtmlText'
        db.delete_table(u'problems_htmltext')

        # Deleting model 'Problem'
        db.delete_table(u'problems_problem')

        # Deleting model 'TestCase'
        db.delete_table(u'problems_testcase')

        # Deleting model 'LocalProblem'
        db.delete_table(u'problems_localproblem')

        # Deleting model 'UVAMapping'
        db.delete_table(u'problems_uvamapping')

        # Deleting model 'VallaMapping'
        db.delete_table(u'problems_vallamapping')


    models = {
        u'problems.htmltext': {
            'Meta': {'object_name': 'HtmlText'},
            'data': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'problem': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['problems.Problem']", 'unique': 'True'})
        },
        u'problems.localproblem': {
            'Meta': {'ordering': "['id']", 'object_name': 'LocalProblem', '_ormbases': [u'problems.Problem']},
            'input_regexp': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            'memory_limit': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'output_format_string': ('django.db.models.fields.CharField', [], {'max_length': '256'}),
            u'problem_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['problems.Problem']", 'unique': 'True', 'primary_key': 'True'}),
            'time_limit': ('django.db.models.fields.CharField', [], {'max_length': '30'})
        },
        u'problems.pdftext': {
            'Meta': {'object_name': 'PdfText'},
            'data': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'problem': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['problems.Problem']", 'unique': 'True'})
        },
        u'problems.problem': {
            'Meta': {'ordering': "['id']", 'object_name': 'Problem'},
            'hidden': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'db_index': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'input_file': ('django.db.models.fields.CharField', [], {'max_length': '256', 'blank': 'True'}),
            'output_file': ('django.db.models.fields.CharField', [], {'max_length': '256', 'blank': 'True'}),
            'residing_judge': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'residing_judge_id': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'source': ('django.db.models.fields.CharField', [], {'db_index': 'True', 'max_length': '256', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '256', 'db_index': 'True'})
        },
        u'problems.testcase': {
            'Meta': {'ordering': "['test_id']", 'object_name': 'TestCase'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'input_filename': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'memory_limit': ('django.db.models.fields.IntegerField', [], {}),
            'output_filename': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'problem': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['problems.Problem']"}),
            'test_id': ('django.db.models.fields.IntegerField', [], {}),
            'time_limit': ('django.db.models.fields.IntegerField', [], {})
        },
        u'problems.uvamapping': {
            'Meta': {'object_name': 'UVAMapping'},
            'display_id': ('django.db.models.fields.CharField', [], {'max_length': '10', 'db_index': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'real_id': ('django.db.models.fields.CharField', [], {'max_length': '10', 'db_index': 'True'})
        },
        u'problems.vallamapping': {
            'Meta': {'object_name': 'VallaMapping'},
            'display_id': ('django.db.models.fields.CharField', [], {'max_length': '10', 'db_index': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'real_id': ('django.db.models.fields.CharField', [], {'max_length': '10', 'db_index': 'True'})
        }
    }

    complete_apps = ['problems']
# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding field 'Problem.checker_type'
        db.add_column(u'problems_problem', 'checker_type',
                      self.gf('django.db.models.fields.IntegerField')(default=0),
                      keep_default=False)


    def backwards(self, orm):
        # Deleting field 'Problem.checker_type'
        db.delete_column(u'problems_problem', 'checker_type')


    models = {
        u'problems.htmltext': {
            'Meta': {'object_name': 'HtmlText'},
            'data': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'problem': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['problems.Problem']", 'unique': 'True'})
        },
        u'problems.pdftext': {
            'Meta': {'object_name': 'PdfText'},
            'data': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'problem': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['problems.Problem']", 'unique': 'True'})
        },
        u'problems.problem': {
            'Meta': {'ordering': "['id']", 'object_name': 'Problem'},
            'checker_type': ('django.db.models.fields.IntegerField', [], {'default': '0'}),
            'hidden': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'db_index': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'input_file': ('django.db.models.fields.CharField', [], {'max_length': '256', 'blank': 'True'}),
            'output_file': ('django.db.models.fields.CharField', [], {'max_length': '256', 'blank': 'True'}),
            'problem_type': ('django.db.models.fields.IntegerField', [], {'default': '0', 'db_index': 'True'}),
            'residing_judge': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'residing_judge_id': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'source': ('django.db.models.fields.CharField', [], {'db_index': 'True', 'max_length': '256', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '256', 'db_index': 'True'})
        },
        u'problems.testcase': {
            'Meta': {'ordering': "['test_id']", 'object_name': 'TestCase'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'input_filename': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'is_pretest': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'memory_limit': ('django.db.models.fields.IntegerField', [], {}),
            'output_filename': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'problem': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['problems.Problem']"}),
            'test_id': ('django.db.models.fields.IntegerField', [], {}),
            'time_limit': ('django.db.models.fields.IntegerField', [], {})
        }
    }

    complete_apps = ['problems']
# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting model 'UVAMapping'
        db.delete_table(u'problems_uvamapping')

        # Deleting model 'LocalProblem'
        db.delete_table(u'problems_localproblem')

        # Deleting model 'VallaMapping'
        db.delete_table(u'problems_vallamapping')

        # Adding field 'TestCase.is_pretest'
        db.add_column(u'problems_testcase', 'is_pretest',
                      self.gf('django.db.models.fields.BooleanField')(default=False),
                      keep_default=False)


    def backwards(self, orm):
        # Adding model 'UVAMapping'
        db.create_table(u'problems_uvamapping', (
            ('display_id', self.gf('django.db.models.fields.CharField')(max_length=10, db_index=True)),
            ('real_id', self.gf('django.db.models.fields.CharField')(max_length=10, db_index=True)),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
        ))
        db.send_create_signal(u'problems', ['UVAMapping'])

        # Adding model 'LocalProblem'
        db.create_table(u'problems_localproblem', (
            ('memory_limit', self.gf('django.db.models.fields.CharField')(max_length=30, blank=True)),
            ('time_limit', self.gf('django.db.models.fields.CharField')(max_length=30)),
            ('output_format_string', self.gf('django.db.models.fields.CharField')(max_length=256)),
            ('input_regexp', self.gf('django.db.models.fields.CharField')(max_length=256)),
            (u'problem_ptr', self.gf('django.db.models.fields.related.OneToOneField')(to=orm['problems.Problem'], unique=True, primary_key=True)),
        ))
        db.send_create_signal(u'problems', ['LocalProblem'])

        # Adding model 'VallaMapping'
        db.create_table(u'problems_vallamapping', (
            ('display_id', self.gf('django.db.models.fields.CharField')(max_length=10, db_index=True)),
            ('real_id', self.gf('django.db.models.fields.CharField')(max_length=10, db_index=True)),
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
        ))
        db.send_create_signal(u'problems', ['VallaMapping'])

        # Deleting field 'TestCase.is_pretest'
        db.delete_column(u'problems_testcase', 'is_pretest')


    models = {
        u'problems.htmltext': {
            'Meta': {'object_name': 'HtmlText'},
            'data': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'problem': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['problems.Problem']", 'unique': 'True'})
        },
        u'problems.pdftext': {
            'Meta': {'object_name': 'PdfText'},
            'data': ('django.db.models.fields.TextField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'problem': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['problems.Problem']", 'unique': 'True'})
        },
        u'problems.problem': {
            'Meta': {'ordering': "['id']", 'object_name': 'Problem'},
            'hidden': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'db_index': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'input_file': ('django.db.models.fields.CharField', [], {'max_length': '256', 'blank': 'True'}),
            'output_file': ('django.db.models.fields.CharField', [], {'max_length': '256', 'blank': 'True'}),
            'residing_judge': ('django.db.models.fields.CharField', [], {'max_length': '30'}),
            'residing_judge_id': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'source': ('django.db.models.fields.CharField', [], {'db_index': 'True', 'max_length': '256', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '256', 'db_index': 'True'})
        },
        u'problems.testcase': {
            'Meta': {'ordering': "['test_id']", 'object_name': 'TestCase'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'input_filename': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'is_pretest': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'memory_limit': ('django.db.models.fields.IntegerField', [], {}),
            'output_filename': ('django.db.models.fields.CharField', [], {'max_length': '255'}),
            'problem': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['problems.Problem']"}),
            'test_id': ('django.db.models.fields.IntegerField', [], {}),
            'time_limit': ('django.db.models.fields.IntegerField', [], {})
        }
    }

    complete_apps = ['problems']
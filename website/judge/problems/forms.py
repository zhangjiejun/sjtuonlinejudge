from django import forms
from django.core.exceptions import ObjectDoesNotExist
from django.conf import settings
from django.core import validators
import judge.problems.models as models
import services.crawler


SUPPORTED_OJ = settings.SUPPORTED_OJ
SUPPORTED_LANGUAGES = settings.SUPPORTED_LANGUAGES


class ProblemField(forms.CharField):
    def clean(self, value):
        if value in validators.EMPTY_VALUES:
            return value
        try:
            value = int(value)
        except ValueError:
            raise forms.ValidationError('Problem ID should be a number')
        try:
            return models.Problem.objects.get(pk=value)
        except ObjectDoesNotExist:
            raise forms.ValidationError('Problem does not exist')


class SubmissionForm(forms.Form):
    language = forms.ChoiceField(SUPPORTED_LANGUAGES)
    code = forms.CharField(widget=forms.Textarea(attrs={'rows': '24', 'cols': '80'}))

    def clean_code(self):
        data = self.cleaned_data['code']
        if len(data) == 0:
            raise forms.ValidationError('Your code should not submit an empty code.')
        if len(data) > 50*1024:
            raise forms.ValidationError('Your code should not exceed 50KB.')
        return data


class UploadHtmlForm(forms.Form):
    html_code = forms.CharField(widget=forms.Textarea(), required=False)
    html_file = forms.FileField(required=False)


class UploadPdfForm(forms.Form):
    pdf_file = forms.FileField()


class AddProblemForm(forms.ModelForm):
    class Meta:
        model = models.Problem


class TestCaseUploadForm(forms.Form):
    problem = ProblemField(widget=forms.TextInput(attrs={'readonly': True}))
    time_limit = forms.IntegerField(help_text='Time limit is in second')
    memory_limit = forms.IntegerField(help_text='Memory limit is in MB')
    input_file = forms.FileField()
    output_file = forms.FileField()


class CheckerUploadForm(forms.Form):
    file = forms.FileField(help_text="ZIP archive will be unzipped automatically")
    next = forms.IntegerField(required=False,
                              help_text="This upload will be assigned to the next {x} problems " + 
                                        "(Include this problem)")


class BulkTestCasesUploadForm(forms.Form):
    time_limit = forms.IntegerField(help_text='Time limit is in second')
    memory_limit = forms.IntegerField(help_text='Memory limit is in MB')
    input_regexp = forms.CharField(help_text="A regular expression for identifying input files. "
                                             "A named group \"testname\" or the first group will be used to "
                                             "generate the output file name",
                                   initial="^(?P<testname>\w+)(\.in)?$")
    output_format = forms.CharField(help_text="A python format string for generating output file names "
                                              "the group extracted in previous regular expression will substitute "
                                              "the {} in this format string",
                                    initial="{}.out")
    zip_file = forms.FileField(help_text='A ZIP file containing inputs and outputs')


CRAWLER_CHOICES = []
for oj in settings.SUPPORTED_OJ:
    if oj[0] != 'LOCAL':
        CRAWLER_CHOICES.append(oj)
CRAWLER_CHOICES.append(("TopCoder", "TopCoder"),)


class CrawlingRequestForm(forms.Form):
    residing_judge = forms.ChoiceField(choices=CRAWLER_CHOICES)
    from_id = forms.CharField()
    to_id = forms.CharField()
    source = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Override source here'}),
                             required = False) 


class CrawlRequestForm(forms.Form):
    """Form for requesting crawl problems

    Will validate the arguments, and produce a "crawl_list" data, a list of problem IDs to crawl.
    Access it using form.cleaned_data["crawl_list"]

    Form fields:
    oj -- The Problem's residing Online Judge
    from_id -- Crawl from this id, inclusive
    to_id -- Crawl until this id, inclusive
    source -- Manually override the crawled problem's source

    """
    oj = forms.ChoiceField(choices=CRAWLER_CHOICES)
    from_id = forms.CharField()
    to_id = forms.CharField()
    source = forms.CharField(widget=forms.TextInput(attrs={'placeholder': 'Override source here'}),
                             required=False)

    def clean(self):
        data = super(CrawlRequestForm, self).clean()
        oj, from_id, to_id = data["oj"], data["from_id"], data["to_id"]
        try:
            data["crawl_list"] = services.crawler.get_crawl_list(oj, from_id, to_id)
        except services.crawler.CrawlRequestError as e:
            raise forms.ValidationError("Bad crawl request. " + e.message)
        return data
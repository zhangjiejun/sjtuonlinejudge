from django.contrib import admin
from judge.problems.models import *


class ProblemAdmin(admin.ModelAdmin):
    def set_problem_not_hidden(self, reqeust, queryset):
        queryset.update(hidden=False)
    set_problem_not_hidden.short_description = 'Mark selected problems as not hidden'

    def set_problem_hidden(self, request, queryset):
        queryset.update(hidden=True)
    set_problem_hidden.short_description = 'Mark selected problems as hidden'

    actions = ["set_problem_hidden", "set_problem_not_hidden"]
    search_fields = ["title", "source"]
    list_display = ['pk', 'title', 'source', 'residing_judge', 'residing_judge_id', 'hidden']
    list_display_links = ['pk', 'title']
    list_filter = ['residing_judge', 'hidden']
    save_as = True
    

class TestCaseAdmin(admin.ModelAdmin):
    search_fields = ['problem__pk']
    list_display = ['problem', 'test_id', "input_filename", "output_filename", "is_pretest"]
    list_filter = ['problem']

    def set_is_pretest(self, request, queryset):
        queryset.update(is_pretest=True)
    set_is_pretest.short_description = "Set selected test cases as pretest"

    def set_is_not_pretest(self, request, queryset):
        queryset.update(is_pretest=False)
    set_is_not_pretest.short_description = "Set selected test cases not pretest"

    actions = ["set_is_pretest", "set_is_not_pretest"]


admin.site.register(TestCase, TestCaseAdmin)
admin.site.register(Problem, ProblemAdmin)
admin.site.register(PdfText)
admin.site.register(HtmlText)

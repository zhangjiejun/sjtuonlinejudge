from judge.problems.models import Problem, PdfText, HtmlText, TestCase
from judge.utils import JudgeFormView
from judge.submissions.models import Submission
from django.contrib import messages
from django.core.exceptions import ObjectDoesNotExist
from django.contrib.auth.decorators import permission_required
from django.http import HttpResponse, Http404, HttpResponseRedirect, HttpResponseBadRequest
from django.core.urlresolvers import reverse
from judge.problems.forms import *
import judge.utils as utils
from django.views.generic import View, DetailView, TemplateView
from django.shortcuts import render
from urllib import urlencode
from django.utils.datetime_safe import datetime
import services
import tempfile
import zipfile
import base64


class ProblemListView(utils.FilteringListView):
    template_name = "problems/problem_list.djhtml"
    paginate_by = 100

    def get_context_data(self, **kwargs):
        context = super(ProblemListView, self).get_context_data(**kwargs)
        context["show_oj"] = settings.OJ["SHOW_EXTERNAL_OJ"]
        context["keyword"] = self.request.GET.get("keyword", "")
        return context

    def get_queryset(self):
        """ will handle filter options in querystring """
        queryset = Problem.objects.all()
        get = self.request.GET
        if "keyword" in get:
            queryset = services.do_quick_search(self.request.user, get["keyword"])
        else:
            queryset = Problem.objects.checked_all(self.request.user)
        if "title" in get:
            queryset = queryset.filter(title__contains=get["title"])
        if "source" in get:
            queryset = queryset.filter(source__contains=get["source"])
        if "oj" in get:
            queryset = queryset.filter(residing_judge=get["oj"])
        queryset = queryset.order_by("-pk")
        return queryset


class ProblemDetailView(DetailView):
    template_name = "problems/problem_detail.djhtml"

    def get_object(self, queryset=None):
        return Problem.objects.checked_get(self.request.user, pk=self.kwargs[self.pk_url_kwarg])


class ProblemSubmitView(JudgeFormView):
    template_name = "general_form.djhtml"
    form_class = SubmissionForm
    login_required = True
    is_wide_form = True

    def get_form_title(self):
        return "Submit Problem ID = " + self.kwargs["pk"]

    def form_valid(self, form):
        data = form.cleaned_data
        now = datetime.now()
        submission = Submission(user=self.request.user, code=data["code"], contest=None,
                                problem=Problem.objects.checked_get(self.request.user, pk=self.kwargs["pk"]),
                                language=data["language"], submit_time=now)
        print submission.judgement
        submission.save()
        messages.success(self.request, "Code received as #{}".format(submission.pk))
        return HttpResponseRedirect(reverse("submission-list"))


class UploadHtmlView(JudgeFormView):
    template_name = "general_upload_form.djhtml"
    permissions_required = ["problems.add_htmltext"]
    form_class = UploadHtmlForm
    form_title = "Upload HTML Text"
    form_help_text = ("Either fill in the HTML code or choose a file to upload.")

    def form_valid(self, form):
        if self.request.FILES.get("html_file"):
            content = self.request.FILES["html_file"].read()
        else:
            content = self.request.POST.get("html_code")
        pk = self.kwargs["pk"]
        problem = Problem.objects.checked_get(self.request.user, pk=pk)
        try:
            text = problem.htmltext
        except ObjectDoesNotExist:
            text = HtmlText()
            text.problem = problem
        text.data = content
        text.save()
        messages.success(self.request, "HTML Saved")
        return HttpResponseRedirect(reverse("problem-detail", args=(pk,)))


class UploadPdfView(JudgeFormView):
    template_name = "general_upload_form.djhtml"
    permissions_required = ["problems.add_pdftext"]
    form_class = utils.GeneralUploadForm
    form_title = "Upload PDF Text"
    form_help_text = ("You can upload a zipfile containing contiguous names PDFs" +
                      "(e.g. xxx1.pdf, xxx2.pdf, xxx3.pdf, ...)." +
                      "They will be assigned to next contiguous problems respectively")

    def form_valid(self, form):
        pk = self.kwargs["pk"]
        problem = Problem.objects.checked_get(self.request.user, pk=pk)
        f = tempfile.TemporaryFile()
        utils.upload.save_upload_to_file(self.request.FILES["file"], f)
        if self.request.FILES["file"].name.endswith(".zip"):
            if not zipfile.is_zipfile(f):
                return HttpResponse("Bad ZIP File")
            z = zipfile.ZipFile(f)
            name_list = [i for i in z.namelist() if i.find("/") == -1 and i[0] != '.' and i.endswith(".pdf")]
            utils.sort_by_numbers(name_list)
            problem_list = Problem.objects.filter(pk__gte=pk).order_by('pk')[:len(name_list)]
            if len(problem_list) != len(name_list):
                return HttpResponse("No Enough Problems")
            for problem, name in zip(problem_list, name_list):
                exists = PdfText.objects.filter(problem=problem).exists()
                text = problem.pdftext if exists else PdfText(problem=problem)
                text.data = z.open(name).read()
                text.save()
                messages.success(self.request, "Pdf {} saved for {}".format(name, problem.pk))
            return HttpResponseRedirect(reverse("problem-detail", args=(pk,)))
        else:
            exists = PdfText.objects.filter(problem=problem).exists()
            text = problem.pdftext if exists else PdfText(problem=problem)
            text.data = base64.encodestring(f.read())
            text.save()
            messages.success(self.request, "PDF saved")
            return HttpResponseRedirect(reverse("problem-upload-pdf", args=(int(pk) + 1,)))


@permission_required('problems.add_problem')
def add_problem(request):
    """Add a problem (for admin)
    
    Specify the following GET parameters to override defaults:
    * hidden
    * source
    * residing_judge
    
    """
    retain_fields = ["hidden", "source", "input_file", "output_file", "residing_judge"]
    if request.method == 'GET':
        GET = request.GET
        form = AddProblemForm(initial={key: GET.get(key, None) for key in retain_fields})
        return render(request, 'general_form.djhtml',
                      {"form": form,
                       "form_title": "Add Problem"})
    else:
        f = AddProblemForm(request.POST)
        if f.is_valid():
            p = f.save()
            url = reverse('problem-add') + "?" + urlencode({key: getattr(p, key) for key in retain_fields})
            messages.success(request, "Problem {} added".format(p))
            return HttpResponseRedirect(url)
        return render(request, 'general_form.djhtml', {'form': f})


def get_html(request, pk, disposition):
    """Delivers HTML text to user
    
    disposition = inline or attachment, for inline displaying & downloading respectively
    
    """
    problem = Problem.objects.checked_get(request.user, pk=pk)
    if not problem.has_html():
        raise Http404()
    text = problem.htmltext
    response = HttpResponse(mimetype='text/html')
    response['Content-Disposition'] = '{0}; filename={1}.html'.format(disposition, pk)
    services.write_html_text_to_response(text.data, response)
    return response


def get_pdf(request, pk, disposition):
    """Delivers PDF text to user
    
    disposition = inline or attachment, for inline displaying & downloading respectively
    
    """
    problem = Problem.objects.checked_get(request.user, pk=pk)
    if not problem.has_pdf():
        raise Http404()
    text = problem.pdftext
    response = HttpResponse(mimetype="application/pdf")
    response['Content-Disposition'] = '{0}; filename={1}.pdf'.format(disposition, pk)
    response.write(base64.decodestring(text.data))
    return response


#def ajax_get_title(request):
#    problem = Problem.objects.checked_get(request.user, pk=request.GET.get('id'))
#    return HttpResponse(unicode(problem.title), content_type="text/plain; charset=utf-8")


class UploadCheckerView(JudgeFormView):
    template_name = "general_upload_form.djhtml"
    form_class = CheckerUploadForm
    permissions_required = ["problems.add_testcase"]

    def get_form_title(self):
        return "Upload Checker for Problem ID = " + self.kwargs["pk"]

    def form_valid(self, form):
        pk = self.kwargs["pk"]
        data = form.cleaned_data
        filename = self.request.FILES["file"].name
        f = tempfile.TemporaryFile()
        utils.upload.save_upload_to_file(self.request.FILES["file"], f)
        problem_list = Problem.objects.filter(pk__gte=pk).order_by('pk')[:(data.get("next") or 1)]
        print problem_list, data.get("next", 1)
        with services.open_ftp_connection() as conn:
            if filename.endswith('.zip'):
                try:
                    z = zipfile.ZipFile(f)
                except:
                    return HttpResponseBadRequest('Bad ZIP File!')
                for name in z.namelist():
                    for p in problem_list:
                        fp = z.open(name)
                        services.upload_checker(fp, p.pk, name, conn)
                    messages.success(self.request, "Uploaded {} for {} problems".format(name, len(problem_list)))
            else:
                for p in problem_list:
                    f.seek(0)
                    services.upload_checker(f, p.pk, filename, conn)
                messages.success(self.request, 'Checker file "{0}" uploaded'.format(filename))
        return HttpResponseRedirect(reverse("problem-upload-checker", args=(pk,)))


@permission_required('problems.add_testcase')
def upload_test_case(request, pk):
    problem = Problem.objects.checked_get(request.user, pk=pk)
    if request.method == 'POST':
        form = TestCaseUploadForm(request.POST, request.FILES)
        if form.is_valid():
            input_filename = request.FILES['input_file'].name
            output_filename = request.FILES['output_file'].name
            with services.open_ftp_connection() as conn:
                services.upload_test_case_ftp(request.FILES["input_file"], pk, input_filename, conn)
                services.upload_test_case_ftp(request.FILES["output_file"], pk, output_filename, conn)
            data = form.cleaned_data
            TestCase.objects.create(problem=problem, input_filename=input_filename, output_filename=output_filename,
                                    test_id=problem.testcase_set.count() + 1, time_limit=data['time_limit'],
                                    memory_limit=data['memory_limit'])
            messages.success(request, "Uploaded in={}, out={}".format(input_filename, output_filename))
            form = TestCaseUploadForm(initial={'problem': pk,
                                               'time_limit': data['time_limit'],
                                               'memory_limit': data['memory_limit']})
    else:
        form = TestCaseUploadForm(initial={'problem': pk})
    return render(request, 'general_upload_form.djhtml',
                  {"form": form,
                   "form_title": "Upload Test Case for " + pk})


class BulkUploadTestCasesView(JudgeFormView):
    permission_required = ["problems.add_test_case"]
    form_class = BulkTestCasesUploadForm
    template_name = "general_upload_form.djhtml"

    def get_form_title(self):
	return "Recoverd from server error 500. The problem name was supposed to be here. I can't fix that, so please don't mind it. But at least it can work now. If you ever want to know what the problem name is, just take a look at the address bar.  ---- lishuo . July30,2013"
        p = Problem.objects.checked_get(self.request.user, self.kwargs["pk"])
        return "Bulk Upload Test Cases for " + unicode(p)

    def form_valid(self, form):
        data = form.cleaned_data
        problem = Problem.objects.checked_get(self.request.user, pk=self.kwargs["pk"])
        uploaded = services.testcase.handle_bulk_upload(self.request.FILES["zip_file"], problem, data["input_regexp"],
                                                        data["output_format"], data["time_limit"], data["memory_limit"])
        messages.success(self.request, "Uploaded {} tests".format(len(uploaded)))
        for input, output in uploaded:
            messages.success(self.request, "({}, {}) uploaded".format(input, output))
        return HttpResponseRedirect(reverse("problem-bulk-upload-test-case", kwargs=self.kwargs))


class CrawlRequestView(JudgeFormView):
    form_class = CrawlRequestForm
    login_required = True
    permission_required = ["problems.add_problem"]

    def get_form_title(self):
        return "Request Crawl Problems"

    def form_valid(self, form):
        data = form.cleaned_data
        created, recrawled = services.initiate_crawl(data["oj"],
                                                     data["crawl_list"],
                                                     data.get("source", None))
        messages.success(self.request, "Request accepted. Created problems: [{}], Re-crawled problems: [{}]".format(
            ", ".join(created),
            ", ".join(recrawled)
        ))
        return HttpResponseRedirect(reverse("problem-crawl-request"))


class QuickSearchApiView(View):
    def get(self, request, *args, **kwargs):
        keywords = request.GET.get("keywords").strip()
        if not keywords:
            json = "[]"
        else:
            queryset = services.do_quick_search(request.user, keywords)[:20]
            json = services.serialize_queryset(queryset,
                                               ("id", "title", "source", "residing_judge", "residing_judge_id"))
        return HttpResponse(json, mimetype="application/json")


class TopCoderSerializationView(TemplateView):
    template_name = "problems/topcoder_serialization.djhtml"

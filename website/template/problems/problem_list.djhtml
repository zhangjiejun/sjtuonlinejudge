{% extends "base.djhtml" %}

{% block header %}
<h1>Problem Set</h1>
{% endblock %}

{% block panels %}
	{{ block.super }}
	<div data-role="panel" data-position="right" data-display="reveal" id="problem-search-panel">
		<h2>Search Problem</h2>
		<form action="./" method="get">
			<input type="number" name="pk" placeholder="Problem ID">
			<input type="text" name="title" placeholder="Title">
			<input type="text" name="source" placeholder="Source">
			<div class="ui-grid-a">
				<div class="ui-block-a"><button type="submit" data-theme="a">Search</button></div>
				<div class="ui-block-b"><a href="./" data-role="button">Reset</a></div>
			</div>
			
		</form>
	</div>
{% endblock %}

{% block header_topright %}
	<a href="#problem-search-panel" data-role="button" data-icon="search" data-iconpos="notext" class="ui-btn-right"></a>
{% endblock %}

{% block content %}
    <form action="{% url "problem-list" %}" method="get">
        <input type="search" name="keyword" placeholder="Press enter to do quick search (Title/Source/OJ...)" value="{{ keyword }}">
    </form>
	{% if user.is_superuser %}
		<p>
			<strong>Management: </strong>
			<a href="{% url "admin:problems_problem_changelist" %}" data-ajax="false">Admin Site</a>
			<a href="{% url "problem-crawl-request" %}">Problem Crawler</a>
		</p>
	{% endif %}
    <table data-role="table" data-mode="columntoggle" class="ui-responsive table-stripe table-stroke problem-set-main-list">
        <colgroup>
            <col span="3">
            <col class="submit">
        </colgroup>
        <thead>
            <th>ID</th>
            <th>Title</th>
            <th data-priority="5">Source</th>
            {% if show_oj %}
                <th data-priority="5">OJ</th>
                <th data-priority="5">OJ ID</th>
            {% endif %}
            <th>Submit</th>
        </thead>
        <tbody>
            {% for p in object_list %}
            {% if user.is_superuser or not p.hidden %}
                <tr{% if p.hidden %} class="hidden-problem"{% endif %}>
                    <td>{{ p.pk }}</td>
                    <td>{% if p.hidden %}[Hidden] {% endif %}<a href="{% url "problem-detail" p.pk %}">{{ p.title }}</a></td>
                    <td><a href="./?source={{ p.source|urlencode }}">{{ p.source }}</a></td>
                    {% if show_oj %}
                        <td>{{ p.residing_judge }}</td>
                        <td>{{ p.residing_judge_id }}</td>
                    {% endif %}
                    <td class="align-right"><a href="{% url "problem-submit" p.pk %}">Submit</a></td>
                </tr>
            {% endif %}
            {% endfor %}
        </tbody>
    </table>
{% include "components/paginator.djhtml" %}
{% endblock %}

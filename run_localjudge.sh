#!/bin/bash

if [ $EUID != 0 ]; then
    echo 'This script requires root priviledge!'
    exit 1
fi

cd website
export C_FORCE_ROOT="true"
python manage.py celeryd -Q localjudge -l INFO -E -n localjudge_worker.`hostname`
